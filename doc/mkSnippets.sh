for i in $( ls images/*.tinte); do
    echo item: $i;
    ../src/tintec ${i%%.*}
    gs -o ${i%%.*}.pdf -sDEVICE=pdfwrite -dDEVICEWIDTHPOINTS=460 -dDEVICEHEIGHTPOINTS=68 -dPDFFitPage out.ps
done

for i in $( ls images2/*.tinte); do
    echo item: $i;
    ../src/tintec ${i%%.*}
    gs -o ${i%%.*}full.pdf -sDEVICE=pdfwrite -dDEVICEWIDTHPOINTS=460 -dDEVICEHEIGHTPOINTS=268 -dPDFFitPage out.ps
    pdfcrop --margin '-1 -100 150 0' --clip ${i%%.*}full.pdf ${i%%.*}.pdf
done
