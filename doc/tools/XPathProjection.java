package name.panitz.crempel.util.xml;
import java.util.List;
import java.util.ArrayList;
import java.io.*;
import javax.xml.parsers.*;

import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;


import javax.xml.transform.*;
import org.w3c.dom.*;

public class XPathProjection {

  public static Node xpath(String path,InputStream inStr)throws Exception{
    Document doc
      = DocumentBuilderFactory
	.newInstance()
	.newDocumentBuilder()
	.parse(inStr) ;
    return xpath(path,doc);

  }

  public static Node xpath(String path,File file)throws Exception{
    return xpath(path,new FileInputStream(file));
  }

  public static Node xpath(String path,Node el)throws Exception{
    Transformer  t = createXPathSelector(path);
    DOMResult resultTree = new DOMResult();
    Element el1
      = DocumentBuilderFactory
	.newInstance()
	.newDocumentBuilder()
	.newDocument().createElement("result") ;

    resultTree.setNode(el1);
    t.transform(new DOMSource(el),resultTree);
    return resultTree.getNode();
  }

  static Transformer createXPathSelector(String path) throws Exception{
    return
        TransformerFactory
	.newInstance()
	.newTransformer(new StreamSource(new StringReader(XSL_START+path+XSL_END)));

  }


  static private String XSL_START=
   "<xsl:stylesheet version=\"1.0\"\n"
  +"                xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\"\n"
  +">\n\n\n"
  +"  <xsl:template match=\"/\">\n"
  +"    <xsl:apply-templates select=\"";




  static private String XSL_END=
   "\"/>\n"
  +"  </xsl:template>\n"
  +"  <xsl:template match=\"@*|node()\">"
  +"    <xsl:copy>"
  +"      <xsl:apply-templates select=\"@*|node()\"/>"
  +"    </xsl:copy>"
  +" </xsl:template>"
  +"</xsl:stylesheet>";

}

