package name.panitz.crempel.util.xml;
import java.util.Iterator;
import org.w3c.dom.*;

public class NodeListIterator implements Iterator<Node>
					,Iterable<Node>{
  final private NodeList nodes;
  private int current = 0;
  public NodeListIterator(NodeList n){nodes=n;}
  public boolean hasNext(){return current<nodes.getLength();}
  public Node next(){current=current+1;return nodes.item(current-1);}
  public void remove(){throw new UnsupportedOperationException();}
  public Iterator<Node> iterator(){return this;}
}
