package name.panitz.crempel.util.xml;
import java.util.Set;
import java.util.HashSet;
import java.util.List;
import java.util.ArrayList;
import java.io.*;
import javax.xml.parsers.*;


import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;


import javax.xml.transform.*;
import org.w3c.dom.*;


import name.panitz.crempel.util.*;

public class ExtractCode {

  public static void extract(File f)throws Exception{
    extract(new FileInputStream(f)); 
  }

  public static void extractTutor(FileInputStream f)throws Exception{
    List<String> deleteMe = new ArrayList<String>();
    deleteMe.add("delete");
    deleteMe.add("student");

    final Node n= XPathProjection.xpath("//code[@class]",delete(f,deleteMe));
    extract(f,n);
  }

  public static void extractStudent(FileInputStream f)throws Exception{
    List<String> deleteMe = new ArrayList<String>();
    deleteMe.add("delete");
    deleteMe.add("tutor");

    final Node n= XPathProjection.xpath("//code[@class]",delete(f,deleteMe));
    extract(f,n);
  }

  public static void extract(FileInputStream f)throws Exception{
    final Node n= XPathProjection.xpath("//code[@class]",deleteDelete(f));
    extract(f,n);
  }
  public static void extract(FileInputStream f,Node n)throws Exception{
    final NodeList cs = n.getChildNodes();    
    Set<Tuple3<String,String,String>> cls =getPackageClass(cs);
    for (Tuple3<String,String,String> cl:cls){
      System.out.println(cl);
      final Node c
       = cl.e1.equals(".")
         ?XPathProjection
          .xpath("//code[@package='.' or not(@package)][@class='"+cl.e2+"'][@lang='"+cl.e3+"']",n) 
         :XPathProjection
          .xpath("//code[@package='"+cl.e1+"'][@class='"+cl.e2+"'][@lang='"+cl.e3+"']",n);
      File dir = new File(cl.e1);
      dir.mkdirs();
      Writer out = cl.e3.equals("")
                   ?new FileWriter(new File(dir,cl.e2)) 
                   :new FileWriter(new File(dir,cl.e2+"."+cl.e3)); 
      NodeList ts = c.getChildNodes();
      for (Node t:new NodeListIterator(ts)){
	  out.write(t.getTextContent());
	  /*final Node textResult
          = XPathProjection
            .xpath("//text()",t);
        System.out.println("iteriere2"+textResult);
        for (Node text:new NodeListIterator(textResult.getChildNodes())){
        System.out.println("iteriere3"+text);
          try {
            out.write(text.getNodeValue());
          }catch (NullPointerException e){System.out.println(text);}
	  }*/
        out.write("\n\n");
      }
      out.flush();
      out.close();
    }
  }

  private static Set<Tuple3<String,String,String>> 
                           getPackageClass(NodeList cs){
    Set<Tuple3<String,String,String>> result 
     = new HashSet<Tuple3<String,String,String>>();
    for (Node c:new NodeListIterator(cs)){
      final Node cl = c.getAttributes().getNamedItem("class");
      final Node pa = c.getAttributes().getNamedItem("package");
      final Node la = c.getAttributes().getNamedItem("lang");
      result.add(new Tuple3<String,String,String>
                  (null==pa?".":pa.getNodeValue()
                  ,cl.getNodeValue()
                  ,null==la?"java":la.getNodeValue()
                  ));  
    }
    return result;
  }

  public static void main(String [] args)throws Exception{
    if (args[0].equals("-tutor"))
      extractTutor(new FileInputStream(new File(args[1])));
    else if (args.length>1)
             extractStudent(new FileInputStream(new File(args[1])));
         else extractStudent(new FileInputStream(new File(args[0])));
  }

  public static Node delete(InputStream inStr,List<String> xs) 
                                                  throws Exception{
    Document doc
      = DocumentBuilderFactory
	.newInstance()
	.newDocumentBuilder()
	.parse(inStr) ;
    Transformer  t = 
         TransformerFactory
	.newInstance()
	.newTransformer(new StreamSource(new StringReader(remove(xs)))); 

    DOMResult resultTree = new DOMResult();
    t.transform(new DOMSource(doc),resultTree);
    return resultTree.getNode();
  }

  public static Node deleteDelete(InputStream inStr) throws Exception{
    Document doc
      = DocumentBuilderFactory
	.newInstance()
	.newDocumentBuilder()
	.parse(inStr) ;
    Transformer  t = 
         TransformerFactory
	.newInstance()
	.newTransformer(new StreamSource(new StringReader(REMOVE_DELETE))); 

    DOMResult resultTree = new DOMResult();
    t.transform(new DOMSource(doc),resultTree);
    return resultTree.getNode();
  }


  static String remove(List<String> xs){
    StringBuffer result=new StringBuffer(REMOVE_START);
    boolean first=true;
    for (String x:xs){
      if (first) first=false;
      else result.append(" | ");
      result.append(x);
    }
    result.append(REMOVE_END);
    return result.toString();
  }

  static String REMOVE_START
   =
   "<xsl:stylesheet version=\"1.0\"\n"
  +"                xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\"\n"
  +">\n\n\n"
  +"  <xsl:template match=\"";

  static String REMOVE_END
   ="\">\n"
  +"  </xsl:template>\n"
  +"  <xsl:template match=\"@*|node()\">"
  +"    <xsl:copy>"
  +"      <xsl:apply-templates select=\"@*|node()\"/>"
  +"    </xsl:copy>"
  +" </xsl:template>"
  +"</xsl:stylesheet>";

  static String REMOVE_DELETE
  = REMOVE_START+"delete"+REMOVE_END;

}
