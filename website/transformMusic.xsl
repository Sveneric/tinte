<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" indent="yes"/>


  <xsl:template match="/">
    <html>
      <head>
	<title>Sheet Music</title>
	<link rel="stylesheet" href="default.css" type="text/css" />
	<meta charset="UTF-8" />
	<meta name="author" content="Sven Eric Panitz" />
	<meta name="description" content=
	      "Tinte Music" />
      </head>

  <body style="background-color: #FFFFFF; ">
    <div style="margin: auto
		;max-width: 1300px
		; background-color: #FFFFFF; ">
      <img src="logo.png" style="float:left;"/>
      <div style="float:right;width: 65%; ">
	<h1  class="top" style="text-align:left;font-size: 3.5vw;"><font color="black">Tinte — Guitar Music</font></h1>
      </div>
    </div>

    <div  style="clear:left;text-align: center;">
      <span  class="menu"><a href="index.html">Home</a></span><xsl:text> </xsl:text>
      <span  class="menu"><a href="music.html">Guitar Music</a></span><xsl:text> </xsl:text>
      <span  class="menu"><a href="tinteUserGuide.pdf">User Guide</a></span><xsl:text> </xsl:text>
      <span  class="menu"><a href="https://github.com/Sveneric/tinte/">Git Repository</a></span><xsl:text> </xsl:text>
      <span  class="menu"><a href="http://panitz.name/">Contact/Impressum</a></span>
    </div>
      
    <div style="clear:right;height: 20px"></div>

    <div style="margin: auto
		    ;max-width: 1300px
		    ; background-color: #FFFFFF; clear:left;">
	  <xsl:apply-templates/>
	</div>

      </body>
    </html>
  </xsl:template>

  <xsl:template match="music">
    <div width="90%">
      <xsl:apply-templates/>
    </div>
  </xsl:template>

  <xsl:template match="experimental">
    <h2>Experimental and Work in Progress</h2>
      <xsl:apply-templates/>

  </xsl:template>

  
  <xsl:template match="piece">
    <b><xsl:apply-templates select="title"/></b>
    <div>
      <xsl:apply-templates select="preview"/>
      <div style="margin-top:20px">
	<xsl:apply-templates select="comment"/>
	<xsl:apply-templates select="files"/>
      </div>
    </div>
  </xsl:template>
  

  <xsl:template match="files">
    <div><div  style="margin-top:20px;">
      <ul>
	<xsl:apply-templates/>
      </ul>
    </div></div>
  </xsl:template>
  <xsl:template match="file">
    <li><a>
      <xsl:attribute name="href">files/music/<xsl:value-of select="name"/>.pdf</xsl:attribute>
      <xsl:apply-templates select="description"/>
    </a></li>
  </xsl:template>

  <xsl:template match="source">
    <li><a>
      <xsl:attribute name="href"><xsl:value-of select="url"/></xsl:attribute>
      Tinte Source Code
    </a></li>
  </xsl:template>

  <xsl:template match="preview">
    <a><xsl:attribute name="href">files/music/<xsl:value-of select="text()"/>.pdf</xsl:attribute>
      <img style="float:left;margin-right: 20px;margin-bottom: 20px;"><xsl:attribute name="src">files/previews/middle-<xsl:value-of select="text()"/>.png</xsl:attribute>
    </img>
    </a>
  </xsl:template>

  
  <xsl:template match="composer">
    <div style="vertical-align: top ;clear:left;margin: 10px;display: inline-block;background-color: #EEE;width: 400px;">
      <div style="margin: 10px">
	<h3>
	   <xsl:choose><xsl:when test="wiki">
	  <a>
	  <xsl:attribute name="href"><xsl:value-of select="wiki"/></xsl:attribute>
	  <xsl:apply-templates select="name"/>
	</a></xsl:when>
	<xsl:otherwise><xsl:apply-templates select="name"/></xsl:otherwise></xsl:choose><xsl:text> </xsl:text><xsl:apply-templates select="dates"/></h3>
	<xsl:apply-templates select="piece"/>
      </div>

    </div>
          
  </xsl:template>

  <xsl:template match="title"><b><xsl:apply-templates/></b> 
  </xsl:template>

  <xsl:template match="comment"><small><xsl:apply-templates/></small> 
  </xsl:template>

  
    <xsl:template match="br"><br/> 
  </xsl:template>

</xsl:stylesheet>
