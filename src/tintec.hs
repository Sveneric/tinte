module Main where
import System.Environment
import Data.Maybe
import Debug.Trace

import Tinte.ParsLib
import Tinte.Tinte
import Tinte.Data
import Tinte.ParseTinte
import Tinte.PP
import Tinte.Pausen
import Tinte.Util
import Data.Map hiding (map)
import Tinte.Notation
import Tinte.ExportMusicXML

main = do
  (name:rgs) <- getArgs
  moin name

moin name= 
 do
   tit  <- readFile (name++".tinte")
   let parses = begin liedImport tit
   if (Prelude.null parses) then do
     putStrLn ("cannot parse source file: "++name++".tinte")
   else do
     let ((_,stimmen):xs) = parses
 
     sequence_
       [let heft = [([(glob,buildOpus mv)|mv<-movements])]
        in  do
          let cah =  cahier 1 heft
          putStrLn ("\nWriting: "++name++".ps")
          tintenotation  (name++".ps") glob (cah) (mkPSHead glob)

          --experimental hook for export of MusicXML
--          let xml =  toMusicXML 1 heft
--          putStrLn ("\nWriting: "++name++".xml")
--          writeTinteXML  (name++".xml") glob xml
       |(name,glob,movements)<- stimmen
       ]
     putStrLn "\nReady"   

buildOpus xs
  = map (\x-> rightOrWrong  
         (hd (begin pSeqList  (removeCommentLine  x)  ) )) xs

hd [] = Left  "hd auf Nil"
hd xs = Right  (head xs)

rightOrWrong (Left s)         = trace (show s)$texto (show s) p1
rightOrWrong (Right (_,res))  = res




