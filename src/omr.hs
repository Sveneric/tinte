module Main where

import System.Environment
import System.Exit

import Carulli.Carulli (moin)

main = do 
  args <- getArgs
  moin args

