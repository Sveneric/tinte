module Tinte.Bogen where
import Debug.Trace

import Data.List
import Tinte.Rat

import Tinte.Data
import Tinte.Constants
import Tinte.Util
import Tinte.Macros
import Tinte.Hals
import Tinte.Noten
import Tinte.Balken
import Tinte.AltSeq
import Tinte.Fahnen



bogenu  (fnr,fh) (tnr,th) alts = bogenuat 1 (fnr,fh) (tnr,th) alts

bogenuat i (fnr,fh) (tnr,th) alts s 
 = (alt {parts=partsneu},s1)
 where
   (alt,s1)= alts s
   partsneu = changeHead addslur (parts alt)
   addslur p
    = p{ppostdefs = ppostdefs p ++[Sluruat i (fnr,fh) (tnr,th)]}


sluruatpart i fr@(fnr,fh) tr@(tnr,th) partsalt s
   |not (success sffrompart&& success sftopart) =  (\z n -> onotation [])
   |otherwise  = --trace ("und los"++show  fr++show tr)$
                  (notaneu)     
   where
--    pdvi = \z n -> (dvineu z n)
--    dvineu    = \z n -> (odvi (D [Push]) +++fst (bogencode z n)+++odvi (D [Pop]))
    notaneu    = \z n -> (onotation [PushPos] +++(bogencode  z n)+++onotation[PopPos])

    bogencode z n = (nota)
     where
      nota = onotation [PushPos,MoveRight (vonw+korright+boff+nocheinkorrekt),MoveDown (starth-staffhoehe)
          ,PutSlurDown (weite+nocheinkorrekt) (hoehe*halbtonab),PopPos]
      nocheinkorrekt = if (weite<abs hoehe*halbtonab)then -halbtonab else 0
      hoeheabs  = abs hoehe
      starth    = fh * halbtonab + 3*halbtonab`div` 2
      vonw    = pwoliegt frompart z n
      nachw    = pwoliegt topart z n
      frompart  = getsuccess sffrompart  
      weite    = abs (vonw-nachw)+poffset topart-poffset frompart
      topart    = getsuccess sftopart
      bogenchr   |hoehe==0  = 32+min 31 (weite`div` bogengerademin)
                 |otherwise  = selbogenchr weite hoeheabs
      korright  = div notendicke 2
      boff    = poffset frompart-poffset(singleHead partsalt)
    hoehe    = (fh-th)
    fntn |hoehe== 0  = fntbogengerade
         |hoehe>0  = fntbogenuntenh
         |otherwise  = fntbogenuntenr
 --   pneu p = p {  pdvi = \z n-> (dvineu z n+++ pdvi p z n)}
 --   partsneu  = changeHead pneu partsalt
    sffrompart  = (sel fnr partsalt)
    sftopart  = (sel tnr partsalt)
    staffhoehe  = putatstaff i 

selbogenchr weite  hoeher
  |hoehe == 0 = 63
  |otherwise = nr
  where
    nu          = 0
    weiternrs  = min (menge) (div weite minweite)
    nr |weiternrs<0     = head (sts!!hoehe) --unclear, why this can happen!!
       |otherwise = --trace ("hoehe: "++show hoehe++"weiternrs "++show weiternrs) 
                       ((sts!!hoehe) !! weiternrs)
    hoehe    = max 1 (min 8 (abs hoeher))
    hmin1    = hoehe-1
    minweite  = --trace ("nu: "++show nu++"hmin1 "++show hmin1 ) 
                 ((sts !! nu)!!hmin1)
    menge    = --trace ("hoehe: "++show hoehe++"nu "++show nu ) 
               ((sts!!hoehe) !! nu)
  
--bogeno :: (Int,Int) (Int,Int) GenElem -> GenElem
bogeno (fnr,fh) (tnr,th) alts = bogenoat 1 (fnr,fh) (tnr,th) alts  


slurAlla :: GenElem -> GenElem
slurAlla elems s = slura 1 (length$getSeq$parts dvielem) elems s
  where
    (dvielem,s2) = elems s

slurAllu :: GenElem -> GenElem
slurAllu elems s = sluru 1 (length$getSeq$parts dvielem) elems s
  where
    (dvielem,s2) = elems s

slura :: Int -> Int -> GenElem -> GenElem
slura fr t elems s1
 = bogeno (fr,xFromH) (t,xToH) elems s1
  where
    (dvielem,s2) = elems s1
    ps = parts dvielem 
    xFromH = phoehe$getTerm$clusterGet fr ps    
    xToH   = phoehe$getTerm$clusterGet t ps    

sluru :: Int -> Int -> GenElem -> GenElem
sluru fr t elems s1
 = bogenu (fr,xFromH) (t,xToH) elems s1
  where
    (dvielem,s2) = elems s1
    ps = parts dvielem 
    xFromH = phoehe$getTerm$clusterGet fr ps    
    xToH   = phoehe$getTerm$clusterGet t ps    

--bogenoat :: Int (Int,Int) (Int,Int) GenElem -> GenElem
bogenoat i (fnr,fh) (tnr,th) alts s 
 = (alt {parts=partsneu},s1)
 where
   (alt,s1)= alts s
   partsneu = changeHead addslur (parts alt)
   addslur p
    = p{ppostdefs = ppostdefs p ++[Sluroat i (fnr,fh) (tnr,th)]}



--makeslurs s p ps z n
--  = (concatO (map (\f -> f z n) (map (fst.(slurToOdvi s  p ps)) (ppostdefs p)))
 --   ,concatON (map (\f -> f z n) (map (snd.(slurToOdvi s  p ps)) (ppostdefs p)))
 --   )


sluroatpart i fr@(fnr,fh) tr@(tnr,th) partsalt s
   |not (success sffrompart&& success sftopart) =  (\z n -> onotation [])
   |otherwise  = --trace ("und los"++show  fr++show tr)$
                  (notaneu)    
   where
    notaneu    = \z n -> (onotation [PushPos] +++ (bogencode  z n)+++onotation[PopPos])

    bogencode z n = (nota)
     where
      nota = onotation [PushPos,MoveRight (vonw+korright+nocheinkorrekt),MoveDown (starth-staffhoehe)
          ,PutSlurUp (weite+nocheinkorrekt) (hoehe*halbtonab),PopPos]
      nocheinkorrekt = if (weite<abs hoehe*halbtonab)then -halbtonab else 0

      starth    = fh * halbtonab - 3*halbtonab `div` 2
      weite    = abs (vonw-nachw)+poffset topart-poffset frompart
      vonw    = (pwoliegt frompart z n)+halbtonab
      nachw    = pwoliegt topart z n
      frompart  = getsuccess sffrompart  
      topart    = getsuccess sftopart
      bogenchr |hoehe == 0 = min 31 (div weite bogengerademin)
              |otherwise = selbogenchr weite hoehe
      korright  = (mod weite  bogenweitemin)`div`2+div notendicke 2
    hoehe    = (fh-th)
    fntn  |hoehe== 0  = fntbogengerade
         |hoehe>0  = fntbogenobenh
         |otherwise  = fntbogenobenr
    sffrompart  = (sel fnr partsalt)
    sftopart  =  (sel tnr partsalt)
    staffhoehe  = putatstaff i 

-- bogenlu :: [((Int,Int), (Int,Int))] GenElem -> GenElem
bogenlu bogdefs alt = foldr (\(f,t) a -> bogenu f t a) alt bogdefs

--bogenlo :: [((Int,Int), (Int,Int))] GenElem -> GenElem
bogenlo bogdefs alt = foldr (\(f,t) a -> bogeno f t a) alt bogdefs





