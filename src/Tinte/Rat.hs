module Tinte.Rat where
import Data.List 
import Debug.Trace
type Rat = (Int,Int)

eq a b = (reduce a) == (reduce b)

less (z1,n1) (z2,n2) = z1*n2 < z2*n1
lessEq (z1,n1) (z2,n2) = z1*n2 <= z2*n1

ratOrder (z1,n1) (z2,n2) 
 |z1*n2 < z2*n1 = LT
 |z1*n2 == z2*n1 = EQ
 |otherwise  = GT

add (0,0) y = y
add x (0,0) = x
add (z1,n1) (z2,n2) 
 |n1==n2=reduce (z1+z2,n1)
 |otherwise= reduce (z1*n2+z2*n1,n1*n2)

minrat xs =  result
  where
   result = foldr (\x y -> -- trace ("" ++show x++"<?" ++ show y)
                             (if (less x y) then x else y)) (100,1) xs

maxrat xs = foldr (\x y -> if (less x y) then y else x) (0,1) xs

reduce (0,n) = (0,1)
reduce (z,n) = (div z ggt,div n ggt)
  where
    fz = primfaktoren z
    fn = primfaktoren n
    sn = schnitt fz fn
    ggt = foldr (*) 1 sn

schnitt [] _= []
schnitt  _ []= []
schnitt (x:xs) ys
 |elem x ys  =(x:schnitt xs (ys \\[x]))
 |otherwise = schnitt xs ys

primfaktoren 1 = []
primfaktoren n
 |otherwise = (ersterteiler:weiter)
  where 
    (ersterteiler:_) = [ x | x <- [2..] ,  mod n  x == 0  ]
    weiter = primfaktoren (div n ersterteiler)


