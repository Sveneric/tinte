module Tinte.Data where
import Tinte.Constants
import Data.Char
import Debug.Trace
import Data.Ratio

infixr 5 +++

class Ap a where
  (+++) :: a -> a -> a

success (Right _) = True
success _  = False

getsuccess (Right r) = r

type Takt = [Element]

data Element 
 =  
    N (Ratio Int) Char Int 
   |NoElement
   |Pause (Ratio Int)
   |Is Element
   |Es Element
   |Na Element
   |Isis Element
   |Eses Element
   |Akkord [Element] StemDirection
   |Beam [Element] StemDirection
   |Alts [Element]
   |Stimme Stimmbezeichnung Element
 deriving (Eq,Show)

hasStem (N rat _ _)   = rat < (1%4) 
hasStem (Pause _)     = False
hasStem (Is el)       = hasStem el
hasStem (Es el)       = hasStem el
hasStem (Na el)       = hasStem el
hasStem (Isis el)     = hasStem el
hasStem (Eses el)     = hasStem el
hasStem (Akkord els NoStem)  = False
hasStem (Stimme _ el) = hasStem el
hasStem _             = True

type  Stimmbezeichnung = String

changeDur f (N dur c i) = N (f dur) c i
changeDur f (Pause dur) = Pause (f dur)
changeDur f NoElement   = NoElement
changeDur f (Is e)      = Is$changeDur f e
changeDur f (Es e)      = Es$changeDur f e
changeDur f (Na e)      = Na$changeDur f e
changeDur f (Isis e)    = Isis$changeDur f e
changeDur f (Eses e)    = Eses$changeDur f e
changeDur f (Stimme n e)= Stimme n $changeDur f e
changeDur f (Akkord els st)= Akkord (map (changeDur f) els) st

taktmin = notendicke
taktmax = notendicke*2
taktamb = taktmax-taktmin
        
type TaktInfo =((Int -> Int->ONotation),Abstand) 
type Abstand = (Int,Int)

isup key c i = setzenoteum key c i > -4
     
setzenoteum :: Schluessel -> Char -> Int -> Int
setzenoteum Violin cc int = (hoch-faktor*7)
  where
     c = ord cc
     hoch 
      |(cc=='a')||(cc=='A')   =  (-3)
      |(cc=='h')  =  (-4)
      | cc == 'H'  = 69-c-1
      |(cc=='C')    =  2
      |isLower  cc  =  101-c
      |otherwise    =  69-c
     faktor
      |isLower  cc = int-1
      |otherwise   = -int-2
setzenoteum Tenor cc int = (setzenoteum Violin cc int) + 1 
setzenoteum Bass cc int = (setzenoteum Violin cc int) + 2-2*7

concatON xs = foldr (+++) (onotation ([])) xs

data Global  
 = Global
   { title                     :: String
   , subtitle                  :: String
   , composer                  :: String
   , tonartsymbol              :: Tonart
   , schluessel                :: [Schluessel]
   , instrument                :: [String]
   , doubleStaff               :: [Int] --die Systemteile, die Pianomusikin zwei Systemen haben
                                          --(bekommen eine geschweifte Klammer)
   , taktstrichart             :: Taktstrich
   , anzahlzeilenersteseite    :: Int
   , anzahlzeilenweitereseiten :: Int
   , teiler                    :: Int
   , artab                     :: Int
   , theGrace                  :: Grace
   , notecontext               :: [NoteContext]
   , beamDirection             :: [StemDirection]
   , zeilenlaenge              :: Int
   , seitennr                  :: Int
   , systemanzahl              :: Int
   , linienabstand             :: Int
   , keyName                   :: String
   , theStimme                 :: Int
   , currentVoice              :: Int
   , currentWert               :: (Int,Int)
   , currentMod                :: Int
   , beamUp                    :: Bool
   , taktNummer                :: Int
   , openUpper                 :: Maybe String
   , openLower                 :: Maybe String
   , klammer                   :: Maybe Int
   , segno                     :: Bool
   , snippet                   :: Bool
   }


modList n e xs = pre++e:ost
 where
   (pre, (_:ost)) = splitAt (n-1) xs

changeSchluessel n clef s = s{schluessel=modList n clef $schluessel s}

data Annotation
 = UpperAnnotation String (Int->Int->Int) (Int->Int->Int)
  |LowerAnnotation String (Int->Int->Int) (Int->Int->Int)

getEnd  (UpperAnnotation _ _ e) = e
getEnd  (LowerAnnotation _ _ e) = e

isUpperAnnotation (UpperAnnotation _ _ _) = True
isUpperAnnotation _ = False

data Schluessel = Violin|Bass|Tenor deriving (Eq,Show)
data Taktstrich = Normal|Ende|EndeWiederholung|EndeAnfangWiederholung|AnfangWiederholung|Doppel
data NoteContext = Head|Note deriving Eq

data StemDirection=Up|Down|Automatic|NoStem deriving (Eq,Show)

--getKeyChar:: Schluessel -> Int
getKeyChar Violin = PutSopranClef
getKeyChar Bass = PutBassClef --bassKey
getKeyChar Tenor = PutTenorClef --tenorKey

initialglob :: Global
initialglob 
 = Global 
    {title="" 
    ,subtitle="" 
    ,composer="" 
    ,tonartsymbol=CDur 
    ,schluessel=[Violin] 
    ,instrument=["guitar"]
    ,doubleStaff = []   
    ,taktstrichart=Normal 
    ,anzahlzeilenersteseite=9 
    ,anzahlzeilenweitereseiten=8
    ,teiler= 30 
    ,artab=0 
    ,theGrace=_Normal 
    ,notecontext=[Note]
    ,beamDirection=[Automatic] 
    ,zeilenlaenge=langl 
    ,seitennr=1 
    ,systemanzahl=1 
    ,linienabstand= deflinienab 
    ,keyName="cdur" 
    ,theStimme=0 
    ,currentVoice=1 
    ,currentWert=(1,4) 
    ,currentMod=1 
    ,beamUp= True 
    ,taktNummer=(1)
    ,openUpper=Nothing
    ,openLower=Nothing
    ,klammer = Nothing
    ,segno=False
    ,snippet=False
    }


type GenElem =  Global -> (DVIelem,Global)

result :: DVIelem -> GenElem
result dvielem = \s -> (dvielem,s)

seqs [x] s = (\(y,z)->([y],z)) $x s 
seqs (x:xs) s = ((r:rs),s3)
 where
   (r,s2)  = x s
   (rs,s3) = seqs xs s2


data DVIelem 
 = DVIelem
   { maxbreite  :: Int             --maximal und minimal moegliche  
   , minbreite  :: Int             --Breite des Elements
   , minzw      :: Int             --minimaler folgender Zwischenraum
   , maxzw      :: Int             --maximaler folgender Zwischenraum
   , zwischenr  :: Int ->Int -> Int  --anschliessenden Zwischenraum zu Faktorisieren
   , hoehe      :: Int             --von e linie in Halbtonhoehe hoechste    
   , tiefe      :: Int             --und tiefster Punkt des Elements
   , offset     :: Int             --Laenge der Vorzeichen
   , schlaege   :: [(Int,Int)]     --Anzahl der Taktschlaege fuer Mehrstimmigkeit
   , ref_hoehe  :: Int             --Hauptton des Elements
   , parts      :: Cluster         --Teilelemente   
   }

instance Show DVIelem where    
   show dv = "DVIelem maxbr: "++(show$maxbreite dv)
    ++"  minbr: "++(show$minbreite dv) ++"  minzw: "++(show$minzw dv)
    ++"  minzw: "++(show$maxzw dv)
    ++"\n : "++(show$parts dv)
        
initdvielem :: DVIelem
initdvielem 
 = DVIelem
    {maxbreite=0 
    ,minbreite=0 
    ,minzw=0 
    ,maxzw=0 
    ,zwischenr=(\z n ->0) 
    ,hoehe=0 
    ,tiefe=0 
    ,offset=0 
    ,schlaege=[] 
    ,ref_hoehe=0 
    ,parts=Null
    }

initgenelem :: GenElem
initgenelem = result initdvielem

data Part  
 = Part
   { poffset     :: Int
   , pwoliegt    :: Int -> Int -> Int
   , ptiefe      :: Int
   , phoehe      :: Int
   , pvorsatznotation::ONotation
   , pnotation   :: Int -> Int -> ONotation
   , pschlag     :: (Int,Int)
   , pvorsatzart :: [(Element,Int)]
   , ppostdefs :: [PostInfo]
   , postProcessNotation :: ( Int -> Int -> ONotation) ->  (Int -> Int -> ONotation)
   , def         :: Element  --eine Definition des Elements 
   , pmaxbr  :: Int          --maximal und minimal moegliche  
   , pminbr  :: Int          --Breite des Elements
   , pneedsStem  :: Bool     --braucht das Element einen Hals=
   , startUpperAnno :: Maybe String
   , startLowerAnno :: Maybe String
   , endLower  :: Bool
   , endUpper  :: Bool
  } 

instance (Show Part) where
  show p = -- "Part: offset: "++(show$poffset p)++" wo: "++(show$pwoliegt p 1 1)++(show$pschlag p)  
    show$def p

initpart :: Part  
initpart 
 =Part
   { poffset      = 0  --das ist wohl Vorschlag etc
   , pwoliegt     = \z n -> 0 --hier fängt es global an
   , ptiefe       = 0
   , phoehe       = 0
   , pvorsatznotation  = onotation []
   , pnotation         = \z n ->onotation []
   , pschlag      = (0,1) --abort "undefinierter Schlag in part"
   , pvorsatzart  = []
   , ppostdefs  = []
   , postProcessNotation  = id
   , def = NoElement
   , pmaxbr = 0
   , pminbr = 0
   , pneedsStem=True
   , startUpperAnno = Nothing
   , startLowerAnno = Nothing 
   , endLower  = False
   , endUpper  = False}

data Anno =  StartUpper String (Int->Int->Int)
            |StartLower String (Int->Int->Int)
            |EndLower (Int->Int->Int)
            |EndUpper (Int->Int->Int)

instance Show Anno where
  show (StartUpper s _) = "(StartUpper "++s++")"
  show (StartLower s _) = "(StartLower "++s++")"
  show (EndLower _) = "(EndLower )"
  show (EndUpper _) = "(EndUpper )"

aLower (StartLower _ _) = True
aLower (EndLower _ ) = True
aLower _ = False

whereIsAnno (StartLower _ x) = x
whereIsAnno (StartUpper _ x) = x
whereIsAnno (EndUpper  x) = x
whereIsAnno (EndLower  x) = x

getAnnos x 
 =   maybe [] (\y->[StartUpper y (pwoliegt x)]) (startUpperAnno x) 
  ++ maybe [] (\y->[StartLower y (pwoliegt x)]) (startLowerAnno x) 
  ++ if (endLower x) then [EndLower (pwoliegt x)]else []
  ++ if (endUpper x) then [EndUpper (pwoliegt x)]else []

collectAnnos = concat.(map getAnnos ).fflat

pwoliegtwirklich p = \z n-> pwoliegt p z n+poffset p

data GenCluster a 
    =  Seq  [GenCluster a]
      |Alt  [GenCluster a]
      |Term a
      |Modi [PostInfo]  (GenCluster a)   
      |Null   deriving Show

type Cluster = GenCluster Part

fflat (Seq  xs) = concat (map fflat xs) 
fflat (Alt  xs) = concat (map fflat xs) 
fflat (Term  xs) = [xs] 
fflat (Modi _ x)= fflat x
fflat Null = []

getTerm (Term a) = a
getTerm (Modi _  a) = getTerm a
getTerm _ = error "no Term found"

isNull :: Cluster -> Bool
isNull Null = True
isNull _    = False

singleHead :: Cluster -> Part
singleHead (Term p)     = p
singleHead (Seq (x:xs)) = singleHead x
singleHead (Alt (x:xs)) = singleHead x
singleHead (Modi _ c)   = singleHead c
singleHead _            = error "singleHead: no part in cluster"

infixr 5 ++++

(++++):: Cluster  -> Cluster -> Cluster
(++++) Null xs = xs
(++++) xs Null = xs
(++++) (Seq xs) (Seq ys) = Seq (xs++ys)
(++++) (Seq xs) c = Seq (xs++[c])
(++++) c (Seq xs) = Seq (c:xs)
(++++) c1 c2      = Seq [c1,c2]

flength :: Cluster -> Int
flength (Seq xs)  = length xs
flength (Null)    = 0
flength (Modi _ c)= flength c
flength _         = 1



getSeq (Seq xs)    = concat (map getSeq xs)
--getSeq (Alt xs)    = concat (getSeq xs)
getSeq (Null)      = []
getSeq (Term x)    = [x]
getSeq (Modi _ c)= getSeq c
--getSeq x           = [x]


sel n (Seq ls)  
 |n<=0      = Left "negativer Index"
 |otherwise = sell n ls
sel _ _     = Left "slection not from sequence"  

sell _ []     = Left "zu grosser Index"
sell 1 (x:_)  = Right (singleHead x)
sell n (_:xs) = sell (n-1) xs

lastInCluster (Seq ls)  =  Right$singleHead$last ls
lastInCluster x         = Right (singleHead x)



clusterMap :: (a -> b) -> (GenCluster a) -> GenCluster b
clusterMap f (Seq  xs)   = Seq (map (clusterMap f) xs)
clusterMap f (Alt  xs)   = Alt (map (clusterMap f) xs)
clusterMap f (Term x)    = Term (f x)
clusterMap f (Modi bs  c) = (Modi bs (clusterMap f c))   
clusterMap f Null        = Null

clusterGet :: Int -> (GenCluster a) -> GenCluster a
clusterGet i (Seq  xs)   = xs!! (i-1)
clusterGet i (Alt  xs)   = clusterGet i (head xs)
clusterGet i (Term x)    = Term  x
-- |i==1= Term  x
-- |otherwise = error "index to high on clusterGet"
clusterGet i (Modi _ c) = clusterGet i c   
clusterGet i Null        = Null

--fmap :: (Part -> Part) -> Cluster -> Cluster

instance Functor GenCluster where
  fmap f (Seq cs)    = Seq (map (fmap f) cs)
  fmap f (Alt cs)    = Alt (map (fmap f) cs)
  fmap f (Term p)    = Term (f p)
  fmap f (Modi bs  c) = Modi bs (fmap f c) 
  fmap _ Null        = Null
      
changeTail :: (Part -> Part) -> Cluster -> Cluster
changeTail  f (Seq (c:cs))= Seq (c:(map (fmap f) cs))
changeTail  f (Seq [])    = Seq []
changeTail  f (Alt (c:cs))= Alt (c:(map (fmap f) cs))
changeTail  f (Alt [])    = Alt []
changeTail  f (Term p)    = Term p
changeTail  f (Modi bs  c) = Modi bs  (changeTail f c) 
changeTail  _ Null        = Null

changeHead :: (Part -> Part) -> Cluster -> Cluster
changeHead  f (Seq (c:cs))= Seq (changeHead f c:cs)
changeHead  f (Seq [])    = Seq []
changeHead  f (Alt (c:cs))= Alt (changeHead f c:cs)
changeHead  f (Alt [])    = Alt []
changeHead  f (Term p)    = Term (f p)
changeHead  f (Modi bs  c) = Modi bs  (changeHead f c) 
changeHead  _ Null        = Null

clusterHead :: Cluster -> Part
clusterHead   (Seq (c:cs))= clusterHead c
clusterHead   (Alt (c:cs))= clusterHead c
clusterHead   (Modi bs  c) = clusterHead c
clusterHead   (Term p)    = p

flattenCluster :: (GenCluster a) -> [a]
flattenCluster (Seq xs)    =  concat (map flattenCluster xs)
flattenCluster (Alt xs)    = concat (map flattenCluster xs)
flattenCluster (Term p)    = [p]
flattenCluster (Modi _ c) = flattenCluster c
flattenCluster Null        = []

data PostInfo
    =   Balkoat Int [(Int,(Int,Int),Int,Int)]
       |Balkuat Int [(Int,(Int,Int),Int,Int)]
       |Balkouat Int [(Int,(Int,Int),Int,Int)] [(Int,(Int,Int),Int,Int)]
       |Sluroat Int (Int,Int) (Int,Int) 
       |Sluruat Int (Int,Int) (Int,Int) 
       |Crescat Int Int Int
       |Decrescat Int Int Int
       |Glissando Int Int
       |Triller Int Int
  deriving Show


data Tonart 
 = CDur|GDur|DDur|ADur|EDur|HDur|FisDur|FDur|BDur|EsDur|AsDur|DesDur|GesDur
 deriving (Eq,Show)

instance Ord Tonart
  where
    (<) CDur _ = True
    (<) _ CDur = False
    (<) lhs rhs 
        |not (kreuztonart lhs == kreuztonart rhs) = False
        |otherwise = (vorzeichenanzahl lhs) < (vorzeichenanzahl rhs)        


kreuztonart  :: Tonart -> Bool
kreuztonart  GDur = True
kreuztonart  DDur = True
kreuztonart  ADur = True
kreuztonart  EDur = True
kreuztonart  HDur = True
kreuztonart  FisDur = True
kreuztonart  _ = False

vorzeichenanzahl:: Tonart -> Int
vorzeichenanzahl CDur = 0
vorzeichenanzahl GDur = 1
vorzeichenanzahl DDur = 2
vorzeichenanzahl ADur = 3
vorzeichenanzahl EDur = 4
vorzeichenanzahl HDur = 5
vorzeichenanzahl FisDur = 6
vorzeichenanzahl FDur = 1
vorzeichenanzahl BDur = 2
vorzeichenanzahl EsDur = 3
vorzeichenanzahl AsDur = 4
vorzeichenanzahl DesDur = 5
vorzeichenanzahl GesDur = 6

changeTaktZahl :: (Int -> Int) -> GenElem -> GenElem
changeTaktZahl f genElem
 = res 
   where
     res g = (dvielem,g2{taktNummer = f (taktNummer g2)})
      where
       (dvielem,g2) = genElem g

data Notation
 = PushPos
  |PopPos
  |MoveRight Int
  |MoveDown Int
  |PutLine Int (Int,Int)
  |PutBeam Int (Int,Int)
  |PutStaffBar Int 
  |PutBrace Int
  |PutNoteHead
  |PutNoteHead1
  |PutNoteHead2
  |PutSharp
  |PutFlat
  |PutDoubleSharp
  |PutDoubleFlat
  |PutNatural
  |Put8FlagUp
  |Put16FlagUp
  |Put32FlagUp
  |Put64FlagUp
  |Put8FlagDown
  |Put16FlagDown
  |Put32FlagDown
  |Put64FlagDown
  |PutRest64
  |PutRest32
  |PutRest16
  |PutRest8
  |PutRest4
  |PutRest2
  |PutRest1

  |PutGraceNoteHead
  |PutGraceNoteHead1
  |PutGraceNoteHead2
  |PutGraceSharp
  |PutGraceFlat
  |PutGraceDoubleSharp
  |PutGraceDoubleFlat
  |PutGraceNatural
  |PutGrace8FlagUp
  |PutGrace16FlagUp
  |PutGrace32FlagUp
  |PutGrace64FlagUp
  |PutGrace8FlagDown
  |PutGrace16FlagDown
  |PutGrace32FlagDown
  |PutGrace64FlagDown
  |PutGraceRest64
  |PutGraceRest32
  |PutGraceRest16
  |PutGraceRest8
  |PutGraceRest4
  |PutGraceRest2
  |PutGraceRest1

  |PutTriller
  |PutTrillerSlur
  |PutSopranClef
  |PutBassClef
  |PutTenorClef
  |PutSlurDown Int Int
  |PutSlurUp Int Int
  |PutMetrum4_4
  |PutMetrum Int Int
  |PutText String String
  |PutDot
  |PutDotDot
  |PutStaccato
  |PutSforzato
  |PutFermateo
  |PutFermateu
  |PutTenuto
  |PutArpeggio
  |PutStaccatissimoo
  |PutStaccatissimou
  |PutMarcatoo
  |PutMarcatou
  |PutSegno


newtype ONotation   = ON ([Notation] -> [Notation])

instance (Ap ONotation) where
  (+++) (ON odvi1) (ON odvi2) = ON (odvi1 . odvi2)

onotation :: [Notation] -> ONotation
onotation s = ON (\x-> (s++x))

notation :: ONotation -> [Notation]
notation (ON s) = s []


apON n1 n2 z n = (n1 z n)+++(n2 z n)
