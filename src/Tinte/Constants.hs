module Tinte.Constants where

version ="0.01"

kurzl       = langl - indent
indent      = 2621440
langl      = 30884166 ::Int
--pslangl=1080705 ::Int

toPSMeasure:: Int->Float
toPSMeasure = \x->(fromInteger$toInteger x)/316882.95

halbtonab::Int
halbtonab = 163840 ::Int

balkendicke    = halbtonab


notendicke    = 365000 ::Int
gracenotendicke    = div (365000*13) 20
ganznotendicke  = 375000
graceganznotendicke  = div (375000*13) 20

isdicke      =ganznotendicke+100000
esdicke      =ganznotendicke
nadicke      =425000 :: Int

graceisdicke    =div (graceganznotendicke*13) 20
graceesdicke    =div (graceganznotendicke*13) 20
gracenadicke    =div (425000*13) 20

defab      =  6*halbtonab

strichdicke    = 19660::Int

hilfsoffset    = 115000 :: Int
hilslaenge    = 655360 ::Int

gracehilfsoffset  = div (115000*13) 20
gracehilslaenge    = div (655360*13) 20


halsdicke    = 2*strichdicke

halslaenge    = 6*halbtonab--1146880

achtfahnenhoehe  = 122880
fahnenhoehe32  = 112880
fahnenhoehe64  = 102880

gracehalslaenge    = div (6*halbtonab*13) 20--1146880
graceachtfahnenhoehe  = div (122880*13) 20
gracefahnenhoehe32  = div (112880*13) 20
gracefahnenhoehe64  = div (102880*13) 20

takthoehe    = 1320550::Int

staffabstand    = 3*takthoehe :: Int



f8breite    = notendicke
f16breite    = notendicke
f32breite    = notendicke
f64breite    = notendicke
f8hoehe      = 6* halbtonab
f16hoehe    = 7* halbtonab
f32hoehe    = 8* halbtonab
f64hoehe    = 9* halbtonab

p1breite    = notendicke
p2breite    = notendicke
p4breite    = notendicke
p8breite    = notendicke
p16breite    = notendicke
p32breite    = notendicke
p64breite    = notendicke

gracef8breite    = gracenotendicke
gracef16breite    = gracenotendicke
gracef32breite    = gracenotendicke
gracef64breite    = gracenotendicke
gracef8hoehe    = div (6* halbtonab*13) 20
gracef16hoehe    = div (7* halbtonab*13) 20
gracef32hoehe    = div (8* halbtonab*13) 20
gracef64hoehe    = div (9* halbtonab*13) 20

gracep1breite    = gracenotendicke
gracep2breite    = gracenotendicke
gracep4breite    = gracenotendicke
gracep8breite    = gracenotendicke
gracep16breite    = gracenotendicke
gracep32breite    = gracenotendicke
gracep64breite    = gracenotendicke

deflinienab    =1010720*5 :: Int

schluesselab  = 2401531 :: Int


steigungenpos 
 = [  (20,1),(20,2),(20,3),(20,4),(20,5),(20,6),
    (20,7),(20,8),(20,9),(20,10)]::[(Int,Int)]--,(20,11)]

steigungenneg
 = [  (20,-1),(20,-2),(20,-3),(20,-4),(20,-5),(20,-6),
    (20,-7),(20,-8),(20,-9),(20,-10)]::[(Int,Int)]--,(20,-11)]  

steigungen
 = ((1,0):steigungenpos++steigungenneg)


balkenweitemin = div (3*halbtonab) 4

bogenweitemin  = div (19*halbtonab) 10
bogengerademin  =  div (4*halbtonab) 3

gracebalkenweitemin = div (div (3*halbtonab)(4*13)) 20

gracebogenweitemin  = div (div (22*halbtonab) (10*13)) 20
gracebogengerademin  =  div (div (3*halbtonab) (2*13)) 20



dickstrich     = 6*strichdicke

doppelpunktbreite::Int
doppelpunktbreite =  div (12*halbtonab) 10


getcc :: (Int,Int) -> Int
getcc (_,i) 
  |i<0&&i>=(-10)   = ((abs i)-1)*6
  |i>0&&i<=(10)  = (i-1)*6+64
--  |i == 11  = 9*6+64
--  |i == -11  = 9*6
  |otherwise  = 0


mincres = 5*notendicke


sts =[minsts,st1,st2,st3,st4,st5,st6,st7]++(repeat st8)

minsts=  repeat bogenweitemin

st1 = [17,0,16,48,1,65,2,66,3,67,4,68,5,69,6,70,7]++repeat 71
st2 = [17,8,24,56,9,73,10,74,11,75,12,76,13,77,14,78,15] ++ repeat 79
st3 = [17,32,64,17,80,18,81,19,82,20,83,21,84,22,85,23,86]++ repeat 87
st4 = [17,40,72,25,88,26,89,27,90,28,91,29,92,30,93,31,94]++repeat 95
st5 = [16,80,33,97,34,98,35,99,36,100,37,101,48,102,39,103]++ repeat 39
st6 = [14,41,105,42,106,43,107,44,108,45,109,46,110,47] ++ repeat 111 
st7 = [14,49,113,50,114,51,115,52,116,53,117,54,118,55] ++ repeat 119
st8 = [14,57,121,58,122,59,123,60,124,61,125,62,126,63] ++ repeat 127

grace = 13
music = 20

type Grace   = Bool

_Grace     = True
_Normal    = False

violinKey = 71::Int
bassKey = 73::Int
tenorKey = 75::Int
ganzerKopf = 35::Int
halberKopf = 34::Int
viertelKopf = 33::Int
isSymbol = 52::Int
esSymbol = 50::Int
naSymbol = 54::Int
isisSymbol = 53::Int
esesSymbol = 51::Int
crescendoSymbolStart = 20::Int
decrescendoSymbolStart = 16::Int


fnttext      = 3::Int
fntNormal    = 14::Int
fntmusic    = 15::Int
fntbalken    = 16::Int
fntbogengerade    = 17::Int
fntbogenuntenh    = 18::Int
fntbogenuntenr    = 19::Int
fntbogenobenh     = 20::Int
fntbogenobenr     = 21::Int
fntgracemusic    = 22::Int
fntgracebalken    = 23::Int
fntgracebogengerade  = 24::Int
fntgracebogenuntenh  = 25::Int
fntgracebogenuntenr  = 26::Int
fntgracebogenobenh   = 27::Int
fntgracebogenobenr   = 28::Int
fntanweisung    = 30::Int
fntfinger          = 31::Int
fntdynamic    = 32::Int


fntNumberToPSFont fnt
 |fnt==fntdynamic   = "/feta-alphabet20  findfont 7.029296875 scalefont setfont"
 |fnt==fntfinger    = "/CenturySchL-Roma  findfont 3.0673828125 scalefont setfont"
 |fnt==fntNormal    = "/CenturySchL-Roma  findfont 3.865234375 scalefont setfont"
 |otherwise         = "/CenturySchL-Roma  findfont 3.0673828125 scalefont setfont"

metrummin  = ganznotendicke+ganznotendicke ::Int
metrummax  = ganznotendicke+ganznotendicke  ::Int
n1min  = ganznotendicke+3*ganznotendicke ::Int
n1max  = ganznotendicke+10*ganznotendicke  ::Int
n2min  = ganznotendicke+3*ganznotendicke ::Int
n2max  = ganznotendicke+8*ganznotendicke ::Int
n4min  = notendicke+2*notendicke ::Int
n4max  = notendicke+8*notendicke ::Int
n8min  = notendicke+1*notendicke ::Int
n8max  = notendicke+5*notendicke ::Int
n16min = notendicke+div (notendicke) 2 ::Int
n16max = notendicke+3*notendicke ::Int
n32min = notendicke+div notendicke 3 ::Int
n32max = notendicke+2*notendicke ::Int
n64min = notendicke+div notendicke 6 ::Int
n64max = notendicke+notendicke ::Int


p1min  = 5*notendicke
p2min  = 2*notendicke
p4min  = 3*notendicke
p8min  = 2*notendicke+div notendicke 2
p16min = 2*notendicke
p32min = 2*notendicke
p64min = 2*notendicke
p1max  = 15*notendicke
p2max  = 13*notendicke
p4max  = 9*notendicke
p8max  = 6*notendicke
p16max = 6*notendicke
p32max = 4*notendicke
p64max = 4*notendicke






