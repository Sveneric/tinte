module Tinte.Lib where


clusterBy eq (x:xs)
  = (((x:[y|y<-xs,eq y x]) : clusterBy eq [y|y<-xs,not $ eq y x]))
clusterBy _ [] = []

toOrdering True = LT
toOrdering _ = GT


