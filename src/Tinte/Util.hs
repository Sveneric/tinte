module Tinte.Util where
import Debug.Trace
import Tinte.Data
import Tinte.Constants
import Tinte.Staff


liftd n alt
 = alt
   { tiefe     = atiefe+n
   , hoehe     = ahoehe+n
   , ref_hoehe = aref+n
   , parts     = fmap (li n (minbreite alt) (maxbreite alt)) aparts 
   }
  where
    aparts  = parts alt
    atiefe  = tiefe alt
    ahoehe  = hoehe alt
    aref  = ref_hoehe alt

lift :: Int -> GenElem -> GenElem
lift n alt s =  (liftd n neu,s1) 
    where
      (neu,s1) = alt s

li n  mmin mmax p
  = p{pnotation 
        = \z n ->
          quantmitnotation 
           (onotation [PushPos,MoveDown h]+++pnotation p z n+++onotation[PopPos]) 
           mmin mmax z n
     , pvorsatznotation = onotation [PushPos,MoveDown h]+++pvorsatznotation p+++onotation [PopPos]
     }
  where
    h    = n*halbtonab



verschiebe :: (Int -> Int -> Int) -> Part -> Part
verschiebe ver part
  = part 
    { pwoliegt = \z n -> (wo z n) + (ver z n)
    }
    where
      wo = pwoliegt part

setWoliegt :: (Int -> Int -> Int) -> Int ->Part -> Part
setWoliegt wo newOffset part
  = part 
    { pwoliegt = -- trace ("da lag:" ++(show$pwoliegt part 1 1))
                 (\z n -> (wo z n))
    , pvorsatznotation= onotation [PushPos
                             ,MoveRight correctOffset]+++oldVorsatznota+++onotation[PopPos]
    , poffset = newOffset
      }
    where
      oldOffset = poffset part
      oldVorsatznota = pvorsatznotation part
      correctOffset
        |newOffset>oldOffset = newOffset-oldOffset        
        |otherwise = 0
 
ohnzw :: GenElem -> GenElem
ohnzw alts = o alts
 where
  o alts s = (alt {
        maxbreite = maxb-maxz, 
        minbreite = minb-minz,
        maxzw    = 0,
        minzw    = 0,
          zwischenr = \ z n -> 0},s1)
   where
   (alt,s1) = alts s
   maxb = maxbreite alt
   minb = minbreite alt
   maxz = maxzw alt
   minz = minzw alt
   zwi = zwischenr alt

is :: GenElem -> GenElem
is alt =  vorsatz PutSharp isSymbol  ( isdicke) Is alt

isis :: GenElem -> GenElem
isis alt =  vorsatz PutDoubleSharp isisSymbol (isdicke) Isis alt


eses alt =  vorsatz PutDoubleFlat esesSymbol (2*esdicke) Eses alt

vorsatz nota chr dicke art alts s 
 |isNull (parts alt) = (alt,s1)
 |otherwise
   = ( alt 
       { maxbreite = dicke+maxbreite alt 
       , minbreite = dicke+minbreite alt
       , parts     = clus2}
     , s1)
  where
   (alt,s1) = alts s
   ref      = ref_hoehe alt
   clus     = parts alt
   clus1    = changeTail (verschiebe (\z n->dicke)) clus
   clus2    = changeHead makeVorsatz clus1
   makeVorsatz p 
    = p 
      { poffset     = dicke+poff
      , pvorsatzart = ((art (error "in is, darf es nie geben"),ref):vorsatza)
      , pvorsatznotation
         = (onotation [PushPos,MoveDown (ref*halbtonab),nota,PopPos,MoveRight dicke])+++pvorsatznotation p
      }
      where
        poff     = poffset p
        vorsatza = pvorsatzart p

es :: GenElem -> GenElem
es alt = vorsatz PutFlat esSymbol ( esdicke) Es alt

na :: GenElem -> GenElem
na alt = vorsatz PutNatural naSymbol ( nadicke) Na alt

nachrechts :: Int -> Int -> GenElem
nachrechts min max  
 = result initdvielem {maxbreite = max,  
          minbreite  = min,
        minzw     = min,        
        maxzw     = max,        
        zwischenr  = quant min max
        }

smallMod :: GenElem -> GenElem
smallMod elem = changeFntSize elem _Grace fntgracemusic

normalMod :: GenElem -> GenElem
normalMod elem = changeFntSize elem _Normal  fntmusic

changeFntSize genF gr fntNumber
  = genF 

stimme :: Int ->  GenElem -> GenElem
stimme n genElem  = addTheStimmInfo n getStimme 
  where
    getStimme glob 
      |theStimme glob == 0 =  (staff (systemanzahl glob +1 -n) genElem) gl
      |n >= theStimme glob  && n < theStimme glob+systemanzahl glob = (staff (systemanzahl glob +theStimme glob -n) genElem) gl
      |otherwise = (initdvielem,gl)
     where
      gl = glob{currentVoice=n}

addTheStimmInfo:: Int ->  GenElem -> GenElem
addTheStimmInfo st gen  glob
  = (el{parts=pan},g)
   where
     (el,g) = gen glob
     pa= parts el
     pan = fmap (\p->p{def=Stimme (instrument glob !! (st-1)) (def p)}) pa


vorzeichenNota:: Schluessel -> Tonart -> ONotation
vorzeichenNota _ CDur = cdurNota
vorzeichenNota Violin GDur = gdurNota
vorzeichenNota Tenor GDur = gdurNotaTenor
vorzeichenNota Bass GDur = gdurNotaBass
vorzeichenNota Violin DDur = ddurNota
vorzeichenNota Violin ADur = adurNota
vorzeichenNota Violin EDur = edurNota
vorzeichenNota Violin HDur = hdurNota
vorzeichenNota Violin FisDur = fisdurNota
vorzeichenNota Violin FDur = fdurNota
vorzeichenNota Violin BDur = bdurNota
vorzeichenNota Violin EsDur = esdurNota
vorzeichenNota Violin AsDur = asdurNota
vorzeichenNota Violin DesDur = desdurNota
vorzeichenNota Violin GesDur = gesdurNota


tonartbreite:: Tonart -> Int
tonartbreite key = vorzeichenLaengenBerechnung (vorzeichenanzahl key )

vorzeichenLaengenBerechnung :: Int -> Int
vorzeichenLaengenBerechnung i = div (i * 4 *  isdicke) 5

cdurNota = onotation []

gdurNotaTenor 
 = onotation
    [PushPos
    ,MoveRight (4161531- schluesselab)
    ,MoveDown ((-1310720)+3*halbtonab)
    ,PutSharp
    ,PopPos
    ]

gdurNotaBass 
 = onotation
    [PushPos
    ,MoveRight (4161531- schluesselab)
    ,MoveDown ((-1310720)+4*halbtonab)
    ,PutSharp
    ,PopPos
    ]


gdurNota 
 = onotation
    [PushPos
    ,MoveRight (4161531- schluesselab)
    ,MoveDown ((-1310720)+2*halbtonab)
    ,PutSharp
    ,PopPos
    ]

ddurNota 
 = onotation
    [PushPos
    ,MoveRight (4161531- schluesselab)
    ,MoveDown ((-1310720)+2*halbtonab)
    ,PutSharp
    ,MoveRight isdicke 
    ,MoveDown (3*halbtonab)
    ,PutSharp
    ,PopPos
    ]

adurNota
 = onotation 
    [PushPos,
    MoveRight (4161531- schluesselab),
    MoveDown (-1310720+2*halbtonab),
    PutSharp,
    PopPos,
    PushPos,
    MoveRight (4476103- schluesselab),
    MoveDown (-819200+2*halbtonab),
    PutSharp,
    PopPos,
    PushPos,
    MoveRight (4790675- schluesselab),
    MoveDown (-1474560+2*halbtonab),
    PutSharp,
    PopPos]


edurNota = onotation 
    [PushPos,
    MoveRight (4161531- schluesselab),
    MoveDown (-1310720+2*halbtonab),
    PutSharp,
    PopPos,
    PushPos,
    MoveRight (4476103- schluesselab),
    MoveDown (-819200+2*halbtonab),
    PutSharp,
    PopPos,
    PushPos,
    MoveRight (4790675- schluesselab),
    MoveDown (-1474560+2*halbtonab),
    PutSharp,
    PopPos,
    PushPos,
    MoveRight (5105147- schluesselab),
    MoveDown (-983040+2*halbtonab),
    PutSharp,
    PopPos
    ]



vorzRespectSchluesselNota od 
 =  onotation ( [PushPos, MoveRight (4161531- schluesselab)])+++od+++onotation ( [PopPos])


flatsNota xs = vorzRespectSchluesselNota (setFlatsNota xs)
sharpsNota xs = vorzRespectSchluesselNota (setSharpsNota xs)



fdurNota = flatsNota (take 1 flatPositions)
bdurNota = flatsNota (take 2 flatPositions)
esdurNota = flatsNota (take 3 flatPositions)
asdurNota = flatsNota (take 4 flatPositions)
desdurNota = flatsNota (take 5 flatPositions)
gesdurNota = flatsNota (take 6 flatPositions)


hdurNota = sharpsNota (take 5 sharpPositions)


fisdurNota = sharpsNota (take 6 sharpPositions)


flatPositions = [-4,-7,-3,-6,-2,-5]
sharpPositions = [-8,-5,-9,-6,-3,-7]


setzeeinvorzeichenNota dies hier
  = onotation ([PushPos, MoveDown ((hier)*halbtonab)]++(dies:[PopPos,  MoveRight 315672]))

setzeeinvorzeichenNotaflat dies hier
  = onotation ([PushPos, MoveDown ((hier+2)*halbtonab)]++(dies:[PopPos, MoveRight 315672]))



setzeVorzsNota diesen xs 
 = foldr (+++) (onotation ([])) (map (setzeeinvorzeichenNota diesen) xs)


setzeFlatVorzsNota diesen xs 
 = foldr (+++) (onotation ([])) (map (setzeeinvorzeichenNotaflat diesen) xs)


setSharpsNota xs = setzeVorzsNota PutSharp xs
setFlatsNota xs = setzeFlatVorzsNota PutFlat  xs
setNatsNota xs = setzeVorzsNota PutNatural  xs


clearFlatsFromToNota f t = setNatsNota (take (t-f) (drop f flatPositions))


clearSharpsFromToNota f t = setNatsNota (take (t-f) (drop f sharpPositions))

--  Berechnung des Versatzes fuer mehrsystem Partituren
putatstaff :: Int -> Int
putatstaff i 
 |i==1 = 0 
 |otherwise = ((i-1)*( staffabstand))
            --  + takthoehe
              
-- ////////Zeichnen von beliebigen Linien
-- ////////////////////////////////////

zeichnebeamnota w st@(xd,yd) 
 |yd == 0   = onotation [PushPos,PutBeam balkendicke  (w,0),PopPos]
 |otherwise = onotation [PushPos,PutBeam balkendicke (w,w `div` xd * yd),PopPos]
  where
    firstc = getcc st

minirule xd yd xnow ynow 
  |xnow>=xd    = (xnow,ynext)
  |abs(ynow-ynext)<5000  = minirule xd yd (xnow+5000) ynow
  |otherwise    = (xnow,ynext)
    where
--      ynextr  :: Real
      ynextr  = div ( yd* xnow) xd
      ynext  =  ynextr


-- //zum Quantifizieren mit dem Koeffizienten    



quantmitnotation odv min max zaehler nenner
  =  odv +++ (onotation [MoveRight (quant min max zaehler nenner)])


quantnachnotation odv min max zaehler nenner
 = onotation [PushPos]+++odv+++onotation [PopPos]+++ (onotation ([MoveRight (quant min max zaehler nenner)]))


quant:: Int -> Int -> Int -> Int ->Int 
quant min max zaehler nenner 
 = (min +  round aux)
     where
       aux::Rational
       aux |koeffn==0 = 1 |otherwise=  (((toRational  max- toRational min)*koeffz))/koeffn
       koeffz :: Rational
       koeffz =  toRational  zaehler
       koeffn :: Rational
       koeffn =   (toRational  nenner)



abstandFuer:: Int -> Int -> (Int -> Int -> Int)
abstandFuer z n = quant mi ma
  where (mi,ma)= abstandFuer2 z n 


abstandFuer2 z n 
 |tausendstel>= 990 =   (n1min, n1max)
 |tausendstel>= 490 =   (n2min, n2max)
 |tausendstel>= 240 =   (n4min ,n4max)
 |tausendstel>= 120 =   (n8min ,n8max)
 |tausendstel>= 60 =   (n16min ,n16max)
 |tausendstel>= 30 =   (n32min, n32max)
 |otherwise =  ( n64min, n64max)
 where
   tausendstel = div (z*1000) n


pairsum :: [(Int,Int)] -> (Int,Int)
pairsum xs = foldr addpair (0,0) xs

addpair (a1,a2) (b1,b2) = (a1+b1,a2+b2)



zipwithtail [] = []
zipwithtail (x:xs) = ((x,xs):zipwithtail xs)


