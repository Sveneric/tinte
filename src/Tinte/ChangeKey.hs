module Tinte.ChangeKey where
import Debug.Trace
import Tinte.Data
import Tinte.Util
import Tinte.Constants


changeclef clef s
  | currentClef==clef = (initdvielem,s)
  | otherwise   =  
      (initdvielem 
        {parts    = Term pa2
        ,maxbreite = schluesselab
        ,minbreite = 0 --schluesselab
        }
      ,changeSchluessel (if v==0 then 1 else v) clef s)
   where
     v= currentVoice  s
     currentClef
       |v== 0 = head$schluessel s
       |otherwise = (schluessel s) !! (v-1)
     pa2=initpart
         {pschlag    = (0,4)
         ,pnotation=nota2
         }
     nota2 
      = \z n ->
               onotation ( [MoveDown (-2*halbtonab),MoveRight (-notendicke),getKeyChar clef, MoveRight (schluesselab) ])


changekey key s
  =  
      (makeKeyTransition (tonartsymbol s) key
      ,s{tonartsymbol=key  --,tonartzeichen=vorzeichen key
        ,artab=tonartbreite key}
      )

makeKeyTransition oldKey newKey 
  |oldKey == newKey =   initdvielem
  |oldKey < newKey 
    =  (initdvielem
      { 
       maxbreite = tonartbreite newKey
      ,minbreite = tonartbreite newKey
      ,parts    = Term pa1
      })
  |newKey <oldKey =   aufloesen newKey oldKey
  |otherwise 
    = initdvielem 
      { -- genOdvi = dvin2
      parts    = Term pa2
      ,maxbreite = tonartbreite newKey+tonartbreite oldKey
      ,minbreite = tonartbreite newKey+tonartbreite oldKey
      } 
  where
    nota1
     = \z n -> 
              onotation ( [PushPos,MoveRight (schluesselab-4161531),MoveDown (-2*halbtonab)])
           +++(vorzeichenNota Violin newKey)
           +++onotation ( [PopPos,MoveRight  (tonartbreite newKey)])
    pa1=initpart
         {pschlag    = (1,4)
         ,pnotation=nota1
         }
    nota2 
      = \z n ->
              onotation   [PushPos]
           +++aufloesung
           +++onotation ( [MoveRight (tonartbreite oldKey), PushPos
                      ,MoveRight (schluesselab-4161531)])
           +++(vorzeichenNota Violin newKey)
           +++onotation ( [PopPos,PopPos,MoveRight (tonartbreite newKey+tonartbreite oldKey)]) 
    aufloesung |(CDur==oldKey) = onotation []
               |otherwise=(pnotation$singleHead$parts$aufloesen CDur oldKey) 1 1  
    pa2=initpart
         {pschlag    = (1,4)
         ,pnotation=nota2
         }

aufloesen newKey oldKey
 |newKey == oldKey = initdvielem
 |otherwise 
    = 
       (initdvielem 
         { --genOdvi = dvin
         maxbreite = breite
         ,minbreite = breite
         ,parts    = Term pa
         }) 
   where
--    dvin= \z n ->
--              odvi (D [Push,Right3 (schluesselab-4161531)])
--           +++(vorzeichen newKey) 
--           +++odvi (D [Pop,Push,Right3 (tonartbreite newKey)])
--           +++clearings 
--           +++odvi (D [Right3 (breite), Pop,Right3 breite])
    notan= \z n ->
              onotation ( [PushPos,MoveRight (schluesselab-4161531)])
           +++(vorzeichenNota Violin newKey) 
           +++onotation ( [PopPos,PushPos,MoveRight (tonartbreite newKey)])
           +++clearingsNota 
           +++onotation ( [MoveRight (breite), PopPos,MoveRight breite])
    pa =initpart
         {pschlag    = (1,4)
--         ,pdvi = dvin
         ,pnotation = notan
         }
{--    clearings
       |kreuztonart oldKey
        = clearSharpsFromTo newZ oldZ 
       |otherwise = clearFlatsFromTo newZ oldZ --}
    clearingsNota
       |kreuztonart oldKey
        = clearSharpsFromToNota newZ oldZ 
       |otherwise = clearFlatsFromToNota newZ oldZ 
    oldZ = (vorzeichenanzahl oldKey)
    newZ = (vorzeichenanzahl newKey)
    breite = vorzeichenLaengenBerechnung (oldZ-newZ) + tonartbreite newKey
