module Tinte.Notation where
import Tinte.Constants
import Tinte.Data
import Data.Char
import Debug.Trace


generatePS:: [(Int,Int)] -> [Notation] -> String
generatePS _ [] = ""
generatePS ((x, y):xys) (MoveRight x1:xs)
  = generatePS ((x+x1,y):xys) xs 
generatePS ((x, y):xys) (MoveDown y1:xs)
  = generatePS ((x,y+y1):xys) xs 
generatePS (xy:xys) (PopPos:xs)
  = generatePS xys xs 
generatePS (xy:xys) (PushPos:xs)
  = generatePS (xy:xy:xys) xs 
generatePS (xy:xys) (x:xs) = setCommand xy x ++generatePS (xy:xys) xs

showXY (x,y) = show(toPSMeasure x)++" "++show(toPSMeasure (y*(-1)))++" "

showMeasure=show.toPSMeasure

setCommand (x,y) PutTrillerSlur
 = showXY (x,y)
   ++ " { magfontPFAemmentaler-20mXVo setfont /scripts.trill_element glyphshow  } place-box\n"

setCommand (x,y) PutTriller
 = showXY (x,y)
   ++ " { magfontPFAemmentaler-20mXVo setfont /scripts.trill glyphshow  } place-box\n"


setCommand (x,y) PutSopranClef
 = showXY (x,y)
   ++ " { magfontPFAemmentaler-20mXVo setfont /clefs.G glyphshow  } place-box\n"

setCommand (x,y) PutTenorClef
 = showXY (x,y- notendicke*7 `div`8 - 4400)
   ++ " { magfontPFAemmentaler-20mXVo setfont /clefs.C glyphshow  } place-box\n"

setCommand (x,y) PutBassClef
 = showXY (x,y- 4*halbtonab)
   ++ " { magfontPFAemmentaler-20mXVo setfont /clefs.F glyphshow  } place-box\n"


setCommand (x,y) (PutSlurDown width height)
 =putSlur (x,y) width height " sub " " -"

setCommand (x,y) (PutSlurUp width height)
 =putSlur (x,y) width height " add " " "

setCommand (x,y) (PutLine width (x2, y2))
 = showXY (x,y)
      ++" { 0 setlinecap 0 setlinejoin "++showMeasure width++" setlinewidth 000.0000 000.0000 moveto "
      ++showXY (x2,y2)++" lineto stroke } place-box\n"



setCommand xy@(x,y) (PutBeam  width p2@(x2, y2))
 = showXY xy
  ++" { "++show (toPSMeasure (-y2)/toPSMeasure x2)
  ++" "++showMeasure x2++" "
  ++showMeasure width    ++" 000.0800  draw_beam } place-box\n"

setCommand (x,y) PutNoteHead1
 = showXY (x,y)
  ++" { magfontPFAemmentaler-20mXVo setfont /noteheads.s0 glyphshow  } place-box\n"

setCommand (x,y) PutNoteHead2
 = showXY (x,y)
  ++" { magfontPFAemmentaler-20mXVo setfont /noteheads.s1 glyphshow  } place-box\n"

setCommand (x,y) PutNoteHead
 = showXY (x,y)
  ++" { magfontPFAemmentaler-20mXVo setfont /noteheads.s2 glyphshow  } place-box\n"

setCommand (x,y) (Put8FlagUp)
 = showXY (x,y)
   ++" { magfontPFAemmentaler-20mXVo setfont /flags.u3 glyphshow  } place-box\n"
setCommand (x,y) (Put16FlagUp)
 = showXY (x,y)
   ++" { magfontPFAemmentaler-20mXVo setfont /flags.u4 glyphshow  } place-box\n"
setCommand (x,y) (Put32FlagUp)
 = showXY (x,y)
   ++" { magfontPFAemmentaler-20mXVo setfont /flags.u5 glyphshow  } place-box\n"
setCommand (x,y) (Put64FlagUp)
 = showXY (x,y)
   ++" { magfontPFAemmentaler-20mXVo setfont /flags.u6 glyphshow  } place-box\n"

setCommand (x,y) (Put8FlagDown)
 = showXY (x,y)
   ++" { magfontPFAemmentaler-20mXVo setfont /flags.d3 glyphshow  } place-box\n"
setCommand (x,y) (Put16FlagDown)
 = showXY (x,y)
   ++" { magfontPFAemmentaler-20mXVo setfont /flags.d4 glyphshow  } place-box\n"
setCommand (x,y) (Put32FlagDown)
 = showXY (x,y)
   ++" { magfontPFAemmentaler-20mXVo setfont /flags.d5 glyphshow  } place-box\n"
setCommand (x,y) (Put64FlagDown)
 = showXY (x,y)
   ++" { magfontPFAemmentaler-20mXVo setfont /flags.d6 glyphshow  } place-box\n"
setCommand (x,y) (PutRest1)
 = showXY (x,y)
   ++" { magfontPFAemmentaler-20mXVo setfont /rests.0 glyphshow  } place-box\n"
setCommand (x,y) (PutRest2)
 = showXY (x,y)
   ++" { magfontPFAemmentaler-20mXVo setfont /rests.1 glyphshow  } place-box\n"
setCommand (x,y) (PutRest4)
 = showXY (x,y)
   ++" { magfontPFAemmentaler-20mXVo setfont /rests.2 glyphshow  } place-box\n"
setCommand (x,y) (PutRest8)
 = showXY (x,y)
   ++" { magfontPFAemmentaler-20mXVo setfont /rests.3 glyphshow  } place-box\n"
setCommand (x,y) (PutRest16)
 = showXY (x,y)
   ++" { magfontPFAemmentaler-20mXVo setfont /rests.4 glyphshow  } place-box\n"
setCommand (x,y) (PutRest32)
 = showXY (x,y)
   ++" { magfontPFAemmentaler-20mXVo setfont /rests.5 glyphshow  } place-box\n"
setCommand (x,y) (PutRest64)
 = showXY (x,y)
   ++" { magfontPFAemmentaler-20mXVo setfont /rests.6 glyphshow  } place-box\n"

setCommand (x,y) (PutSharp)
 = showXY (x,y)
  ++" { magfontPFAemmentaler-20mXVo setfont /accidentals.2 glyphshow  } place-box\n"

setCommand (x,y) (PutFlat)
 = showXY (x,y)
  ++" { magfontPFAemmentaler-20mXVo setfont /accidentals.M2 glyphshow  } place-box\n"

setCommand (x,y) (PutDoubleSharp)
 = showXY (x,y)
  ++" { magfontPFAemmentaler-20mXVo setfont /accidentals.4 glyphshow  } place-box\n"

setCommand (x,y) (PutDoubleFlat)
 = showXY (x,y)
  ++" { magfontPFAemmentaler-20mXVo setfont /accidentals.M4 glyphshow  } place-box\n"

setCommand (x,y) (PutNatural)
 = showXY (x,y)
  ++" { magfontPFAemmentaler-20mXVo setfont /accidentals.0 glyphshow  } place-box\n"



--grace elements
setCommand (x,y) PutGraceNoteHead1
 = showXY (x,y)
  ++" { magfontPFAemmentaler-14mXVo setfont /noteheads.s0 glyphshow  } place-box\n"

setCommand (x,y) PutGraceNoteHead2
 = showXY (x,y)
  ++" { magfontPFAemmentaler-14mXVo setfont /noteheads.s1 glyphshow  } place-box\n"

setCommand (x,y) PutGraceNoteHead
 = showXY (x,y)
  ++" { magfontPFAemmentaler-14mXVo setfont /noteheads.s2 glyphshow  } place-box\n"

setCommand (x,y) (PutGrace8FlagUp)
 = showXY (x,y)
   ++" { magfontPFAemmentaler-14mXVo setfont /flags.u3 glyphshow  } place-box\n"
setCommand (x,y) (PutGrace16FlagUp)
 = showXY (x,y)
   ++" { magfontPFAemmentaler-14mXVo setfont /flags.u4 glyphshow  } place-box\n"
setCommand (x,y) (PutGrace32FlagUp)
 = showXY (x,y)
   ++" { magfontPFAemmentaler-14mXVo setfont /flags.u5 glyphshow  } place-box\n"
setCommand (x,y) (PutGrace64FlagUp)
 = showXY (x,y)
   ++" { magfontPFAemmentaler-14mXVo setfont /flags.u6 glyphshow  } place-box\n"

setCommand (x,y) (PutGrace8FlagDown)
 = showXY (x,y)
   ++" { magfontPFAemmentaler-14mXVo setfont /flags.d3 glyphshow  } place-box\n"
setCommand (x,y) (PutGrace16FlagDown)
 = showXY (x,y)
   ++" { magfontPFAemmentaler-14mXVo setfont /flags.d4 glyphshow  } place-box\n"
setCommand (x,y) (PutGrace32FlagDown)
 = showXY (x,y)
   ++" { magfontPFAemmentaler-14mXVo setfont /flags.d5 glyphshow  } place-box\n"
setCommand (x,y) (PutGrace64FlagDown)
 = showXY (x,y)
   ++" { magfontPFAemmentaler-14mXVo setfont /flags.d6 glyphshow  } place-box\n"
setCommand (x,y) (PutGraceRest1)
 = showXY (x,y)
   ++" { magfontPFAemmentaler-14mXVo setfont /rests.0 glyphshow  } place-box\n"
setCommand (x,y) (PutGraceRest2)
 = showXY (x,y)
   ++" { magfontPFAemmentaler-14mXVo setfont /rests.1 glyphshow  } place-box\n"
setCommand (x,y) (PutGraceRest4)
 = showXY (x,y)
   ++" { magfontPFAemmentaler-14mXVo setfont /rests.2 glyphshow  } place-box\n"
setCommand (x,y) (PutGraceRest8)
 = showXY (x,y)
   ++" { magfontPFAemmentaler-14mXVo setfont /rests.3 glyphshow  } place-box\n"
setCommand (x,y) (PutGraceRest16)
 = showXY (x,y)
   ++" { magfontPFAemmentaler-14mXVo setfont /rests.4 glyphshow  } place-box\n"
setCommand (x,y) (PutGraceRest32)
 = showXY (x,y)
   ++" { magfontPFAemmentaler-14mXVo setfont /rests.5 glyphshow  } place-box\n"
setCommand (x,y) (PutGraceRest64)
 = showXY (x,y)
   ++" { magfontPFAemmentaler-14mXVo setfont /rests.6 glyphshow  } place-box\n"

setCommand (x,y) (PutGraceSharp)
 = showXY (x,y)
  ++" { magfontPFAemmentaler-14mXVo setfont /accidentals.2 glyphshow  } place-box\n"

setCommand (x,y) (PutGraceFlat)
 = showXY (x,y)
  ++" { magfontPFAemmentaler-14mXVo setfont /accidentals.M2 glyphshow  } place-box\n"

setCommand (x,y) (PutGraceDoubleSharp)
 = showXY (x,y)
  ++" { magfontPFAemmentaler-14mXVo setfont /accidentals.4 glyphshow  } place-box\n"

setCommand (x,y) (PutGraceDoubleFlat)
 = showXY (x,y)
  ++" { magfontPFAemmentaler-14mXVo setfont /accidentals.M4 glyphshow  } place-box\n"

setCommand (x,y) (PutGraceNatural)
 = showXY (x,y)
  ++" { magfontPFAemmentaler-14mXVo setfont /accidentals.0 glyphshow  } place-box\n"









setCommand (x,y) (PutMetrum4_4)
 = showXY (x,y)
  ++" { magfontPFAemmentaler-20mXVo setfont /timesig.C44 glyphshow  } place-box\n"

setCommand (x,y) (PutMetrum z n)
 = showXY (x,y)
   ++" { gsave 1 output-scale div 1 output-scale div scale\n\
    \ /feta-alphabet20  findfont 7.029296875 scalefont setfont\n"
    ++setNum n++" glyphshow \n\
    \ grestore } place-box\n"
    ++(show$toPSMeasure x)++" "++show(toPSMeasure (-y+4*halbtonab))
    ++" { gsave 1 output-scale div 1 output-scale div scale\n\
    \  /feta-alphabet20  findfont 7.029296875 scalefont setfont\n"
    ++setNum z++" glyphshow\n\
    \ grestore } place-box\n"

setCommand (x,y) (PutText fnt text )
 = showXY (x,y)
   ++" { gsave 1 output-scale div 1 output-scale div scale\n"
   ++" "++fnt++"\n"
   ++"("++text++")\nshow\n"
   ++" grestore } place-box\n"
setCommand (x,y) (PutDotDot )
 = showXY (x,y)
  ++"{ magfontPFAemmentaler-20mXVo setfont /dots.dot glyphshow  } place-box\n"
  ++ showXY (x,y-2*halbtonab)
  ++"{ magfontPFAemmentaler-20mXVo setfont /dots.dot glyphshow  } place-box\n"
setCommand (x,y) (PutStaccato )
 = showXY (x,y)
  ++"{ magfontPFAemmentaler-20mXVo setfont /scripts.staccato glyphshow  } place-box\n"
setCommand (x,y) (PutSforzato )
 = showXY (x,y)
  ++"{ magfontPFAemmentaler-20mXVo setfont /scripts.sforzato glyphshow  } place-box\n"
setCommand (x,y) (PutMarcatoo )
 = showXY (x,y)
  ++"{ magfontPFAemmentaler-20mXVo setfont /scripts.umarcato glyphshow  } place-box\n"
setCommand (x,y) (PutMarcatou )
 = showXY (x,y)
  ++"{ magfontPFAemmentaler-20mXVo setfont /scripts.dmarcato glyphshow  } place-box\n"
setCommand (x,y) (PutTenuto )
 = showXY (x,y)
  ++"{ magfontPFAemmentaler-20mXVo setfont /scripts.tenuto glyphshow  } place-box\n"
setCommand (x,y) (PutStaccatissimoo )
 = showXY (x,y)
  ++"{ magfontPFAemmentaler-20mXVo setfont /scripts.ustaccatissimo glyphshow  } place-box\n"
setCommand (x,y) (PutStaccatissimou )
 = showXY (x,y)
  ++"{ magfontPFAemmentaler-20mXVo setfont /scripts.dstaccatissimo glyphshow  } place-box\n"
setCommand (x,y) (PutFermateo )
 = showXY (x,y)
  ++"{ magfontPFAemmentaler-20mXVo setfont /scripts.ufermata glyphshow  } place-box\n"
setCommand (x,y) (PutFermateu )
 = showXY (x,y)
  ++"{ magfontPFAemmentaler-20mXVo setfont /scripts.dfermata glyphshow  } place-box\n"
setCommand (x,y) (PutArpeggio )
 = showXY (x,y)
  ++"{ magfontPFAemmentaler-20mXVo setfont /scripts.arpeggio glyphshow  } place-box\n"
setCommand (x,y) (PutSegno)
 = (showXY (x,y)
   ++" { magfontPFAemmentaler-20mXVo setfont /scripts.segno  glyphshow  } place-box\n")


setCommand (x,y) (PutDot)
 = (showXY (x,y)
   ++" { magfontPFAemmentaler-20mXVo setfont /dots.dot glyphshow  } place-box\n")

setCommand (x,y) (PutStaffBar height)
 =    showXY (x,y)
   ++" { -00.0000 000.4500 "++show(toPSMeasure height)++" 0 "-- ++show(toPSMeasure height)
        ++" draw_box } place-box\n"
   ++show(toPSMeasure x+0.8)++" "++show(toPSMeasure (y*(-1)))++" "
   ++" { -00.0000 000.1600 "++show(toPSMeasure height)++" 0 "-- ++show(toPSMeasure height)
         ++" 000.1000 draw_round_box } place-box\n "
   ++show(toPSMeasure x)++" "++show(toPSMeasure ((y+height)*(-1)))++" "
   ++" { magfontPFAemmentaler-20mXVo setfont /brackettips.down glyphshow  } place-box\n"
   ++show(toPSMeasure x)++" "++show(toPSMeasure (y*(-1)))++" "
   ++" { magfontPFAemmentaler-20mXVo setfont /brackettips.up glyphshow  } place-box\n"

setCommand (x,y) (PutBrace height) = 
       show(toPSMeasure x)++" "++show(toPSMeasure ((y+height)*(-1)))++" "
   ++ " { magfontPFAaybabtumXVo setfont /brace327 glyphshow  } place-box\n "
--   ++ " { magfontemmentaler-bracemXVo /brace205 glyphshow  } place-box\n "



setCommand (x,y) _ =""

setNum 0 ="/zero "
setNum 1 ="/one "
setNum 2 ="/two "
setNum 3 ="/three "
setNum 4 ="/four "
setNum 5 ="/five "
setNum 6 ="/six "
setNum 7 ="/seven "
setNum 8 ="/eight "
setNum 9 ="/nine "


schlag2kopf (1,1) = PutNoteHead1
schlag2kopf (1,2) = PutNoteHead2
schlag2kopf _ = PutNoteHead

schlag2gracekopf (1,1) = PutGraceNoteHead1
schlag2gracekopf (1,2) = PutGraceNoteHead2
schlag2gracekopf _ = PutGraceNoteHead

schlag2rest (1,1) = PutRest1
schlag2rest (1,2) = PutRest2
schlag2rest (1,4) = PutRest4
schlag2rest (1,8) = PutRest8
schlag2rest (1,16) = PutRest16
schlag2rest (1,32) = PutRest32
schlag2rest (1,64) = PutRest64

putChar c=putChar2 c++" glyphshow\n"

putChar2 c
 |isDigit c = setNum (ord c-48)
putChar2 ' '="/space"
putChar2 '.'="/period"
putChar2 c  ="/"++[c]

mkPSHead glob
 = let titel = title glob
       subtitel=subtitle glob
       komponist = composer glob 
    in 
      "005.6906 -05.0000 { gsave 1 output-scale div 1 output-scale div scale\n\
      \/CenturySchL-Roma  findfont 3.865234375 scalefont setfont\n\
      \/space glyphshow\n\
      \grestore } place-box\n\

      \0  -20.0607 { gsave 1 output-scale div 1 output-scale div scale\n\
      \/CenturySchL-Roma  findfont 3.865234375 scalefont setfont\n\
      \("++komponist++") dup stringwidth  pop -1 mul -195 sub 0  moveto\n\
      \show\n\
      \grestore} place-box\n\
      \line-width 2 div -11.5607 { gsave 1 output-scale div 1 output-scale div scale\n\
      \/CenturySchL-Bold  findfont 6.1357421875 scalefont setfont\n\
      \("++titel++") dup stringwidth pop 2 div 040.5346 2 div exch sub 0 add 0 moveto\n\ 
      \show\n\
      \grestore} place-box\n\
      \line-width 2 div -15.5607 { gsave 1 output-scale div 1 output-scale div scale\n\
      \/CenturySchL-Bold  findfont 6.1357421875 scalefont setfont\n\
      \("++subtitel++") dup stringwidth pop 2 div 040.5346 2 div exch sub 0 add 0 moveto\n\ 
      \show\n\
      \grestore} place-box\n"
--      ++concat (map Tinte.Notation.putChar titel)
--      ++"grestore } place-box\n"


endPage num
 ="\n004.8028 -159.4715 { gsave 1 output-scale div 1 output-scale div scale\n\
  \ /CenturySchL-Roma  findfont 3.0673828125 scalefont setfont\n"
  ++concat (map Tinte.Notation.putChar$show num )
  ++" grestore } place-box\n\
  \} stop-system \n\
  \showpage\n\n"

generatePSFirstPage (seite)
 = generatePS[(indent+indent `div` 2  ,2*staffabstand+takthoehe)] seite++endPage 1
   
generatePSSnippet (seite)
 = generatePS[(0,indent`div`2)] seite++endPage 1


generatePSFurther m (num,(seite)) 
 ="%%Page: "++show num++" "++show m++"\n\
  \%%BeginPageSetup\n\
  \%%EndPageSetup\n\
  \start-page { set-ps-scale-to-lily-scale \n"
  ++generatePS[(indent+indent `div` 2  ,4*takthoehe)] seite++endPage num

putSlur (x,y) widthorg1 heightorg1 addOrSub minusOrPlus
 = let yk = ykorFor width
       yk2 = yk+0.2
       ykor = show yk
       y2kor = show yk2
       xkor =show$xkorFor width
       height=0
       fromOrg wo = let wsq = ((toInteger wo)^2+(toInteger heightorg1)^2) 
                    in  truncate$sqrt(fromInteger wsq)  
       widthorg = widthorg1 --if (abs heightorg1)>(widthorg1) then (widthorg1-notendicke)
                   --else widthorg1  
       heightorg = if (abs heightorg1)>(widthorg1) then (heightorg1-notendicke) else heightorg1  
       width=fromOrg widthorg
       alpha=atan ((fromInteger$toInteger heightorg)/(fromInteger$toInteger width))*180/pi
  in
   (showXY (x,y))
   ++" { "
   ++ (if heightorg==0 then "" else (show alpha ++" rotate\n"))
   ++xkor++" "++showMeasure height++" "++ykor++addOrSub                   --startpunkt moveto
   ++  showMeasure width++" "++xkor++" sub "
          ++showMeasure height++" "++ykor++addOrSub
          ++showMeasure width++ " "
          ++showMeasure height    --bezier
   ++" 0.0 0  "-- ++showMeasure height++" "                                 -- lineto  startpunkt
   ++showMeasure width++ " "++xkor++" sub "++showMeasure height++" "++y2kor++addOrSub
  -- curveto  startpunkt
   ++" "++xkor++minusOrPlus++y2kor++" 0.0  0 "-- ++showMeasure height++" " --bezier 
   ++showMeasure width ++" "++showMeasure height++"  "          --endpunkt moveto
   ++" 000.1000 draw_bezier_sandwich \n"
   ++"} place-box\n"


asGrace PutNoteHead=PutGraceNoteHead
asGrace PutNoteHead1=PutGraceNoteHead1
asGrace PutNoteHead2=PutGraceNoteHead2
asGrace PutSharp=PutGraceSharp
asGrace PutFlat=PutGraceFlat
asGrace PutDoubleSharp=PutGraceDoubleSharp
asGrace PutDoubleFlat=PutGraceDoubleFlat
asGrace PutNatural=PutGraceNatural
asGrace Put8FlagUp=PutGrace8FlagUp
asGrace Put16FlagUp=PutGrace16FlagUp
asGrace Put32FlagUp=PutGrace32FlagUp
asGrace Put64FlagUp=PutGrace64FlagUp
asGrace Put8FlagDown=PutGrace8FlagDown
asGrace Put16FlagDown=PutGrace16FlagDown
asGrace Put32FlagDown=PutGrace32FlagDown
asGrace Put64FlagDown=PutGrace64FlagDown
asGrace PutRest64=PutGraceRest64
asGrace PutRest32=PutGraceRest32
asGrace PutRest16=PutGraceRest16
asGrace PutRest8=PutGraceRest8
asGrace PutRest4=PutGraceRest4
asGrace PutRest2=PutGraceRest2
asGrace PutRest1=PutGraceRest1
asGrace x=x

ykorFor::Int -> Float
ykorFor width
 |width>3000000 = 2.1
 |width>2500000 = 1.8
 |width>2000000 = 1.6
 |width>1500000 = 1.3
 |width>1000000 = 1.0
 |width>500000 = 0.5
 |width>250000 = 0.2
 |otherwise = 0.1

xkorFor width
 |width>3000000 = 3.1
 |width>2500000 = 2.7
 |width>2000000 = 2.3
 |width>1500000 = 1.9
 |width>1000000 = 1.5
 |width>500000 = 1.2
 |width>250000 = 0.6
 |otherwise = 0.2

