module Tinte.Fahnen where
import Tinte.Data
import Tinte.Constants
import Tinte.Util
import Tinte.Macros

--import Dvi.Dvi

f8o :: GenElem -> GenElem
f8o alts = ffo alts f8breite 40  Put8FlagUp

grfo :: GenElem -> GenElem
grfo alts = ffo alts f8breite 12  PutGrace16FlagUp

ffo alts breite char flagnota s 
 =  ( alt  
      { maxbreite = maxb+ mbreite
      , minbreite = minb+ mbreite
--      ,genOdvi    = quantmit  (odvi (D []))(minb+ mbreite) (maxb+ mbreite)
      ,parts      = partsneu
      }
    , s1)
  where
    (alt,s1) = alts s
    h     = hoehe alt
    maxb   = maxbreite alt
    minb   = minbreite alt
 --   gehhoch  = Y3 (h*halbtonab+mass f8hoehe)
--    fahne    = Set_char char
    mass n |theGrace s = div (n*13) 20|otherwise = n
    mbreite  = mass breite
    partsalt = parts alt
    partsneu = changeHead pneu partsalt
    pneu p  = p { --pdvi = dvineu,
                  pnotation=notaneu}
      where
--       dvineu z n 
--         = (odvi (D [Push,Right3 (mass notendicke),gehhoch,fahne,Pop]))+++dvoalt
--           +++odvi (D [Right3 mbreite])
--          where
--             dvoalt = pdvi p z n
       notaneu z n = onotation [PushPos,MoveRight (strichdicke+mass notendicke)
                          , MoveDown (h*halbtonab) -- -2*mass f8hoehe) 
                          ,flagnota,PopPos]+++notaalt
        where
          notaalt = pnotation p z n

f8u :: GenElem -> GenElem
f8u alts = ffu alts 0 45 Put8FlagDown

ffu alts kor char flagnota s
 = (alt {   parts  = partsneu},s1)
  where
   (alt,s1) = alts s
   t    = tiefe alt
--   gehhoch  = Y3 (t*halbtonab+kor)
--   fahne  = Set_char char
   partsalt  = parts alt
   partsneu   = changeHead pneu partsalt
   pneu p   = p { --pdvi = dvineu,
                 pnotation=notaneu}
     where
--      dvineu z n = (odvi (D [Push,gehhoch,fahne,Pop]))+++dvoalt
--        where
--          dvoalt = pdvi p z n
      notaneu z n = onotation [PushPos,MoveRight (halsdicke),MoveDown (t*halbtonab+kor) ,flagnota,PopPos]+++notaalt
        where
          notaalt = pnotation p z n


f8 :: GenElem -> GenElem
f8 alts = f alts
 where
  f alts s   
    |ref_hoehe alt>(-4)= f8o alts s
    |otherwise = f8u alts s
    where
      (alt,_) = alts s
    
f16o :: GenElem -> GenElem
f16o alts = ffo alts f8breite 41 Put16FlagUp


f16u :: GenElem -> GenElem
f16u alts = ffu alts halbtonab 46 Put16FlagDown

f16 :: GenElem -> GenElem
f16 alts  = f alts
 where
  f alts s
    |ref_hoehe alt>(-4)= f16o alts s
    |otherwise = f16u alts s
     where
      (alt,_) = alts s
   
f32o :: GenElem -> GenElem 
f32o alts = ffo alts f8breite 42  Put32FlagUp

f32u :: GenElem -> GenElem 
f32u alts = ffu alts (2*halbtonab) 47 Put32FlagDown

f32 :: GenElem -> GenElem 
f32 alts  = f alts
 where
  f alts s
    |ref_hoehe alt>(-4)= f32o alts s
    |otherwise = f32u alts s
    where
      (alt,_) = alts s

f64o :: GenElem -> GenElem 
f64o alts = ffo alts f8breite 43 Put64FlagUp

f64u :: GenElem -> GenElem 
f64u alts = ffu alts (4*halbtonab) 48 Put64FlagDown

f64 :: GenElem -> GenElem 
f64 alts  = f alts
 where
  f alts s
    |ref_hoehe alt>(-4)= f64o alts s
    |otherwise = f64u alts s
     where
      (alt,_) = alts s

fa :: GenElem -> GenElem 
fa alts = g alts
 where
  g alts s
    |null schl  = alts s
    |otherwise    = (whichf (head schl)) alts s
    where
      schl = schlaege alt
      (alt,_) = alts s

whichf (1,8)  = f8
whichf (1,16)  = f16
whichf (1,32)  = f32
whichf (1,64)  = f64
whichf _    = id

fo :: GenElem -> GenElem 
fo alts = g alts
 where
  g alts s
    |null schl  = alts s
    |otherwise    = (whichfo (head schl)) alts s
     where
      schl = schlaege alt
      (alt,_) = alts s

fu :: GenElem -> GenElem 
fu alts = g alts
 where
  g alts s
    |null schl  = alts s
    |otherwise    = (whichfu (head schl)) alts s
     where
      schl = schlaege alt
      (alt,_) = alts s


whichfo (1,8)  = f8o
whichfo (1,16)  = f16o
whichfo (1,32)  = f32o
whichfo (1,64)  = f64o
whichfo _  = id

whichfu (1,8)  = f8u
whichfu (1,16)  = f16u
whichfu (1,32)  = f32u
whichfu (1,64)  = f64u
whichfu _  = id


