module Tinte.Grace where

import Debug.Trace

import Tinte.Data
import Tinte.Constants
import Tinte.Util
import Tinte.Macros
import Tinte.AltSeq
import Tinte.Hals
import Tinte.Fahnen
import Tinte.Balken
import Tinte.Bogen
import Tinte.Notation
import Tinte.Noten
import Data.Ratio




grace :: GenElem -> GenElem -> GenElem
grace gr nr glob
-- |isNull (parts nre) = nr glob
-- |otherwise 
  = (nre{ maxbreite = maxn 
        , minbreite = minn
--        , genOdvi    = \z n -> quantnach (dvia z n) minn maxn z n
        , offset    = offa
        , parts     = clus2}
    ,glob3) 
  where
    (grc,glob2) = (smallMod gr) glob{theGrace = _Grace}
    (nre,glob3) = (normalMod nr) glob {theGrace = _Normal}
    maxb     = maxbreite nre
    minb     = minbreite nre
    maxn     = maxb+maxbreite grc
    minn     = minb+maxbreite grc
--    dvia     = genOdvi nre  
    ref      = ref_hoehe nre
    offa     = offset nre+maxbreite grc
    clus     = parts nre
    clus1    = changeTail (verschiebe (quant (minbreite grc)(maxbreite grc))) clus
    clus2    = changeHead (makeVorsatz grc) clus1
    makeVorsatz grc p 
      = p 
      { poffset     = poff + graceOffset
--      , pvorsatzdvi = (mdvi) 1 10 +++pvorsatz
      , pvorsatznotation = onotation (map asGrace (notation$(mnota) 1 10) )
                           +++onotation [MoveRight graceOffset ] +++ pvorsatznota
      , pvorsatzart = ((error "in is, darf es nie geben",ref):vorsatza)
      }
      where
        graceOffset=quant (minbreite grc) (maxbreite grc) 1 5 -- +die laenge des grace vorsatzes
        poff     = poffset p
  --      pvorsatz = pvorsatzdvi p
        pvorsatznota = pvorsatznotation p
        vorsatza = pvorsatzart p
        (((mnota),(maxb)),s1) = mktaktGlob1 glob{theGrace = _Grace} (smallMod gr) 

mktaktGlob1 s alts = (((mnota),(maxb)),s1)
   where
    (alt,s1) = alts s
    minb = minbreite alt
    maxb = maxbreite alt
--    mdvi z n = -- vor+++pdvis+++dv
--                 pdvis
     where
--      dv    = genOdvi alt z n 
--      vor   = vorsatzdvi alt
      par   = parts alt
    mnota z n = pnotas
     where
      --dv    = genOdvi alt z n 
--      vor   = vorsatznotation alt
      par   = parts alt
      pnotas = onotation [PushPos]
--	      +++dv 
 --             +++odvi (D [Pop,Push])
              +++concatON
                 [
                  onotation [PushPos,MoveRight (pwoliegt p z n)]
                  +++(pvorsatznotation p)
                  +++onotation [PopPos,PushPos,MoveRight (pwoliegtwirklich p z n)]
                  +++(pnotation p z n)
                  +++onotation [PopPos,PushPos,MoveRight$poffset p]
                  +++(makepostdefs s p ps z n)
                  +++onotation[PopPos]
                 |(p,ps)<-zipwithtail (flattenCluster par)]
              +++onotation [PopPos]


makepostdefs s p ps z n
  = (--concatO (map (\f -> (fst f) z n) (map (balkenToOdvi s  p ps) (ppostdefs  p)))
    -- ,
    concatON (map (\f -> ( f) z n) (map (balkenToOdvi s  p ps) (ppostdefs p)))
    )


balkenToOdvi s p ps (Balkoat i defs)  =  
  -- (postProcess p$fst$balkoatpart s  i defs (Seq (map Term (p:ps))) 
  --,
  pileONZN$postProcessNotation p$balkoatpart s  i defs (Seq (map Term (p:ps))) --) 
balkenToOdvi s p ps (Balkuat i defs) =  
   (
 -- postProcess p$fst$balkuatpart s  i defs (Seq (map Term (p:ps)))
  --,
  pileONZN$postProcessNotation p$balkuatpart s  i defs (Seq (map Term (p:ps))) ) 
balkenToOdvi s p ps (Balkouat i defo defu) =
  (
  -- postProcess p$fst$balkouatpart s i defo defu (Seq (map Term (p:ps)))
  --,
  pileONZN$postProcessNotation p$balkouatpart s i defo defu (Seq (map Term (p:ps))))
balkenToOdvi s p ps (Sluroat i (fnr,fh) (tnr,th)) = 
  ( --postProcess p $
    --fst (sluroatpart i (fnr,fh) (tnr,th) (Seq (map Term (p:ps))) s)
  -- ,
  pileONZN$postProcessNotation p 
   (sluroatpart i (fnr,fh) (tnr,th) (Seq (map Term (p:ps))) s))
balkenToOdvi s p ps (Sluruat i (fnr,fh) (tnr,th)) 
 =( -- postProcess p $fst
   -- (sluruatpart i (fnr,fh) (tnr,th) (Seq (map Term (p:ps))) s)
  --,
  pileONZN$ postProcessNotation p
   (sluruatpart i (fnr,fh) (tnr,th) (Seq (map Term (p:ps))) s))
balkenToOdvi s p ps (Crescat i fnr tnr )
 =( -- \z n ->odvi$D [] -- postProcess p (fst
   --(crescatpart i fnr tnr (Seq (map Term (p:ps))) s))
  --,
  postProcessNotation p 
   (crescatpart i fnr tnr False (Seq (map Term (p:ps))) s))
balkenToOdvi s p ps (Decrescat i fnr tnr )
 = (-- \z n ->odvi$D [] -- postProcess p (fst
   --(crescatpart i fnr tnr (Seq (map Term (p:ps))) s))
  -- ,
  postProcessNotation p (
   (crescatpart i fnr tnr True (Seq (map Term (p:ps))) s)))
balkenToOdvi s p ps (Glissando fnr tnr )
 =( postProcessNotation p (
   (glissandoatpart fnr tnr (Seq (map Term (p:ps))) s)))
balkenToOdvi s p ps (Triller fnr tnr )
 =(  postProcessNotation p (
   (trilleratpart fnr tnr (Seq (map Term (p:ps))) s)))

pileONZN nota = \z n -> (onotation [PushPos]+++nota z n+++onotation [PopPos])


crescatpart  i fnr tnr isDecrescendo  partsalt s
   |not (success sffrompart&& success sftopart) =  (\z n -> onotation [])
   |otherwise  =  (notaneu)     
   where

    notaneu    = \z n -> (onotation [PushPos] +++ (cresccode  z n)+++onotation[PopPos])

    cresccode z n = nota
     where
      nota = onotation ([PushPos,MoveRight rechtsab,MoveDown (i*halbtonab)]
                        ++crescsymbol ++[PopPos])

      rechtsab  -- |one = -2*notendicke |otherwise
                  = (vonw)
      crescsymbol 
        |isDecrescendo
          =  [PushPos,MoveDown halbtonab,PutLine halsdicke (weite,-halbtonab),PopPos]
           ++[PushPos,MoveDown (-halbtonab),PutLine halsdicke (weite,halbtonab),PopPos]
        |otherwise=[PutLine halsdicke (weite,halbtonab),PutLine halsdicke (weite,-halbtonab)]

      vonw    = pwoliegt frompart z n
      nachw    = pwoliegt topart z n
      frompart  = getsuccess sffrompart  
   
      weite    = abs (vonw-nachw)+poffset topart-poffset frompart
      topart    = getsuccess sftopart

    sffrompart  = (sel fnr partsalt)
    sftopart    = (sel tnr partsalt)
    staffhoehe  = putatstaff i 


glissandoatpart    fnr tnr   partsalt s
   |not (success sffrompart&& success sftopart) =  (\z n -> onotation [])
   |otherwise  =  (notaneu)     
   where
    notaneu    = \z n -> (onotation [PushPos] +++ (cresccode  z n)+++onotation[PopPos])

    cresccode z n = (nota)
     where
      nota = onotation ([PushPos,MoveRight rechtsab,MoveDown (ptiefe frompart*halbtonab)]
                        ++crescsymbol ++[PopPos])
      rechtsab  -- |one = -2*notendicke |otherwise
                  = (vonw)
      crescsymbol 
       =  trace (show weite++show (ptiefe frompart)++show (ptiefe  topart))
         [MoveDown$ptiefe frompart,PutLine halsdicke (weite,(-halbtonab*(ptiefe frompart-ptiefe topart)))]

      vonw    = pwoliegt frompart z n
      nachw    = pwoliegt topart z n
      frompart  = getsuccess sffrompart  
   
      weite    = abs (vonw-nachw)+poffset topart-poffset frompart
      topart    = getsuccess sftopart

    sffrompart  = (sel fnr partsalt)
    sftopart    = (sel tnr partsalt)



trilleratpart    fnr tnr   partsalt s
   |not (success sffrompart&& success sftopart) =  (\z n -> onotation [])
   |otherwise  =  (notaneu)     
   where
    notaneu    = \z n -> (onotation [PushPos] +++ (cresccode  z n)+++onotation[PopPos])

    cresccode z n = (nota)
     where
      nota = onotation ([PushPos
                        ,MoveRight rechtsab
                        ,MoveDown (ptiefe frompart*halbtonab-notendicke)]
                        ++crescsymbol
                        ++[PopPos])
      rechtsab  = (vonw)
      crescsymbol 
       = [PutTriller, MoveRight notendicke   ]++
          (concat $ take ((nachw-vonw+endbreite)`div` notendicke)$
          repeat [ MoveRight notendicke 
                 , PutTrillerSlur 
                 ])

      vonw    = pwoliegt frompart z n
      nachw    = pwoliegt topart z n
      frompart  = getsuccess sffrompart
      endbreite = (pminbr  topart+pmaxbr  topart) `div` 2
   
      weite    = abs (vonw-nachw)+poffset topart-poffset frompart
      topart    = getsuccess sftopart

    sffrompart  = (sel fnr partsalt)
    sftopart    = (sel tnr partsalt)




gr8o  c i genelem
 = Tinte.Grace.grace  
    (grfo$halso (k  c i (div n64min 2) (div n64max 2) notendicke 33 (0,1) True
                                                                 (N (1%8) c i))) genelem
   {--smallMod (grfo (smallMod (halso (smallMod(k c i (div n64min 2) (div n64max 2)
    notendicke 33 schlag (N8 c i)))))) 
   -/normal
   where
  schlag  = (1,8) --}
