module Tinte.Hals where

import Tinte.Data
import Tinte.Constants
import Tinte.Util
import Tinte.Macros

--import Dvi.Dvi

halsol :: Int -> GenElem -> GenElem
halsol l alts = h l alts
 where 
  h l alts s = (alt {   hoehe   = h-div lm halbtonab,
            parts  = partsneu},s1)
   where
    (alt,s1) = alts s 
    h      = hoehe alt
    t      = tiefe alt
  --  hals    = Set_rule halsl strichdicke
    halsnot    = PutLine (halsdicke) (0,-halsl)  
    halsl    = (abs (h-t))*halbtonab+lm
 --   gehhoch    = Y3 (t*halbtonab)
    gehhochnot = MoveDown (t*halbtonab)
    partsalt  = parts alt
    partsneu  = changeHead pneu partsalt
    pneu p   = p{ --pdvi = dvineu,
                pnotation=pnotneu}
     where
--      dvineu z n  = (odvi (D [Push,Right3 (mass notendicke),gehhoch,hals,Pop]))
--            +++(pdvi p z n)
      pnotneu z n= (onotation [PushPos,MoveRight$mass notendicke
                              ,gehhochnot,halsnot,PopPos])+++(pnotation p z n)
    mass n |theGrace s = div (n*13) 20|otherwise = n
    lm |theGrace s = 4*halbtonab|otherwise = 6*halbtonab

halsul :: Int -> GenElem -> GenElem
halsul l alts = h l alts
 where 
  h l alts s = (alt{  tiefe  = t+div lm halbtonab,
            parts  = partsneu},s1)
   where
    (alt,s1) = alts s 
    h      = hoehe alt
    t      = tiefe alt
  --  hals    = Set_rule halsl strichdicke
    halsnot    = PutLine (halsdicke) (0,halsl) 
    halsl    = (abs (h-t)-1)*halbtonab +lm
 --   gehhoch    = Y3 ((t*halbtonab+lm))
    gehhochnot = MoveDown ((t*halbtonab)-((abs (h-t))*halbtonab ))
    partsalt  = parts alt
    partsneu  = changeHead pneu partsalt
    pneu p   = p { -- pdvi = dvineu,
                pnotation=pnotneu}
     where
--      dvineu z n  = (odvi (D [Push,gehhoch,hals,Pop]))+++(pdvi p z n)
      pnotneu z n= (onotation [PushPos,MoveRight halsdicke
                              ,gehhochnot,halsnot,PopPos])+++(pnotation p z n)
    mass n |theGrace s = div (n*13) 20|otherwise = n
    lm |theGrace s = 4*halbtonab|otherwise = 6*halbtonab
  

halso ::  GenElem -> GenElem
halso alt = halsol halslaenge alt

halsu ::  GenElem -> GenElem
halsu alt = halsul halslaenge alt

hals :: GenElem -> GenElem
hals alts  = h alts
 where 
  h  alts s  
   |ref_hoehe alt>(-4)= halso alts s
   |otherwise = halsu alts s
   where
    (alt,_) = alts s



    


