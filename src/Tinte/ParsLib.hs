module Tinte.ParsLib where
import Data.Char
import Tinte.Constants

type CParser s r t 
  = (SucCont s r t) -> (XorCont s t) -> (AltCont s t) -> Parser s t
type SucCont s r t = r (XorCont s t) -> (AltCont s t) -> Parser s t
type XorCont s t = (AltCont s t) -> ParsResult s t
type AltCont s t = ParsResult s t
type ParsResult s r = [([s],r)]
type Parser s t = [s]->ParsResult s t
        
fail = \sc xs ac ss -> xs ac

--yield :: r -> CParser s r t
yield x = \sc -> sc x

symbol sym = p
  where
    p sc xc ac (s:ss)  | s == sym = sc sym xc ac ss
    p sc xc ac _                  = xc ac

--satisfy  :: (s ->Bool) -> CParser s s  v 
satisfy pred = p
  where
    p sc xc ac (s:ss)  | pred s = sc s xc ac ss
    p sc xc ac _                = xc ac

--white :: CParser Char [Char] a
white = (>*<) whiteSymbol 

--identifier :: CParser Char String a
identifier =  alpha >&< \x ->  ((>*<) alphaNum) <@ \xs ->  (x:xs)

keyword ys = keyword1 ys <@ \_ -> ys

keyword1 []     = yield ""
keyword1 (x:xs) = symbol x >&< keywordaux xs

keywordaux xs _ = keyword1 xs

keywordaslist []     = yield []
keywordaslist xs = symbol (head xs) >&< \_ -> keyword1 (tail xs) <@ \_ -> xs

--alpha :: CParser Char Char a
alpha = satisfy (\c -> elem c (['a'..'z']++['A'..'Z']))

beta 
 = (keyword "\\\"" <@ \_-> "\"")   --"
   >!< (satisfy (\c -> not (elem c ("\""))) <@ \x ->  [x]) --"
     

--character :: CParser Char Char a
character = symbol '\'' >&< \_-> alpha >&< \c -> symbol '\''<@ \_-> c

--number  :: CParser Char Int a
number
  = ((((symbol '~') <@ (\_-> \x->0-x) )>|< yield id)) 
         >&< \f -> unsigned <@ \n -> f n
         

unsigned  = ((>+<) ziffer) <@ foldl (\x y -> x *10+((fromChar y)-startOrd)) 0 

fromChar y = ord y - 48

startOrd :: Int
startOrd = fromChar '0'

alphaNum = alpha >!< ziffer

ziffer = satisfy (\c -> elem c (['0'..'9']))
zifferAlsZahl = ziffer <@ \z -> 0 

whiteSymbol = satisfy (\c -> elem c ([' ','\n','\t'])||isControl c)

--removeWhite :: (CParser Char b c) -> CParser Char b c
removeWhite p = white >&< \_ -> p  <@ \res -> res 

infixl 5 <@ 
infixl 6 >&<
infixr 6 >!&<
infixr 4 >!<
infixr 4 >|< 
infixr 2 >**<
infixr 2 >++<

infixr 1 >*<
infixr 1 >+<

(<@) p f = p >&< yield . f  
    
(>&<) p1 p2 = \sc -> p1 (\t -> p2 t sc)

(>!&<) p1 p2 = \sc -> p1 (\t ac2 -> p2 t sc (\ac ->ac))

(>!<) p1 p2 
  = \sc xc ac ss -> p1 (\x xc2 -> sc x xc) (\ac3 -> p2 sc xc ac3 ss) ac ss

(>|<) p1 p2 =  \sc xc ac ss -> p1 sc (\ac2 -> ac2)(p2 sc xc ac ss) ss

(>*<) p = ((   p        >!&< \r ->   
          (>*<) p      <@ \rs -> (r:rs)))
          >!< yield []
            
(>+<) p = p >!&< \a -> (>*<) p <@ \as -> (a:as)    

(>**<) p  sep 
  = (   p                        >!&< \r ->   
        ((>*<) (sep >!&< \_ -> p ))  <@ \rs -> (r:rs)
    )
    >!< yield []
 
(>++<) p sep  = p >!&< \r -> (>*<) (sep >!&< \_ -> p)  <@ \rs -> (r:rs)  

--string :: CParser Char String a
string = 
 symbol '"'                                                   >&< \_ -> 
 (((>*<) beta) <@ \ls ->  (concat ls)  )               >&< \str ->
 symbol '"'                                                    <@ \_ ->
 str


--paranthesis :: (CParser Char b c) -> CParser Char b c
paranthesis p = removeWhite (symbol '(') >&< \_ ->  (removeWhite p) >&< \x -> removeWhite (symbol ')') <@ \_-> x

--squarebracket:: (CParser Char b c) -> CParser Char b c
squarebracket p = removeWhite (symbol '[') >&< \_ -> removeWhite p >&< \x -> removeWhite (symbol ']') <@ \_-> x

--braces:: (CParser Char b c) -> CParser Char b c
braces p = removeWhite (symbol '{') >&< \_ -> removeWhite p >&< \x -> removeWhite (symbol '}') <@ \_-> x


--cleanlist :: (CParser Char b c) -> CParser Char [b] c
cleanlist p = squarebracket (removeWhite p >**< removeWhite (symbol ','))

commasep p =  (removeWhite p >**< removeWhite (symbol ','))

--newcleanlist ::  CParser Char [[Char]] c
newcleanlist  = removeWhite (symbol '[') >&< \_ -> cleanlistaux 0 0 False [] [] 
--cleanlistaux :: Int Bool [Char] [b] (CParser Char b b) -> CParser Char [b] b
cleanlistaux brackets pars quotes currentElement akku 
 = pr
   where
    pr sc xc ac (']':ss)  
     | brackets == 0 && not quotes
        = (sc (reverse (reverse currentElement:akku)) xc ac ss)
     | not quotes                  
        =  ( (cleanlistaux (brackets-1) pars quotes (']':currentElement) akku ) sc xc ac ss)
     | otherwise                   
         =  ( (cleanlistaux brackets pars quotes (']':currentElement) akku )  sc xc ac ss)
    pr sc xc ac ('[':ss)
     | not quotes                  
       =  ( (cleanlistaux (brackets+1) pars quotes ('[':currentElement) akku )  sc xc ac ss)
     | otherwise                   
        =  ( (cleanlistaux brackets pars quotes ('[':currentElement) akku)  sc xc ac ss)
    pr sc xc ac (')':ss)  
     | not quotes                  
       =  ( (cleanlistaux brackets (pars-1) quotes (')':currentElement) akku ) sc xc ac ss)
     | otherwise                   
       =  ( (cleanlistaux brackets pars quotes (')':currentElement) akku )  sc xc ac ss)
    pr sc xc ac ('(':ss)
     | not quotes                  
       =  ( (cleanlistaux brackets (pars+1) quotes ('(':currentElement) akku )  sc xc ac ss)
     | otherwise                   
       =  ( (cleanlistaux brackets pars quotes ('(':currentElement) akku)  sc xc ac ss)
    pr sc xc ac (',':ss)
     | brackets == 0 && not quotes && pars == 0
       =  ( (cleanlistaux 0 0 False [] (reverse currentElement:akku) ) sc xc ac ss)
     | otherwise                  
       =  ( (cleanlistaux brackets pars quotes (',':currentElement) akku )  sc xc ac ss)
    pr sc xc ac ('"':ss)           
       =  ( (cleanlistaux brackets pars (not quotes) ('"':currentElement) akku )  sc xc ac ss)
    pr sc xc ac ('\\':'"':ss)      
       =  ( (cleanlistaux brackets pars quotes ("\\\""++currentElement) akku )
              sc xc ac ss) --"
    pr sc xc ac (s:ss)             
     =  ( (cleanlistaux brackets pars quotes (s:currentElement) akku )  sc xc ac ss)
    pr sc xc ac ss                 = xc ac

--"
removeCommentLine xs = commentWeg False  False xs

commentWeg _ _ [] = []
commentWeg _ _ ('\n':xs) = ('\n':commentWeg False False xs)
commentWeg False _ ('/':'/':xs) = commentWeg False True xs
commentWeg _ True (x:xs) = commentWeg False True xs
commentWeg inQuote False ('"':xs) = ('"':commentWeg (not inQuote) False xs)
commentWeg inQuote False ('\\':'"':xs) = ('\\':'"':commentWeg inQuote False xs)
commentWeg inQuote False (x:xs) = (x:commentWeg inQuote False xs)


--begin :: (CParser s t t) -> Parser s t
begin p = p (\ x xc ac ss -> ((ss,x):xc ac)) (\ac -> ac) []

concatWith1 :: (a ->a -> a) -> [a] -> a
concatWith1 f [x] = x
concatWith1 f (x:xs) = f x (concatWith1 f xs)

aANDbORc 
 = symbol 'a' >&< \x ->
   (symbol 'b' >|< symbol 'c')<@ \y -> (x,y)
--}
--Start = begin (aANDbORc) "abc"
--

pabstand        = psum >&< \lhs -> 
                  pterm <@ \rhs -> 
                  rhs lhs
 
pterm
 =   ( removeWhite paddop >&< \op -> pabstand <@ \rhs -> (\lhs -> op lhs rhs) )
  >!< yield (\x -> x)
  
psum
 = primary >&< \lhs -> psum2 <@ \rhs -> rhs lhs
 
psum2
 =   (removeWhite pmulop >&< \op -> psum <@ \rhs -> (\lhs -> op lhs rhs)  )
  >!< yield (\x -> x)
 
primary 
 =      removeWhite pnotendicke >!< removeWhite pdeflinienab
    >!< removeWhite number >!< removeWhite (paranthesis pabstand)

padd = symbol '+' <@ \_ -> (+)
psub = symbol '-' <@ \_ -> (-)
pdiv = symbol '/' <@ \_ -> div
pmul = symbol '*' <@ \_ -> (*)

paddop = concatWith1 (>!<) [padd,psub]
pmulop  = concatWith1 (>!<) [pdiv,pmul]

pnotendicke  = createsimpleelement "notendicke" notendicke
pdeflinienab  = createsimpleelement "deflinienab" deflinienab

createsimpleelement str f = keyword str    <@ \_ -> f
