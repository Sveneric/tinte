module  Tinte.ParseTinte where
import Tinte.ParsLib


import Tinte.Data hiding (klammer)
import Tinte.Constants
import Tinte.Util

import Tinte.Noten hiding (n1,n2,n4,n8,n16,n32,n64)
import Tinte.PP
import Tinte.AltSeq
import Tinte.Fahnen
import Tinte.Hals
import Tinte.Balken
import Tinte.Akkord
import Tinte.Bogen
import Tinte.Pausen
import Tinte.Staff
import Tinte.Grace
import Tinte.ChangeKey

data UNIT = UNIT
 
epsilon = yield UNIT 

liedImport 
  = (((>+<) parseEnv)
      >!< (yield [("out", initialglob{teiler= 480
                               ,snippet=True
                               ,zeilenlaenge=kurzl})]) )
                                      >&< \envs ->
    removeWhite identifier            >&< \i -> 
    removeWhite (symbol '=')          >&< \_ ->
    cleanlist (globalAndBars)         <@ \movements ->
    [(name,env ,movements)|(name,env)<-envs]

parseEnv  
 = (removeWhite identifier                         >&< \name -> 
    (removeWhite (symbol '=')                       >&< \_ ->
      (braces
       (pStateChange >**< removeWhite(symbol ',')))  <@ \changes ->
       (name,foldl (\st changer->changer st) initialglob changes)
      )
   )

globalAndBars =
  removeWhite unparsedList    <@ \li       ->
  map removeAdditionlWhite li

statechange = 
    (removeWhite identifier <@ \_ -> id)
    >!< (removeWhite pGlobalStateChange)



removeAdditionlWhite xs 
 =  (unlines (removeAdditionalWhiteLines (lines  xs)))

removeAdditionalWhiteLines [] = []
removeAdditionalWhiteLines [x] = [x]
removeAdditionalWhiteLines (x:xs) = (x:xsNew)
    where
        xsNew = map (drop minSpace) xs
        minSpace = minMember (map (\x-> length (takeWhile (\y->y==' ') x)) xs)

minMember [x] = x
minMember (x:xs)
 |x<minRest = x
 |otherwise = minRest
  where
   minRest = minMember xs


pGlobalStateDeclaration
  = removeWhite identifier    >&< \name -> 
    removeWhite (symbol '=')  >&< \_ ->
    removeWhite pGlobalStateChange

pTaktStrichChange =
      (removeWhite (keyword "|||")  <@ \_ -> (\x -> (initdvielem,x{taktstrichart=Ende})))
  >!< (removeWhite (keyword "||:")  <@ \_ -> (\x -> (initdvielem,x{taktstrichart=AnfangWiederholung})))
  >!< (removeWhite (keyword "||")   <@ \_ -> (\x -> (initdvielem,x{taktstrichart=Doppel})))
  >!< (removeWhite (keyword ":||:") <@ \_ -> (\x -> (initdvielem,x{taktstrichart=EndeAnfangWiederholung})))
  >!< (removeWhite (keyword ":||")  <@ \_ -> (\x -> (initdvielem,x{taktstrichart=EndeWiederholung})))


  
pGlobalStateChange
  =    braces     
        (   removeWhite identifier   >&< \_ -> 
            removeWhite (symbol '&')  >&< \_ ->
            (pStateChange >**< removeWhite(symbol ','))
                                     <@ \xs -> (\x -> foldlist xs x)
        )
    where
        foldlist xs st = foldl f st xs
        f y g = g y

pInternalStateChange
 = pGlobalStateChange   >&< \stchange -> 
   pSeqList             <@ \elem -> 
   \s->elem (stchange s)

--pStateChange :: CParser Char (Global -> Global) a
pStateChange 
    =          pZeilenWeitereSeitenChange 
        >!<    pZeilenErsteSeiteChange 
        >!<    pTonartSymbolChange
        >!<    pTaktstrichart
        >!<    pSegno
        >!<    pSchluessel
        >!<    pDoubleStaff
        >!<    pInstrument
        >!<    pArtabChange
        >!<    pTeilerChange
        >!<    pSystemAnzahlChange
        >!<    pLinienAbstabdChange
        >!<    pStimmeChange
        >!<    pTaktNummerChange
        >!<    ptitleChange
        >!<    psubtitleChange
        >!<    pcomposerChange
        
pSchluessel
  =  removeWhite (keyword "schluessel" )    >&< \_ ->
     removeWhite (symbol '=')                              >&< \_ ->
     removeWhite (cleanlist ppSchluessel)                  <@  \n -> 
     (\x -> x{schluessel=n})

pDoubleStaff
  =  removeWhite (keyword "doubleStaff" )    >&< \_ ->
     removeWhite (symbol '=')                              >&< \_ ->
     removeWhite (cleanlist number)                  <@  \n -> 
     (\x -> x{doubleStaff=n})

ptitleChange    =pchangeStringValue "title"    (\v x  -> x{title=v})
psubtitleChange =pchangeStringValue "subtitle" (\v x  -> x{subtitle=v})
pcomposerChange =pchangeStringValue "composer" (\v x  -> x{composer=v})

pchangeStringValue value f 
 =   removeWhite (keyword value )    >&< \_ ->
     removeWhite (symbol '=')        >&< \_ ->
     removeWhite string               <@ \v ->
     f v

pInstrument
  =  removeWhite (keyword "instrument" )    >&< \_ ->
     removeWhite (symbol '=')                              >&< \_ ->
     removeWhite (cleanlist string)                  <@  \n -> 
     (\x -> x{instrument=n})

pTaktstrichart
  =  removeWhite (keyword "taktstrichart" )    >&< \_ ->
     removeWhite (symbol '=')                              >&< \_ ->
     removeWhite ppTaktstrichart           <@  \n -> 
     (\x -> x{taktstrichart=n})

pSegno
  =  removeWhite (keyword "segno" )    >&< \_ ->
     removeWhite (symbol '=')                              >&< \_ ->
     removeWhite ((keyword "True"  <@ \_-> True)
              >!< (keyword "False" <@ \_-> False))           <@  \n -> 
     (\x -> x{segno=n})

ppSchluessel 
  =   (removeWhite (keyword "Violin")   <@ \_ -> Violin)
   >!<(removeWhite (keyword "Bass")     <@ \_ -> Bass)
   >!<(removeWhite (keyword "Tenor")    <@ \_ -> Tenor)

ppTaktstrichart
  =   (removeWhite (keyword "Normal")                 <@ \_ -> Normal)
   >!<(removeWhite (keyword "EndeWiederholung")       <@ \_ -> EndeWiederholung)
   >!<(removeWhite (keyword "EndeAnfangWiederholung") <@ \_ -> EndeAnfangWiederholung)
   >!<(removeWhite (keyword "AnfangWiederholung")     <@ \_ -> AnfangWiederholung)
   >!<(removeWhite (keyword "Ende")                   <@ \_ -> Ende)
   >!<(removeWhite (keyword "Doppel")                   <@ \_ -> Doppel)


pZeilenWeitereSeitenChange
  =  removeWhite (keyword "anzahlzeilenweitereseiten" )    >&< \_ ->
     removeWhite (symbol '=')                              >&< \_ ->
     removeWhite number                                    <@  \n -> 
     (\x -> x{anzahlzeilenweitereseiten=n})
pZeilenErsteSeiteChange
  =  removeWhite (keyword "anzahlzeilenersteseite")         >&< \_ ->
     removeWhite (symbol '=')                               >&< \_ ->
     removeWhite number                                     <@  \n -> 
     (\x -> x{anzahlzeilenersteseite=n})
-- pTonartChange 
-- =   removeWhite (keyword "tonartzeichen")                  >&< \_ ->
--     removeWhite (symbol '=')                                >&< \_ ->
--     removeWhite pTonart                                    <@  \(tonartName,tonart,off) 
--         -> (\x -> x{keyName=tonartName,artab=off*notendicke})
pTonartSymbolChange
 =   removeWhite (keyword "tonartsymbol")                          >&< \_ ->
     removeWhite (symbol '=')                                >&< \_ ->
     removeWhite pTonart2                                    <@  \tonartName 
         -> (\x -> x{tonartsymbol=tonartName,keyName=show tonartName
                    ,artab=tonartbreite tonartName
                   })

pArtabChange
 =  removeWhite (keyword "artab")                           >&< \_ ->
    removeWhite (symbol '=')                                >&< \_ ->
    removeWhite pabstand                                    <@  \artab -> 
    (\x -> x{artab=artab})
pTeilerChange
 =  removeWhite (keyword "teiler")                          >&< \_ ->
    removeWhite (symbol '=')                                >&< \_ ->
    removeWhite number                                      <@  \teiler -> 
    (\x -> x{teiler=teiler})
pSystemAnzahlChange
 =  removeWhite (keyword "systemanzahl")                  >&< \_ ->
    removeWhite (symbol '=')                              >&< \_ ->
    removeWhite number                                     <@ \systemanzahl-> 
    (\x -> x{systemanzahl=systemanzahl})
pLinienAbstabdChange
 =  removeWhite (keyword "linienabstand")                  >&< \_ ->
    removeWhite (symbol '=')                               >&< \_ ->
    removeWhite pabstand                                    <@  \linienabstand->    
    (\x -> x{linienabstand=linienabstand})
pStimmeChange
 =  removeWhite (keyword "stimme")                          >&< \_ ->
    removeWhite (symbol '=')                                >&< \_ ->
    removeWhite number                                        <@  \sti -> 
    (\x -> x{theStimme=sti})

pTaktNummerChange
 =  removeWhite (keyword "taktNummer")                        >&< \_ ->
    removeWhite (symbol '=')                                  >&< \_ ->
    removeWhite number                                        <@  \sti -> 
    (\x-> x{taktNummer=sti})


pClef
 =     (keyword "Violin"          <@ \_ -> Violin)
   >!< (keyword "Tenor"           <@ \_ -> Tenor)
   >!< (keyword "Bass"            <@ \_ -> Bass)

pTonart2 
 =             (keyword "CDur"          <@ \_ -> CDur)
    >!<        (keyword "GDur"          <@ \_ -> GDur)
    >!<        (keyword "DDur"          <@ \_ -> DDur)
    >!<        (keyword "ADur"          <@ \_ -> ADur)
    >!<        (keyword "EDur"          <@ \_ -> EDur)
    >!<        (keyword "HDur"          <@ \_ -> HDur)
    >!<        (keyword "FisDur"        <@ \_ -> FisDur)
    >!<        (keyword "FDur"          <@ \_ -> FDur)
    >!<        (keyword "BDur"          <@ \_ -> BDur)
    >!<        (keyword "EsDur"         <@ \_ -> EsDur)
    >!<        (keyword "AsDur"         <@ \_ -> AsDur)
    >!<        (keyword "DesDur"        <@ \_ -> DesDur)
    >!<        (keyword "GesDur"        <@ \_ -> GesDur)

unparsedList 
 =  newcleanlist <@ \ls ->  ls

list 
 = removeWhite (symbol '[')                            >&< \t  -> 
   (removeWhite seqList >**< removeWhite (symbol ',')) >&< \xs -> 
   removeWhite  (symbol ']')                            <@ \li -> xs

pList = list


pSeqList = altList

altList
 =   (
   ((seqList) >++< removeWhite (keyword "|/">!<keyword "|")) <@ \ls -> concatWith1 (|/) ls
   )

seqList = (
   p 
    >!&<
     \r -> (>*<) (seqTypes >!&< \sep -> seqElement<@ \el->(el,sep) )  <@ \rs -> (r:rs))
     <@ combine1
  where
   p = seqElement <@ (\e ->(e, \x y -> e))

   combine1 ((el,_):xs) = combine el xs

   combine el [] = el
   combine el ((el2,sep):xs) = combine (sep el el2) xs


seqTypes = 
         ( (epsilon <@(\_->"E") >|< (removeWhite (keyword "--" ))) <@ \_ -> (-/))
    >!<  (removeWhite (keyword "-|-") <@ (\_ -> \x y -> x-/takt-/y))

seqElement
 = 
   ( removeWhite (   (paranthesis altList) >!< pGrace >!< pL>|< 
    ppp >!< pNote  >!< pBalken >!< pBeams >!< pChord >!< pAkkord
   >!< pBogen >!< pSlur >!<pEscape>!<pCres >!< pFahnen >!< pHals
   >!< pMacros >!< pPausen >!< pStaff >!< paranthesis seqList >!< pStimme >!<   
    pInternalStateChange >!< pS
     )
   ) >&< \elem -> posts <@ \p-> p elem

pS = removeWhite (symbol 's') <@ \_ -> s

posts 
 = (post >&< \p1->posts <@ \p2 -> p2.p1) 
   >!< (yield id)

post 
 = ppupu2 >!< ppu2 >!< p3 >!< p5 >!< p6 >!< particulation

pStimme 
 = removeWhite (keyword "stimme") >&< \_ ->
   removeWhite number                >&< \n ->
   seqList                          <@  \xs -> stimme n xs
 
pGrace 
 = removeWhite (keyword "grace") >&< \_ ->
   seqList                       >&< \gr ->
   seqList                       <@  \nr -> Tinte.Grace.grace gr nr

pgr8o
  = removeWhite (keyword "gr8o")  >&< \_ ->
    removeWhite character         >&< \no -> 
    removeWhite number            >&< \n ->
    seqList                       <@ \gr ->
    gr8o no n gr

pg8
  = removeWhite (keyword "v8")  >&< \_ ->
    ptoene                      >&< \no -> 
    number                      >&< \n ->
    seqList                      <@ \gr ->
    gr8o no n gr

--genericlist :: [Char] ([GenElem] -> GenElem) -> CParser Char GenElem d 
genericlist str f 
 = keyword str >&< \_ ->  removeWhite (pelementlist) <@ \li ->f li 

--genEric :: [Char] (GenElem -> GenElem) -> CParser Char GenElem d 
genEric str f 
 = keyword str >&< \_ ->  seqElement <@ \li ->f li 


--genericint ::  [Char] (Int GenElem-> GenElem) -> CParser Char GenElem d 
genericint str f 
 = keyword str                    >&< \_ -> 
   removeWhite number             >&< \n ->
   removeWhite seqList             <@ \li ->f n li 

--create :: [Char] GenElem ->  CParser Char GenElem d 
create str f  = keyword str  <@ \_ ->f  

--createsimpleelement :: [Char] -> f -> CParser Char f a


--pelementlist  :: CParser Char [GenElem] a
pelementlist 
 =      cleanlist seqList 
    >!< pmap 
    >!< (
          removeWhite (symbol '(')      >&< \_ ->
          removeWhite pelementlist        >&< \xs ->
          removeWhite (symbol ')')       <@ \_ ->
          xs)


pn1 = pnote  "1" n1
pn2 = pnote  "2" n2
pn4 = pnote  "4" n4
pn8 = pnote  "8" n8
pn16 = pnote  "16" n16
pn32 = pnote  "32" n32
pn64 = pnote  "64" n64
pn1o = pnote  "1^" n1o
pn2o = pnote  "2^" n2o
pn4o = pnote  "4^" n4o
pn8o = pnote  "8^" n8o
pn16o = pnote  "16^" n16o
pn32o = pnote  "32^" n32o
pn64o = pnote  "64^" n64o
pn1u = pnote  "1_" n1u
pn2u = pnote  "2_" n2u
pn4u = pnote  "4_" n4u
pn8u = pnote  "8_" n8u
pn16u = pnote  "16_" n16u
pn32u = pnote  "32_" n32u
pn64u = pnote  "64_" n64u

pk1 = pkopf  "1" k1
pk2 = pkopf  "2" k2
pk4 = pkopf  "4" k4
pk8 = pkopf  "8" k8
pk16 = pkopf  "16" k16
pk32 = pkopf  "32" k32
pk64 = pkopf  "64" k64

pkopf = pnote

pSimpleKopf
 = removeWhite (symbol 'k') >&< \_ -> 
   concatWith1 (>!<) [pk1,pk2,pk4,pk8,pk16,pk32,pk64]

pSimpleNoten
 = removeWhite (symbol 'n') >&< \_ -> 
   concatWith1 (>!<)
      [pn1o,pn2o,pn4o,pn8o,pn16o,pn32o,pn1u,pn2u,pn4u,pn8u,pn16u,pn32u,pn64u
      ,pn16,pn1,pn2,pn4,pn8,pn32,pn64]

pNote 
 = concatWith1 (>!<) 
  (
  [pgr8o
  ,pg8
  ,pchangekey
  ,pchangeclef
  ,pTaktStrichChange
  ,pSimpleNoten
  ,pSimpleKopf
  ,pmetrum 
  ,pklammer 
  ,pfingerat 
  ,pfingero 
  ,pfingeru 
  ,panwo 
  ,panwu
  ,panwat
  ,pliedtext 
  ,pliedtexth 
  ,ppupu 
  ,ppu 
  ,pstau
  ,pstao
  ,pakzu 
  ,pakzo 
  ,pendwiederholung 
  ,pendwiederholungmitte 
  ,pdoppelstrich 
  ,panfwiederholung 
  ,panfwiederholungmitte
  ,panfendwiederholung 
  ,pendstrich 
  ,pbreittaktstrich 
  ,ptakt
  ,pdicktaktstrich 
  ,pfermateo 
  ,pfermateu
  ,ptro
  ,ptru
  ,ptru
  ,pleft 
  ,pschiebe 
  ,ptriole
   ])>!< ptexts

pnote str f
 =   keyword str >&< \_ -> removeWhite character >&< \no -> removeWhite number 
   <@ \n -> f no n


pmetrum 
 =  keyword "metrum"                 >&< \_ ->
    removeWhite number                 >&< \n1 -> 
    removeWhite number                  <@ \n2 -> 
    metrum n1 n2

pklammer 
 =  keyword "klammer"                >&< \_ ->
    removeWhite number                 >&< \n1 -> 
    removeWhite seqList                 <@ \elem ->
    klammer n1  elem


pfingerat 
 =  keyword "fingerat"               >&< \_ ->
    removeWhite number                 >&< \n1 -> 
    removeWhite number                 >&< \n2 -> 
    removeWhite seqList                 <@ \elem ->
    fingerat n1 n2 elem

pfingero 
 =  keyword "fingero"                >&< \_ ->
    removeWhite number                 >&< \n1 -> 
    removeWhite seqList                 <@ \elem ->
    fingero n1  elem

pfingeru 
 =  keyword "fingeru"                >&< \_ ->
    removeWhite number                 >&< \n1 -> 
    removeWhite seqList                 <@ \elem ->
    fingeru n1  elem


panwat 
 =  keyword "anwat"               >&< \_ ->
    ptoene                             >&< \c ->    
    number                              >&< \n ->
    removeWhite string                 >&< \n2 -> 
    removeWhite seqList                 <@ \elem ->
    anwat c n n2 elem

panwo 
 =  keyword "anwo"                >&< \_ ->
    removeWhite string                 >&< \n1 -> 
    removeWhite seqList                 <@ \elem ->
    anwo n1 elem

panwu 
 =  keyword "anwu"                >&< \_ ->
    removeWhite string                 >&< \n1 -> 
    removeWhite seqList                 <@ \elem ->
    anwu n1  elem


ptextmfnt 
 =  keyword "mfnt"                   >&< \_ ->
    removeWhite number                 >&< \n1 -> 
    removeWhite fontnumber                 >&< \n2 -> 
    removeWhite string                 >&< \str -> 
    removeWhite seqList                 <@ \elem ->
    textmfnt n1 n2 str elem

ptextmfnth 
 =  keyword "mfnth"                  >&< \_ ->
    removeWhite number                 >&< \n1 -> 
    removeWhite fontnumber             >&< \n2 -> 
    removeWhite string                 >&< \str -> 
    removeWhite seqList                 <@ \elem ->
    textmfnth n1 n2 str elem

ptextmfnto 
 =  keyword "mfnto"                  >&< \_ ->
    removeWhite fontnumber                 >&< \n1 -> 
    removeWhite string                 >&< \str -> 
    removeWhite seqList                 <@ \elem ->
    textmfnto n1 str elem

ptextmfntu 
 =  keyword "mfntu"                  >&< \_ ->
    removeWhite fontnumber                 >&< \n1 -> 
    removeWhite string                 >&< \str -> 
    removeWhite seqList                 <@ \elem ->
    textmfntu n1 str elem

ptexto  
 =  keyword "o"                       >&< \_ ->
    removeWhite string                  >&< \str -> 
    removeWhite seqList                  <@ \elem ->
    texto str elem

ptextat
  = keyword "at"                       >&< \_ ->
    ptoene                             >&< \c ->    
    number                              >&< \n ->
    removeWhite string                  >&< \str -> 
    removeWhite seqList                  <@ \elem ->
    textat c n str elem


ptextu  
 =  keyword "u"                       >&< \_ ->
    removeWhite string                  >&< \str -> 
    removeWhite seqList                  <@ \elem ->
    textu str elem

ptextuh  
 =  keyword "uh"                      >&< \_ ->
    removeWhite string                  >&< \str -> 
    removeWhite seqList                  <@ \elem ->
    textuh str elem

pliedtext 
 =  keyword "liedtext"  >&< \_ ->
    removeWhite string                  >&< \str -> 
    removeWhite seqList                  <@ \elem ->
    liedtext str elem

pliedtexth 
 =  keyword "liedtexth"               >&< \_ ->
    removeWhite string                  >&< \str -> 
    removeWhite seqList                  <@ \elem ->
    liedtexth str elem

ptext
 =  removeWhite number                  >&< \n1 -> 
    removeWhite string                  >&< \str -> 
    removeWhite seqList                  <@ \elem ->
    text n1 str elem


ptexts 
 =  keyword "text" >&< \_ ->
     (concatWith1 (>!<) 
      [ptextat,ptextmfnth ,ptextmfnto ,ptextmfntu,ptextmfnt,ptextuh ,ptexto  ,ptextu  ,ptext  ])

modifiyGenElem2 str f 
 =  seqList >&< \elem -> 
    removeWhite (keyword str) <@ \_ -> 
    f elem

modifiyGenElem str f 
 = keyword str >&< \_ -> 
   removeWhite seqList <@ \elem -> 
   f elem

generateElem str f = keyword str  <@ \_ -> f

ppu2 = symbol '.' <@ \_-> pu
ppupu2= keyword ".." <@ \_-> pupu

p3 = keyword "\\3" <@ \_-> triole
p5 = keyword "\\5" <@ \_-> quintole
p6 = keyword "\\6" <@ \_-> sexttole

particulation
 = symbol '-'  >&< \_ ->
   (pArtStacc >!< pArtStaccatissimo >!< pArtSforzato >!< pArtMarcato >!< pArtTenuto >!< pArtFinger )

pArtStacc 
 = symbol '.'  >&< \_ ->
   ((symbol '^' <@ \_->stao) >!< (symbol '_' <@ \_->stau))

pArtStaccatissimo
 = symbol '\''  >&< \_ ->
   ((symbol '^' <@ \_->staccissio) >!< (symbol '_' <@ \_->staccissiu))

pArtMarcato
 = symbol '^'  >&< \_ ->
   ((symbol '^' <@ \_->marcatoo) >!< (symbol '_' <@ \_->marcatou))

pArtTenuto
 = symbol '-'  >&< \_ ->
   ((symbol '^' <@ \_->tenutoo) >!< (symbol '_' <@ \_->tenutou))

pArtSforzato
 = symbol '>'  >&< \_ ->
   ((symbol '^' <@ \_->sforzatoo) >!< (symbol '_' <@ \_->sforzatou))
pArtFinger
 = number  >&< \n ->
   ((symbol '^' <@ \_->fingero n)>!< (symbol '_' <@ \_->fingeru n))
   
ppu = modifiyGenElem "pu" pu 
ppupu = modifiyGenElem "pupu" pupu 

pstau = modifiyGenElem "stau" stau
pstao = modifiyGenElem "stao" stao
pakzu = modifiyGenElem "akzu" akzu
pakzo = modifiyGenElem "akzo" akzo

pgpu = generateElem  "pu" pu
pgstau = generateElem  "stau" stau
pgstao = generateElem  "stao" stao
pgakzu = generateElem  "akzu" akzu
pgakzo = generateElem  "akzo" akzo


pendwiederholung = generateElem "endwiederholung" endwiederholung
pendwiederholungmitte = generateElem "endwiederholungmitte" endwiederholungmitte
pdoppelstrich = generateElem "doppelstrich" doppelstrich
panfwiederholung = generateElem "anfwiederholung" anfwiederholung
panfwiederholungmitte = generateElem "anfwiederholungmitte" anfwiederholungmitte
panfendwiederholung = generateElem "anfendwiederholung" anfendwiederholung
pendstrich = generateElem "endstrich" endstrich
pbreittaktstrich = generateElem "breittaktstrich" breittaktstrich
ptakt = generateElem "takt" takt
pdicktaktstrich = generateElem "dicktaktstrich" dicktaktstrich

pchangekey 
 = keyword "\\changekey"  >&< \_ ->
   removeWhite pTonart2 <@  \key->
   changekey key

pchangeclef 
 = keyword "\\changeclef"  >&< \_ ->
   removeWhite pClef <@  \key->
   changeclef key


pfermateo = modifiyGenElem "fermateo" fermateo
pfermateu = modifiyGenElem "fermateu" fermateu
ptro = modifiyGenElem "tro" tro
ptru = modifiyGenElem "tru" tru

pleft 
 =  removeWhite (keyword "left")     >&< \_ ->
    removeWhite number                  <@ \n1 -> 
    left n1 

pschiebe 
 =  removeWhite (keyword "schiebe")  >&< \_ ->
    removeWhite number                 >&< \n1 -> 
    removeWhite seqList                 <@ \elem ->
    schiebe n1  elem

pgschiebe 
 =  removeWhite (keyword "schiebe")  >&< \_ ->
    removeWhite number                 <@ \n1 -> 
    \x -> schiebe n1  x

-- //left :: Int -> GenElem
 -- //schiebe :: Int GenElem -> GenElem

ptriole = modifiyGenElem "triole" triole

genEric2 str f
 = keyword str                  >&< \_ ->
   removeWhite seqList           <@ \elem ->
   f elem

pf8 = genEric2 "8" f8
pf8u = genEric2 "8u" f8u
pf8o= genEric2 "8o" f8o
pf16 = genEric2 "8" f8
pf16u = genEric2 "16u" f16u
pf16o = genEric2 "16o" f16o
pf32 = genEric2 "8" f8
pf32u = genEric2 "32u" f32u
pf32o = genEric2 "32o" f32o
pf64 = genEric2 "8" f8
pf64u = genEric2 "64u" f64u
pf64o = genEric2 "64o" f64o
pfa = genEric2 "a" fa
pfo = genEric2 "o" fo
pfu = genEric2 "u" fu
pgrfo = genEric2 "grfo" grfo

pFahnen
  = (symbol 'f' >!&< \_ -> 
     concatWith1 (>!<) 
        [pf8,pf8u,pf8o,pf16,pf16u,pf16o,pf32,pf32u,pf32o,pfa,pfo,pfu])
     >!<pgrfo


phals = genEric [] hals
phalso = genEric "o" halso
phalsu = genEric "u" halsu
phalsol = genericint "ol" halsol
phalsul = genericint "ul" halsul

pHals = keyword "hals" >&< \_ -> 
         concatWith1 (>!<) [phals, phalso, phalsu, phalsul, phalsol]


pBalken = removeWhite (keyword "balk")  >&< \_ -> concatWith1 (>!<) 
  [pbalkenou 
  ,pbalkeno 
  ,pbalkenu 
  ,pbalkou
  ,pbalkoat 
  ,pbalkuat 
  ,pbalko 
  ,pbalku
  ]

pEscape 
 = removeWhite (symbol '\\')                   >&< \_   ->
   (pCrescendi>!<pSlurs>!<pDynamics>!<pGlissando>!<pTriller>!<pAnnotations>!<pSatz)

pSatz
 = keyword "movement"    >&< \_ ->
   removeWhite string                          >&< \anno->
   seqList                                      <@ \el  ->
   satz anno el

pAnnotations 
 = keyword "annotation"                         >&< \_   ->
   pLowerAnnotation >!< pUpperAnnotation>!<pEndUpperAnnotation>!<pEndLowerAnnotation

pLowerAnnotation
 = symbol '_'                                  >&< \_   ->
   removeWhite string                          >&< \anno->
   seqList                                      <@ \el  ->
   startLower anno  el

pUpperAnnotation
 = symbol '^'                                  >&< \_   ->
   removeWhite string                          >&< \anno->
   seqList                                      <@ \el  ->
   startUpper anno  el

pEndUpperAnnotation 
 = keyword "!^"                                >&< \_   ->
   seqList                                      <@ \el  ->   
   endUpperAnnotation el

pEndLowerAnnotation 
 = keyword "!_"                                >&< \_   ->
   seqList                                      <@ \el  ->   
   endLowerAnnotation el

pCrescendi 
 = ((symbol '<'  <@ \_ ->  crescendo)>!<
    (symbol '>'  <@ \_ ->  decrescendo) )      >&< \c   ->
   seqList                                     >&< \el  ->
   removeWhite (keyword "\\!")                   <@ \_  ->
   c el



pGlissando 
 = symbol '~'                                  >&< \c   ->
   seqList                                     >&< \el  ->
   removeWhite (keyword "\\~!")                   <@ \_  ->
   glissando  el


pTriller 
 = keyword "tr"                                  >&< \c   ->
   seqList                                       >&< \el  ->
   removeWhite (keyword "\\tr!")                   <@ \_  ->
   triller  el


pSlurs
 = symbol '('                                  >&< \_   ->
   ((symbol '^'  <@ \_ ->  slurAlla)>!<
    (symbol '_'  <@ \_ ->  slurAllu) )         >&< \c   ->
   seqList                                     >&< \el  ->
   removeWhite (keyword "\\)")                   <@ \_  ->
   c el

pDynamics
 = pDymsymbol                                  >&< \sym  ->
   (   (keyword "__"  <@ \_  -> dynamictextat 'a' (-1))
    >!<(symbol '_'  <@ \_  -> dynamictextat 'd' (0))
    >!<(symbol '^'  <@ \_  -> dynamictextat 'c' 3)
    >!< yield (dynamictextat 'a' 0))               >&< \fff -> 
   seqList                                      <@ \el  ->
   fff sym el

pDymsymbol = foldl1 (>!<)$map keyword ["fff","ff","fz","f","ppp","pp","pf","p","mf","sf"]

pintquartuple 
 =  removeWhite (symbol '(')      >&< \_ ->
    removeWhite number            >&< \n1 ->
    removeWhite (symbol ',')      >&< \_ ->
    removeWhite number            >&< \n2 ->
    removeWhite (symbol ',')      >&< \_ ->
    removeWhite number            >&< \n3 ->
    removeWhite (symbol ',')      >&< \_ ->
    removeWhite number            >&< \n4 ->
    removeWhite (symbol ')')       <@ \_ ->
   (n1,n2,n3,n4)
   
pintquartuplelist = cleanlist pintquartuple  

pbalkoat 
 =  keyword "oat"                   >&< \_ ->
   removeWhite number                 >&< \n ->
   pintquartuplelist                  >&< \li ->
   seqElement                          <@ \elem ->
   balkoat n (map (\(a,b,c,d)->(a,(1,b),c,d)) li) elem
pbalkuat 
 =  keyword "uat"                   >&< \_ ->
   removeWhite number                 >&< \n ->
   pintquartuplelist                  >&< \li ->
   seqElement                          <@ \elem ->
   balkuat n (map (\(a,b,c,d)->(a,(1,b),c,d)) li) elem

pbalkeno 
 = keyword "eno"                     >&< \_ ->
   pelementlist                         <@ \li ->
   balkeno li
pbalkenu 
 = keyword "enu"                     >&< \_ ->
   pelementlist                         <@ \li ->
   balkenu li

pbalkenou 
 = keyword "enou"                    >&< \_ ->
   cleanlist number                    >&< \is1 ->
   cleanlist number                    >&< \is2 ->
   pelementlist                         <@ \li ->
   balkenou is1 is2 li

pBeams
 = removeWhite$ keyword "["              >&< \_ ->
   commasep  seqList                     >&< \li ->   
   removeWhite$keyword "]"               >&< \_ ->
   ((pBeama li) >!< (pBeamu li))  

pBeama li
 = keyword "^"                           <@ \_ ->
   balkeno li

pBeamu li
 = keyword "_"                           <@ \_ ->
   balkenu li

pbalko 
 = symbol 'o'                           >&< \_ ->
   pintquartuplelist                    >&< \is1 ->
   seqElement                            <@ \li ->
   balko (map (\(a,b,c,d)->(a,(1,b),c,d)) is1) li
pbalku
 = symbol 'u'                           >&< \_ ->
   pintquartuplelist                    >&< \is1 ->
   seqElement                            <@ \li ->
   balku (map (\(a,b,c,d)->(a,(1,b),c,d)) is1) li
pbalkou
 = keyword "ou"                       >&< \_ ->
   pintquartuplelist                    >&< \is1 ->
   pintquartuplelist                    >&< \is2 ->
   seqElement                            <@ \li ->
   balkou (map (\(a,b,c,d)->(a,(1,b),c,d)) is1) (map (\(a,b,c,d)->(a,(1,b),c,d)) is2)  li

{--
pnn1 = pnote  "n1" n1
pnn2 = pnote  "n2" n2
pnn4 = pnote  "n4" n4
pnn8 = pnote  "n8" n8
pnn16 = pnote "n16" n16
pnn32 = pnote "n32" n32
pnn64 = pnote "n64" n64
pnn1o = pnote "n1o" n1o
pnn2o = pnote "n2o" n2o
pnn4o = pnote "n4o" n4o
pnn8o = pnote "n8o" n8o
pnn16o = pnote "n16o" n16o
pnn32o = pnote "n32o" n32o
pnn64o = pnote "n64o" n64o
pnn1u = pnote  "n1u" n1u
pnn2u = pnote  "n2u" n2u
pnn4u = pnote  "n4u" n4u
pnn8u = pnote  "n8u" n8u
pnn16u = pnote "n16u" n16u
pnn32u = pnote "n32u" n32u
pnn64u = pnote "n64u" n64u

pnote str f
 =   keyword str >&< \_ -> removeWhite character >&< \no -> removeWhite number 
   <@ \n -> f no n
--}   
fontnumber =
  number
  >!<  thisNumber "fnttext"  fnttext    
  >!<  thisNumber "fntmusic" fnttext    
  >!<  thisNumber "fntbalken" fntbalken      
  >!<  thisNumber "fntbogengerade" fntbogengerade    
  >!<  thisNumber "fntbogenuntenh" fntbogenuntenh    
  >!<  thisNumber "fntbogenuntenr" fntbogenuntenr  
  >!<  thisNumber "fntbogenobenh" fntbogenobenh    
  >!<  thisNumber "fntbogenobenr" fntbogenobenr    
  >!<  thisNumber "fntgracemusic" fntgracemusic  
  >!<  thisNumber "fntgracebalken" fntgracebalken    
  >!<  thisNumber "fntgracebogengerade"  fntgracebogengerade
  >!<  thisNumber "fntgracebogenuntenh"  fntgracebogenuntenh
  >!<  thisNumber "fntgracebogenuntenr"  fntgracebogenuntenr
  >!<  thisNumber "fntgracebogenobenh" fntgracebogenobenh   
  >!<  thisNumber "fntgracebogenobenr" fntgracebogenobenr  
  >!<  thisNumber "fntanweisung" fntanweisung    
  >!<  thisNumber "fntfinger" fntfinger      

thisNumber str i = keyword str <@ \_ -> i
   
-- pmetrum = createsimpleelement "metrum" metrum   
pgklammer = createsimpleelement "klamme" klammer

--pgfingerat:: CParser Char (Int -> Int -> GenElem -> GenElem) a
--pgfingerat = createsimpleelement "fingerat" fingerat
pgfingero = createsimpleelement "fingero" fingero
pgfingeru = createsimpleelement "fingeru" fingeru
pgtextmfnt = createsimpleelement "textmfnt" textmfnt
pgtextmfnth = createsimpleelement "textmfnth" textmfnth
pgtextmfnto = createsimpleelement "textmfnto" textmfnto
pgtextmfntu = createsimpleelement "textmfntu" textmfntu
pgtexto = createsimpleelement "texto" texto
pgtextu = createsimpleelement "textu" textu
pgtextuh = createsimpleelement "textuh" textuh
pgliedtext = createsimpleelement "liedtext" liedtext
pgliedtexth = createsimpleelement "liedtexth" liedtexth
pgtext = createsimpleelement "text" text

pgendwiederholung = createsimpleelement "endwiederholung" endwiederholung
pgendwiederholungmitte = createsimpleelement "endwiederholungmitte" endwiederholungmitte
pgdoppelstrich = createsimpleelement "doppelstrich" doppelstrich
pganfwiederholung = createsimpleelement "anfwiederholung" anfwiederholung
pganfwiederholungmitte = createsimpleelement "anfwiederholungmitte" anfwiederholungmitte
pganfendwiederholung = createsimpleelement "anfendwiederholung" anfendwiederholung
pgendstrich = createsimpleelement "endstrich" endstrich
pgbreittaktstrich = createsimpleelement "breittaktstrich" breittaktstrich
pgfermateo = createsimpleelement "fermateo" fermateo
pgfermateu = createsimpleelement "fermateu" fermateu
pgtro = createsimpleelement "tro" tro
pgtru = createsimpleelement "tru" tru

pgtakt = createsimpleelement "takt" takt
pgdicktaktstrich = createsimpleelement "dicktaktstrich" dicktaktstrich
pgleft = createsimpleelement "left" left
--pgschiebe = createsimpleelement "schiebe" schiebe

pgtriole = createsimpleelement "triole" triole

ppp 
 = concatWith1 (>!<) 
    [pgendwiederholung ,pgendwiederholungmitte,pgdoppelstrich,pganfwiederholung
    ,pganfwiederholungmitte,pganfendwiederholung,pgendstrich,pgbreittaktstrich,pgtakt,pgdicktaktstrich
    ]
   >!<
   concatWith1 (>!<) (map (\pf-> pf >&< \f -> seqElement <@ \elem -> f elem )
    [pgpu,pgstau,pgstao,pgakzu,pgakzo,pgfermateo,pgfermateu,pgtro,pgtru,pgtriole])
   >!< pintelem
{--   >!< 
       pstringelem
   >!< pintstringelem
   >!< pintintstringelem
   >!< pmetrum
   >!< pintintelem
   >!< pabstandelem--}

pintelem = 
      concatWith1 (>!<) (map 
      (\pf-> pf >&< \f -> removeWhite number >&< \ n -> seqElement <@ \elem -> f n elem )
       [pgklammer,pgfingero,pgfingeru])

pabstandelem 
  = 

               pgschiebe >&< \f ->
               removeWhite seqElement <@ \elem -> 
               f  elem 


pstringelem = 
  concatWith1 (>!<) (map 
  (\pf-> pf >&< \f -> removeWhite string >&< \ s -> seqElement <@ \elem -> f s elem )
    [pgtexto,pgtextu,pgtextuh,pgliedtext,pgliedtexth ])
    
--pintint
-- = pmetrum >&< \f ->  removeWhite number >&< \n1 ->removeWhite number  <@ \n2 -> f n1 n2 

--pint
-- = pleft >&< \f ->  removeWhite number   <@ \n -> f n

pintintelem
 =  keyword "fingerat"               >&< \_ ->
    removeWhite number                 >&< \n1 -> 
    removeWhite number                 >&< \n2 -> 
    removeWhite seqList                 <@ \elem ->
    fingerat n1 n2 elem


{--pintstringelem 
 = concatWith1 (>!<) (map 
   (\pf-> pf >&< \f ->  
    removeWhite fontnumber >&< \n ->
    removeWhite string >&< \s -> 
    seqElement <@ \elem -> 
    f n s elem)  
   [ptextmfnto,ptextmfntu,ptext])

pintintstringelem 
 = concatWith1 (>!<) (map 
   (\pf-> pf >&< \f -> removeWhite pabstand >&< \n1 ->removeWhite fontnumber >&< \n2 ->
          removeWhite string >&< \s -> seqElement <@ \elem -> f n1 n2 s elem)  
   [ptextmfnt,ptextmfnth ])
--}   
pmap 
 =  paranthesis
     (
       keyword "map"                   >&< \_ ->
       removeWhite pModifier             >&< \f ->
       removeWhite (cleanlist seqElement) <@ \xs ->
       map f xs
     )

pModifier = pModifieraux >!< paranthesis pModifier 
   
pModifieraux
 = concatWith1 (>!<) 
    [pgpu
    ,pgstau
    ,pgstao
    ,pgakzu
    ,pgakzo
    ,pgschiebe 
    ]  


pakk = genericlist [] akk
pakkmax = genericlist "max" akk
pakko = genericlist "o" akko
pakku = genericlist "u" akku
-- palt = genericlist "alt" Tinte.Akkord.alt

pAkkord
  = (keyword "akk" >!&< \_ ->(concatWith1 (>!<) [pakk,pakko,pakku,pakkmax]))
   --    >!< palt

pChord
 = removeWhite(symbol '<')  >&< \_  ->
   commasep  seqList        >&< \li ->   
   removeWhite(symbol '>')  >&<   \_ ->
     ((symbol '^' <@ \_-> akko li )>!< (symbol '_' <@ \_-> akku li)
       >!< yield(akk li) )

ppair p
 = removeWhite (symbol '(')      >&< \_ ->
   removeWhite p                 >&< \n1 ->
   removeWhite (symbol ',')      >&< \_ ->
   removeWhite p                 >&< \n2 ->
   removeWhite (symbol ')')       <@ \_ ->
   (n1,n2)

pintpair = ppair number
ppairpair = ppair pintpair
ppairpairlist = cleanlist ppairpair

pbogenu 
 = 
   (keyword "u") >&< \_ ->
   pintpair                         >&< \p1 ->
   pintpair                         >&< \p2 ->
   seqElement                        <@ \elem ->
   bogenu p1 p2 elem

pslura
 = (keyword "^") >&< \_ ->
   removeWhite number               >&<  \n1 -> 
   removeWhite number               >&<  \n2 -> 
   seqElement                        <@ \elem ->
   slura n1 n2 elem

psluru
 = (keyword "_") >&< \_ ->
   removeWhite number               >&<  \n1 -> 
   removeWhite number               >&<  \n2 -> 
   seqElement                        <@ \elem ->
   sluru n1 n2 elem

pbogeno
 =   (keyword "o") >&< \_ ->
   pintpair                         >&< \p1 ->
   pintpair                         >&< \p2 ->
   seqElement                        <@ \elem ->
   bogeno p1 p2 elem
pbogenuat
 = (keyword "uat") >&< \_ ->
   removeWhite number                 >&< \n ->
   pintpair                           >&< \p1 ->
   pintpair                           >&< \p2 ->
   seqElement                          <@ \elem ->
   bogenuat n p1 p2 elem
pbogenoat
 =   (keyword "oat") >&< \_ ->
   removeWhite number                 >&< \n ->
   pintpair                           >&< \p1 ->
   pintpair                           >&< \p2 ->
   seqElement                          <@ \elem ->
   bogenoat n p1 p2 elem
pbogenlu 
 =  (keyword "lu")  >&< \_ ->
   ppairpairlist                      >&< \pp ->
   removeWhite seqElement              <@ \elem ->
   bogenlu pp elem
pbogenlo 
 =   (keyword "lo")  >&< \_ ->
   ppairpairlist                      >&< \pp ->
   removeWhite seqElement              <@ \elem ->
   bogenlo pp elem
     
 
pBogen 
  = keyword "bogen" >!&< \_ ->
    concatWith1 (>!<) [pbogenu,pbogenlu,pbogeno,pbogenlo,pbogenuat,pbogenoat]

pSlur
 = keyword "slur" >!&< \_ ->
   (pslura >!< psluru)



pPausen = removeWhite (symbol 'p') >&< \_ -> ppausenaux

ppausenaux 
 = concatWith1 (>!<) 
     [b1pur,b2pur,b1o,b2o,b4o,b8o,b32o,b64o
     ,b1u,b2u,b4u,b8u,b16u,b32u,b64u,b2
     ,b4,b8,b16o,b16,b32,b64,b1]

b1 = create "1" p1
b2 = create "2" p2
b1pur = create "1pur" p1pur
b2pur = create "2pur" p2pur
b4 = create "4" p4
b8 = create "8" p8
b16 = create "16" p16
b32 = create "32" p32
b64 = create "64" p64
b1o = create "1o" p1o
b2o = create "2o" p2o
b4o = create "4o" p4o
b8o = create "8o" p8o
b16o = create "16o" p16o
b32o = create "32o" p32o
b64o = create "64o" p64o
b1u = create "1u" p1u
b2u = create "2u" p2u
b4u = create "4u" p4u
b8u = create "8u" p8u
b16u = create "16u" p16u
b32u = create "32u" p32u
b64u = create "64u" p64u

genEric3 str f 
 = keyword str                     >&< \_ ->
   removeWhite number              >&< \n1 ->
   removeWhite number              >&< \n2 ->
   seqList                          <@ \elem ->
   f n1 n2 elem

generic2 str f 
 = keyword str                     >&< \_ ->
   removeWhite number              >&< \n1 ->
   removeWhite number              >&< \n2 ->
   removeWhite number              >&< \n3 ->
   seqList                          <@ \elem ->
   f n1 n2 n3 elem

  
pcres = genEric3 "cres" cres
pdecr = genEric3 "decr" decr
pcrest = generic2  "crest" crest
pdecrt = generic2  "decrt" decrt
pCres =concatWith1 (>!<) [pcres,pdecr,pcrest,pdecrt]

pStaff
 = keyword "staff"   >&< \_ ->
   removeWhite number  >&< \n -> 
   removeWhite seqList  <@ \elem -> staff n elem


pc = symbol 'c'
pd = symbol 'd'
pe = symbol 'e'
pf = symbol 'f'
pg = symbol 'g'
pa = symbol 'a'
ph = symbol 'h'
pb = symbol 'b'

pC = symbol 'C'
pD = symbol 'D'
pE = symbol 'E'
pF = symbol 'F'
pG = symbol 'G'
pA = symbol 'A'
pH = symbol 'H'
pB = symbol 'B'

ptoene = concatWith1 (>!<) [pc,pd,pe,pf,pg,pa,ph,pC,pD,pE,pF,pG,pA,pH,pB]

pL 
 = ptoene                             >&< \c ->
   (
        (number      >&< \i -> 
           (    (symbol '^' <@ \_ -> (kEnv2 c i Up)) 
            >!< (symbol '_' <@ \_ -> (kEnv2 c i Down))   
            >!< (yield$ kEnv2 c i Automatic)   
           )
        )       
     >!< (      (symbol '^'<@ \_->kEnv c Up) 
            >!< (symbol '_'<@ \_->kEnv c Down)   
            >!< (yield$ kEnv c Automatic)   
         )
    )


genk str f
 = keyword str                        >&< \_ -> 
   ptoene                             >&< \c ->
   number                              <@ \n ->
   f c n           
genn str f
 = keyword str                        >&< \_ -> 
   ptoene                             >&< \c ->
   number                              <@ \n ->
   f c n           

pkn
 = (removeWhite (symbol 'n') >&< \_ -> 
       concatWith1 (>!<) [pmn2o,pmn4o,pmn8o,pmn16o,pmn32o,pmn64o
                         ,pmn2u,pmn4u,pmn8u,pmn16u,pmn32u,pmn64u
                         ,pmn1,pmn2,pmn4,pmn8,pmn16,pmn32,pmn64
                         ])
   >!< (removeWhite (symbol 'k') >&< \_ -> 
       concatWith1 (>!<) [pmk1,pmk2,pmk4,pmk8,pmk16,pmk32,pmk64])

pmk1 = genk "1" k1
pmk2 = genk "2" k2
pmk4 = genk "4" k4
pmk8 = genk "8" k8
pmk16 = genk "16" k16
pmk32 = genk "32" k32
pmk64 = genk "64" k64

pmn1 = genn "1" n1
pmn2 = genn "2" n2
pmn4 = genn "4" n4
pmn8 = genn "8" n8
pmn16 = genn "16" n16
pmn32 = genn "32" n32
pmn64 = genn "64" n64
pmn2o = genn "2^" n2o
pmn4o = genn "4^" n4o
pmn8o = genn "8^" n8o
pmn16o = genn "16^" n16o
pmn32o = genn "32^" n32o
pmn64o = genn "64^" n64o
pmn2u = genn "2_" n2u
pmn4u = genn "4_" n4u
pmn8u = genn "8_" n8u
pmn16u = genn "16_" n16u
pmn32u = genn "32_" n32u
pmn64u = genn "64_" n64u
       
pohnzw = genEric "ohnzw" ohnzw
plift = genericint "lift" lift

parpeggio = genEric "arpeggio" arpeggio

pis = genEric "is" is
pes = genEric "es" es
pna = genEric "na" na

pisis = genEric "isis" isis
peses = genEric "eses" eses

pis2 = genEric "#" is
pes2 = genEric "b" es
pisis2 = genEric "##" isis
peses2 = genEric "bb" eses

psmallMod = genEric "smallMod" smallMod
pnormalMod = genEric "normalMod" normalMod


--psmall = create "small" small
--pnormal = create "normal" normal
pcenter= genEric "center" center

pMacros
  = concatWith1 (>!<) 
    [pkn,psmallMod,pnormalMod
    ,pohnzw,plift,parpeggio,pisis2,pisis,pis2,pis,peses2,peses,pes2,pes,pna,pcenter]









