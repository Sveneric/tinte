module Tinte.PP where 

import Data.Char
import Data.Ratio
import Debug.Trace
import Tinte.Rat
import Tinte.Data
import Tinte.Constants
import Tinte.Util
import Tinte.Macros
import Tinte.Hals
import Tinte.Noten
import Tinte.Balken
import Tinte.Bogen
import Tinte.AltSeq
import Tinte.Fahnen
import Tinte.Grace


n1 c i= k1 c i
n2 c i = hals (k2 c i)
n4 c i = hals (k4 c i)
n8 c i = fa (hals (k8 c i))    
n16 c i = fa (hals (k16 c i))    
n32 c i = fa (hals (k32 c i))    
n64 c i = fa (hals (k64  c i))    




metrum :: Int -> Int ->GenElem
metrum z n 
 = result initdvielem{maxbreite  = metrummax
          ,minbreite  = metrummin
          ,maxzw    = (metrummax-ganznotendicke)
          ,minzw    = (metrummin-ganznotendicke)
          ,zwischenr = quant (metrummin-ganznotendicke) (metrummax-ganznotendicke)
          ,hoehe    = 4
          ,tiefe    = 4
          ,offset    = 0
          ,schlaege  = [(1,2)]
          ,ref_hoehe  = 4
          ,parts    = Term pa}
  where
   notametrum
    |z==4 && n==4 = onotation  [PushPos,MoveDown (-4*halbtonab),MoveRight (-notendicke`div` 2),PutMetrum4_4,PopPos]
    |otherwise = onotation  [PushPos,MoveRight (-notendicke`div` 2),PutMetrum z n,PopPos]

   pa 
    = initpart
       {poffset    = 0
       ,pwoliegt  = \z n -> 0
       ,ptiefe    = 4
       ,phoehe    = 4
       ,pnotation = quantmitnotation notametrum metrummin metrummax
       ,pschlag    = (1,2)
       ,ppostdefs  = []
       }
   zoff |z<10 && n>=10 = notendicke `div`  2 |otherwise = 0
   noff |n<10 && z>=10 = notendicke `div`  2 |otherwise = 0

triole :: GenElem -> GenElem
triole alt = nfuerm 3 2 alt

quintole :: GenElem -> GenElem
quintole alt = nfuerm 5 4 alt

sexttole alt = nfuerm 6 4 alt

nfuerm n m alts s 
   |isNull oldparts  = (alt,s1)
   |otherwise      
       = (alt  { parts     = newparts
                ,maxbreite    = maxbn
                ,minzw      = div (minzw alt*m) n
                ,maxzw      = div (maxzw alt*m) n
                ,zwischenr    = \zz nn -> zwischenr alt (zz*m) (n*nn)
                ,schlaege     = map (\(z,n1)->reduce(z*m,n1*n)) slgs
                },s1)
   where
    (alt,s1) = alts s
    oldparts = parts alt
    newparts = fmap (fuer n m) oldparts
    maxbn      =  div (maxbreite alt*m) n
    slgs = schlaege alt

fuer n1 m1 part 
  =  part  
     { pwoliegt = \z n -> (wo (z*m1) (n*n1))
     , pschlag=pschlagNeu
     , def = changeDur (\_-> (fst pschlagNeu%snd pschlagNeu))(def part)
     }
 where
  pschlagNeu=reduce (z*m1,n*n1)
  wo = pwoliegt part
  (z,n) = pschlag part


fingerat i zahl alts = textmfnth i fntfinger (show zahl) alts
anwatn i te alts = textmfnth i fntfinger (te) alts
anwat c i te alts g= anwatn (hoehe+1) (te) alts g
  where
    (alt,g2) = alts g 
    hoehe = setzenoteum ((schluessel g2)!!(currentVoice g2-1)) c i  
anwo te alts s= anwatn (hoehe alt-2) te alts s
  where
       (alt,s1) = alts s 
anwu te alts s = anwatn (hoehe alt+2) te alts s
  where
       (alt,s1) = alts s

satz name alts g
  = textmfnth2  (schluesselab*(-2 )) (h)  fntNormal name alts g
 where
  h = setzenoteum ((schluessel g)!!(currentVoice g-1)) 'c' 3  

fingero zahl alts = aux 
 where
  aux s = fingerat (h-2) zahl alts s
   where
     (alt,s1) = alts s
     h = hoehe alt

fingeru zahl alts = aux 
 where
  aux s = fingerat (h+4) zahl alts s
   where
     (alt,s1) = alts s
     h = tiefe alt

textmfnth i fnt text alts = textmfnth2 0  i  fnt text alts
textmfnthsub i fnt text alts
  = textmfnth2 ((-(length text)*notendicke*2) 
    + ((length text `div` 2)*(notendicke `div` 2)))  i  fnt text alts


textmfnth2  off i fnt text alts s 
 |isNull partsalt = (alt,s)
 |otherwise       = (alt{ parts = partsneu},s1)
   where
    (alt,s1) = alts s
    textnota
     =onotation 
      ([PushPos
       ,PushPos
       ,MoveDown (i*halbtonab),MoveRight off
       ,PutText (fntNumberToPSFont fnt) text
       ]++[PopPos,PopPos])
    partsalt = parts alt
    oneu p   = p{pvorsatznotation = textnota+++pvorsatznotation p}
    partsneu  = changeHead oneu partsalt

textmfnt i fnt text alts = t i fnt text alts
 where
  t i fnt text alts s 
   |isNull partsalt = (alt,s1)
   |otherwise       = (alt  {parts  = partsneu},s1)
   where
    (alt,s1) = alts s
    textnota
     =onotation 
      ([PushPos
       ,PushPos
       ,MoveDown (i*halbtonab)
       ,MoveRight (-(2*notendicke))
       ,PutText (fntNumberToPSFont fnt) text
       ,PopPos,PopPos])
    partsalt = parts alt
    oneu p   = p{pvorsatznotation = textnota+++pvorsatznotation p}
    partsneu  = changeHead oneu partsalt


textmfntstaff i staff fnt text alts s 
   |isNull partsalt = (alt,s1)
   |otherwise       = (alt  {parts  = partsneu},s1)
   where
    (alt,s1) = alts s
    textnota
     =onotation 
      ([PushPos
       ,PushPos
       ,MoveDown (i*halbtonab-putatstaff staff)
       ,MoveRight (-(2*notendicke))
       ,PutText (fntNumberToPSFont fnt) text
       ,PopPos,PopPos])
    partsalt = parts alt
    oneu p   = p{pvorsatznotation = textnota+++pvorsatznotation p}
    partsneu  = changeHead oneu partsalt


textmfntostaff staff fnt text alt = textmfntstaff ((-14)) staff fnt text alt
textmfnto fnt text alt = textmfnt (-14) fnt text alt
textmfntu fnt text alt = textmfnt 4 fnt text alt
textu text alt = textmfntu fntanweisung text alt

textat c i text el g1
  = textmfnt hoehe fntanweisung text el g2
   where
    (alt,g2) = el g1
    hoehe = setzenoteum ((schluessel g2)!!(currentVoice g2-1)) c i  

textatleft c i text el g1
  = textmfnthsub hoehe fntdynamic text el g2
   where
    (alt,g2) = el g1
    hoehe = setzenoteum ((schluessel g2)!!(currentVoice g2-1)) c i  

dynamictextat c i text el g1
  = textmfnthsub hoehe fntdynamic text el g2
   where
    (alt,g2) = el g1
    hoehe = setzenoteum Violin c i  

textuh text alt = textmfntuh fntanweisung text alt
liedtext text alt = textmfnt 6 fntanweisung text alt
liedtexth text alt = textmfnth 6 fntanweisung text alt
textmfntuh fnt text alt = textmfnth 4 fnt text alt
texto text alt = textmfnto fntanweisung text alt
textostaff staff text alt = textmfntostaff staff fntanweisung text alt
text i  text alt = textmfnt i fntanweisung text alt


data ChrTok = Uml Char |Chr Char

tokenstr xs = tok chrlist
  where
    chrlist :: [Char]
    chrlist =   xs

tok [] = []
tok ('"':c:xs) = (Uml c:tok xs)
tok "\"" = []
tok (c:xs) = (Chr c:tok xs)

punktbreite = notendicke
punktab    = div notendicke 3


punktnota = onotation [PutDot]

pu alts s 
  = (alt {   maxbreite = maxn,
         minbreite = minn,
         parts  = partsneu
         ,schlaege=map (\(zk,zn) ->add (zk,zn) (zk,zn*2)) $schlaege alt},s1)
   where
    (alt,s1) = alts s
    maxb   = maxbreite alt
    minb   = minbreite alt
    ref    = ref_hoehe alt
    min   = minb-minzww
    max    = maxb-maxzww
    maxzww   = maxzw alt
    minzww   = minzw alt
    maxn  = maxb+punktbreite
    minn  = minb+punktbreite
    kor |(mod ref  2) == 0 = div halbtonab 2|otherwise = 0 --div halbtonab 3
    off   = offset alt
    partsalt  = parts alt
    partsneu  = changeHead pneu partsalt 
    pneu p   = p {pschlag=schlagNeu,pnotation=notan
                 ,def=changeDur (\_->schla%gNeu) (def p )}
     where
      (zk, zn) = pschlag p
      schlagNeu = add (zk,zn) (zk,zn*2)
      (schla,gNeu)=schlagNeu
      notan z n= quantmitnotation( 
                 (pnotation p z n)
            +++  onotation [MoveDown ((ref*halbtonab)-kor)]
            +++  onotation [MoveRight (-quant minb maxb z n)]
            +++  onotation [MoveRight (notendicke+punktab)]+++
          punktnota
          ) (minn-off) (maxn-off) z n



pupu alts s 
  = (alt {   maxbreite = maxn,
             minbreite = minn,
             parts  = partsneu},s1)
   where
    (alt,s1) = alts s
    maxb   = maxbreite alt
    minb   = minbreite alt
    ref    = ref_hoehe alt
    min   = minb-minzww
    max    = maxb-maxzww
    maxzww   = maxzw alt
    minzww   = minzw alt
    maxn  = maxb+punktbreite
    minn  = minb+punktbreite
    kor |(mod ref  2) == 0 = div halbtonab 2|otherwise = 0 --div halbtonab 3
    off   = offset alt
    partsalt  = parts alt
    partsneu  = changeHead pneu partsalt 
    pneu p   = p {pschlag=schlagNeu
--                 ,pdvi = dvin
                 ,pnotation=notan1
                 ,def=changeDur (\_->schla%gNeu) (def p )}
     where
      (zk, zn) = pschlag p
      schlagNeu = add (add (zk,zn) (zk,zn *2)) (zk,zn *4)
      (schla,gNeu)=schlagNeu
      notan1  z n= quantmitnotation( 
                 (pnotation p z n)
            +++  onotation [MoveDown ((ref*halbtonab)-kor)]
            +++  onotation [MoveRight (-quant minb maxb z n)]
            +++  onotation [MoveRight (notendicke+punktab)]+++
             punktnota+++onotation [MoveRight (2*punktab)]+++punktnota
          ) (minn-off) (maxn-off) z n


--klammer :: Int GenElem ->GenElem
klammer i alts g
  = (alt,gneu{Tinte.Data.klammer=Just i})
  where
   (alt,gneu) = alts g

stau :: GenElem -> GenElem
stau alts = st alts
 where
  st alts s = (alt {parts  = partsneu},s1)
   where
    (alt,s1)= alts s
    ref    = ref_hoehe alt
    tie    = tiefe alt
    kor |mod ref  2 == 0 = halbtonab*3|otherwise = (halbtonab*2)
    partsalt  = parts alt
    partsneu  = changeHead pneu partsalt
    pneu p   = p {pvorsatznotation = notan+++pvorsatznotation p
                 }
      where              
       notan = onotation
                  [PushPos,MoveDown ((tie*halbtonab)+kor)
                  ,MoveRight (notendicke`div` 3+poff),PushPos,PutStaccato,PopPos,PopPos]
       poff    = poffset p

stao :: GenElem ->GenElem
stao alts = st alts
 where
  st alts s = (alt {parts  = partsneu},s1)
   where
    (alt,s1)= alts s
    ref    = ref_hoehe alt
    kor |ref< -6 = halbtonab*2
        |mod ref  2 == 0 = halbtonab+ (halbtonab `div` 4)
        |otherwise = halbtonab*2
    partsalt  = parts alt
    partsneu  = changeHead pneu partsalt
    pneu p  = p {pvorsatznotation = notan+++pvorsatznotation p}
      where
        notan  =   onotation (   [PushPos,MoveDown (((ref-4)*halbtonab)+kor)
              ,MoveRight (notendicke`div` 3+poff),PushPos,PutStaccato,PopPos,PopPos])
        poff    = poffset p



staccissiu :: GenElem -> GenElem
staccissiu alts = st alts
 where
  st alts s = (alt {parts  = partsneu},s1)
   where
    (alt,s1)= alts s
    ref    = ref_hoehe alt
    tie    = tiefe alt
    kor |mod ref  2 == 0 = halbtonab*3|otherwise = (halbtonab*2)
    partsalt  = parts alt
    partsneu  = changeHead pneu partsalt
    pneu p   = p {pvorsatznotation = notan+++pvorsatznotation p}
      where              
       notan = onotation
                  [PushPos,MoveDown ((tie*halbtonab)+kor)
                  ,MoveRight (notendicke`div` 3+poff),PushPos,PutStaccatissimou,PopPos,PopPos]
       poff    = poffset p

staccissio :: GenElem ->GenElem
staccissio alts = st alts
 where
  st alts s = (alt {parts  = partsneu},s1)
   where
    (alt,s1)= alts s
    ref    = ref_hoehe alt
    kor |ref< -6 = halbtonab*2
        |mod ref  2 == 0 = halbtonab+ (halbtonab `div` 4)
        |otherwise = halbtonab*2
    partsalt  = parts alt
    partsneu  = changeHead pneu partsalt
    pneu p  = p {pvorsatznotation = notan+++pvorsatznotation p}
      where
        notan  =   onotation (   [PushPos,MoveDown (((ref-4)*halbtonab)+kor)
              ,MoveRight (notendicke`div` 3+poff),PushPos,PutStaccatissimoo,PopPos,PopPos])
        poff    = poffset p


tenutou :: GenElem -> GenElem
tenutou alts = st alts
 where
  st alts s = (alt {parts  = partsneu},s1)
   where
    (alt,s1)= alts s
    ref    = ref_hoehe alt
    tie    = tiefe alt
    kor |mod ref  2 == 0 = halbtonab*3|otherwise = (halbtonab*2)
    partsalt  = parts alt
    partsneu  = changeHead pneu partsalt
    pneu p   = p {pvorsatznotation = notan+++pvorsatznotation p}
      where              
       notan = onotation
                  [PushPos,MoveDown ((tie*halbtonab)+kor)
                  ,MoveRight (notendicke`div` 3+poff),PushPos,PutTenuto,PopPos,PopPos]
       poff    = poffset p

tenutoo :: GenElem ->GenElem
tenutoo alts = st alts
 where
  st alts s = (alt {parts  = partsneu},s1)
   where
    (alt,s1)= alts s
    ref    = ref_hoehe alt
    kor |ref< -6 = halbtonab*2
        |mod ref  2 == 0 = halbtonab+ (halbtonab `div` 4)
        |otherwise = halbtonab*2
    partsalt  = parts alt
    partsneu  = changeHead pneu partsalt
    pneu p  = p {pvorsatznotation = notan+++pvorsatznotation p}
      where
        notan  =   onotation (   [PushPos,MoveDown (((ref-4)*halbtonab)+kor)
              ,MoveRight (notendicke`div` 3+poff),PushPos,PutTenuto,PopPos,PopPos])
        poff    = poffset p



marcatou :: GenElem -> GenElem
marcatou alts = st alts
 where
  st alts s = (alt {parts  = partsneu},s1)
   where
    (alt,s1)= alts s
    ref    = ref_hoehe alt
    tie    = tiefe alt
    kor |mod ref  2 == 0 = halbtonab*3|otherwise = (halbtonab*2)
    partsalt  = parts alt
    partsneu  = changeHead pneu partsalt
    pneu p   = p {pvorsatznotation = notan+++pvorsatznotation p}
      where              
       notan = onotation
                  [PushPos,MoveDown ((tie*halbtonab)+kor)
                  ,MoveRight (notendicke`div` 3+poff),PushPos,PutMarcatou,PopPos,PopPos]
       poff    = poffset p

marcatoo :: GenElem ->GenElem
marcatoo alts = st alts
 where
  st alts s = (alt {parts  = partsneu},s1)
   where
    (alt,s1)= alts s
    ref    = ref_hoehe alt
    kor |ref< -6 = halbtonab*2
        |mod ref  2 == 0 = halbtonab+ (halbtonab `div` 4)
        |otherwise = halbtonab*2
    partsalt  = parts alt
    partsneu  = changeHead pneu partsalt
    pneu p  = p {pvorsatznotation = notan+++pvorsatznotation p}
      where
        notan  =   onotation (   [PushPos,MoveDown (((ref-4)*halbtonab)+kor)
              ,MoveRight (notendicke`div` 3+poff),PushPos,PutMarcatoo,PopPos,PopPos])
        poff    = poffset p


sforzatou :: GenElem -> GenElem
sforzatou alts = st alts
 where
  st alts s = (alt {parts  = partsneu},s1)
   where
    (alt,s1)= alts s
    ref    = ref_hoehe alt
    tie    = tiefe alt
    kor |mod ref  2 == 0 = halbtonab*3|otherwise = (halbtonab*2)
    partsalt  = parts alt
    partsneu  = changeHead pneu partsalt
    pneu p   = p {pvorsatznotation = notan+++pvorsatznotation p}
      where              
       notan = onotation
                  [PushPos,MoveDown ((tie*halbtonab)+kor)
                  ,MoveRight (notendicke`div` 3+poff),PushPos,PutSforzato,PopPos,PopPos]
       poff    = poffset p

sforzatoo :: GenElem ->GenElem
sforzatoo alts = st alts
 where
  st alts s = (alt {parts  = partsneu},s1)
   where
    (alt,s1)= alts s
    ref    = ref_hoehe alt
    kor |ref< -6 = halbtonab*2
        |mod ref  2 == 0 = halbtonab+ (halbtonab `div` 4)
        |otherwise = halbtonab*2
    partsalt  = parts alt
    partsneu  = changeHead pneu partsalt
    pneu p  = p {pvorsatznotation = notan+++pvorsatznotation p}
      where
        notan  =   onotation (   [PushPos,MoveDown (((ref-4)*halbtonab)+kor)
              ,MoveRight (notendicke`div` 3+poff),PushPos,PutSforzato,PopPos,PopPos])
        poff    = poffset p


akzu :: GenElem -> GenElem
akzu alts = st alts
 where
  st alts s = (alt {parts  = partsneu},s1)
   where
    (alt,s1)= alts s
    ref    = ref_hoehe alt
    tie    = tiefe alt
    kor |mod ref  2 == 0 = halbtonab*1|otherwise = 0
    partsalt  = parts alt
    partsneu  = changeHead pneu partsalt
    pneu  p  = p -- {  pvorsatzdvi = dvin+++ pvorsatzdvi p}
      where
        poff    = poffset p

akzo :: GenElem ->GenElem
akzo alts = st alts
 where
  st alts s = (alt {   parts  = partsneu},s1)
   where
    (alt,s1)= alts s
    ref    = ref_hoehe alt
    kor | mod ref 2 == 0 = halbtonab*3|otherwise = (halbtonab*2)
    partsalt = parts alt
    partsneu  = changeHead pneu partsalt
    pneu p   = p -- {pvorsatzdvi = dvin+++pvorsatzdvi p}
     where
      poff    = poffset p
--      dvin  = odvi   (D   [Push,Y3 (((ref-4)*halbtonab)+kor)
--              ,Right4 ((div notendicke 3)+poff)])+++
--          akzento+++odvi (D [Pop])

fermateo :: GenElem ->GenElem
fermateo alts = st alts
 where
  st alts s = (alt {parts  = partsneu},s1)
   where
    (alt,s1)= alts s
    ref    = min (-8)$ ref_hoehe alt
    kor |mod ref  2 == 0 = halbtonab*3|otherwise = (halbtonab*2)
    partsalt  = parts alt
    partsneu  = changeHead pneu partsalt
    pneu p   = p {pvorsatznotation = notan+++pvorsatznotation p}
      where
       notan  = onotation  
                [PushPos,MoveDown (((ref-4)*halbtonab)+kor)
                ,MoveRight (poff+div notendicke 2),PutFermateo,PopPos]
       poff    = poffset p
 
fermateu :: GenElem -> GenElem
fermateu alts = st alts
 where
  st alts s = (alt {parts  = partsneu},s1)
   where
    (alt,s1)= alts s
    ref    = ref_hoehe alt
    tie    = tiefe alt
    kor |mod ref  2 == 0 = halbtonab*1|otherwise = 0
    partsalt  = parts alt
    partsneu  = changeHead pneu partsalt
    pneu p   = p {pvorsatznotation = notan+++pvorsatznotation p}
     where
       notan 
        = onotation  [PushPos,MoveDown ((tie*halbtonab)+kor+2* halbtonab )
              ,MoveRight (poff+(div notendicke 2)),PutFermateu,PopPos]

       poff    = poffset p

tro :: GenElem ->GenElem
tro alts = st alts
 where
  st alts s = (alt{parts  = partsneu},s1)
   where
    (alt,s1)= alts s
    ref    = min (-8) $ref_hoehe alt
    kor |mod ref  2 == 0 = halbtonab*3|otherwise = (halbtonab*2)
    partsalt  = parts alt
    partsneu  = changeHead pneu partsalt
    pneu p   = p -- { pvorsatzdvi = dvin+++pvorsatzdvi p}
     where
--      dvin  = odvi   (D   [Push,Y3 (((ref-4)*halbtonab)+kor)
--              ,Right4 (poff)])+++
--          triller+++odvi (D [Pop])
      poff    = poffset p

  
tru :: GenElem -> GenElem
tru alts = st alts
 where
  st alts s = (alt {parts  = partsneu},s1)
   where
    (alt,s1)= alts s
    ref    = ref_hoehe alt
    tie    = tiefe alt
    kor |mod ref  2 == 0 = halbtonab*1|otherwise = 0
    partsalt  = parts alt
    partsneu  = changeHead pneu partsalt
    pneu p   = p -- {pvorsatzdvi = dvin+++pvorsatzdvi p}
     where
--      dvin  = odvi (D   [Push,Y3 ((tie*halbtonab)+kor)
--              ,Right4 (poff)])
--          +++triller+++odvi (D [Pop])
      poff    = poffset p

endstrich :: GenElem
endstrich 
 =result initdvielem { 
           maxbreite = breite,
           minbreite = breite,
--           genOdvi = dvin,
           parts    = Term pa}
   where
    breite  = 3*dickstrich
    pa =initpart
        {poffset    = 0
        ,pwoliegt  = \z n -> 0
        ,ptiefe    = 4
        ,phoehe    = 4
  --      ,pvorsatzdvi  = odvi (D [])
        ,pschlag    = (1,2)
        ,pvorsatzart  = []
  --      ,pdvi = dvin
        ,ppostdefs  = []}


addTakt dvielem = \s -> (dvielem,s{taktNummer= taktNummer s+1})

breittaktstrich :: GenElem
breittaktstrich
    = addTakt initdvielem { 
        maxbreite = breite,
         minbreite = breite --,
         -- genOdvi = dvin
       }
   where
     breite  = 2*dickstrich


endwiederholung :: GenElem
endwiederholung 
 =result initdvielem { 
           maxbreite = breite,
           minbreite = breite,
--           genOdvi = dvin,
           parts    = Term pa}
   where
--     dvin z n= odvi (D [Set_char 126,Set_rule takthoehe strichdicke,
--               Right3 dickstrich,Set_rule takthoehe dickstrich])
     breite  = 2*dickstrich+strichdicke+doppelpunktbreite
     pa =initpart
        {poffset    = 0
        ,pwoliegt  = \z n -> 0
        ,ptiefe    = 4
        ,phoehe    = 4
  --      ,pvorsatzdvi  = odvi (D [])
        ,pschlag    = (1,4)
        ,pvorsatzart  = []
  --      ,pdvi = dvin
        ,ppostdefs  = []}

endwiederholungmitte :: GenElem
endwiederholungmitte 
    = addTakt initdvielem { 
        maxbreite = breite,
         minbreite = breite--,
     --      genOdvi = dvin
      }
   where
 --   dvin= quantnach 
 --          (odvi (D [Set_char 126,Set_rule takthoehe dickstrich])) breite breite
    breite  = dickstrich+doppelpunktbreite

anfwiederholungmitte :: GenElem
anfwiederholungmitte 
    = addTakt initdvielem { 
        maxbreite = breite,
         minbreite = breite --,
         -- genOdvi = dvin
         }
   where
--    dvin= quantnach 
--           (odvi (D [Set_rule takthoehe dickstrich,Set_char 126])) breite breite
    breite  = dickstrich+doppelpunktbreite

anfendwiederholung :: GenElem
anfendwiederholung 
 = addTakt initdvielem {
        minzw    = taktmin,
        maxzw    = taktmax,
        maxbreite   = breite+taktmax,
        minbreite   = breite+taktmin --,
        -- genOdvi = dvin
        }
   where
--    dvin z n= quantnach
--          (odvi (D [Set_char 126,Set_rule takthoehe dickstrich,
--               Right3 dickstrich,Set_rule takthoehe dickstrich,Set_char 126]))
 --         (taktmin+breite) (taktmax+breite) z n
    breite  = 3*dickstrich+2*doppelpunktbreite


dicktaktstrich :: GenElem
dicktaktstrich = taktstrichmdicke dickstrich

left :: Int -> GenElem
left i = result initdvielem { 
        minzw    = 0,
        maxzw    = 0,
        maxbreite = i,
        minbreite = i --,
        --genOdvi = dvin
        }
--   where
--     dvin z n = quantmit (odvi (D [])) i i z n


schiebe :: Int -> GenElem -> GenElem
schiebe i alts  = st i alts
 where
  st i alts s = (alt {maxbreite   = i+maxalt,
             minbreite   = i+minalt,
            offset     = i+offalt,
  --          genOdvi      = quantmit (odvi (D []))(i+minalt) (i+maxalt),
            parts  = partsneu},s1)
   where
    offalt = offset alt
    minalt = minbreite alt
    maxalt = maxbreite alt 
    (alt,s1)= alts s
   -- dvin  = odvi (D   [X3 i])
    partsalt  = parts alt
    partsneu1  = changeTail (verschiebe (\z n ->i)) partsalt
    partsneu   = changeHead pneu partsneu1
    pneu p = p { -- pvorsatzdvi = dvin+++pvorsatzdvi p,
              poffset = poffset p+i} 


taktstrichmdicke dick
 = result initdvielem {
      minzw  = taktmin,
      maxzw  = taktmax,
      maxbreite  = mab,
      minbreite  = mib,
--      genOdvi = quantmit (odvi (D [])) mib mab,
      parts    = Term pa}
 where
  pa  = initpart 
    {poffset    = 0,
    pwoliegt  = \z n -> 0,
    ptiefe    = 4,
    phoehe    = 4,
 --   pvorsatzdvi  = odvi (D []),
 --   pdvi    = (quantmit (odvi  (D [Put_rule takthoehe dick])) taktmin taktmax),
    pnotation = quantmitnotation (onotation  [MoveDown (-takthoehe),PutLine dick (0,takthoehe)]) taktmin taktmax,
    pschlag    = (1,1024),
    pvorsatzart  = [],
    ppostdefs  = []}
  breite = dick
  mab = breite +taktmax
  mib = breite +taktmin

anfwiederholung :: GenElem
anfwiederholung 
 = result initdvielem { 
        minzw    = taktmin,
        maxzw    = taktmax,
        maxbreite   = breite+taktmax,
         minbreite   = breite+taktmin--,
        -- genOdvi = dvin
        }
   where
    breite  = 2*dickstrich+strichdicke+doppelpunktbreite

printTaktNumber ::  GenElem -> GenElem
printTaktNumber  altsOrg s
  |mod(taktNummer s)  5 == 0 && taktNummer s > 0 
     = if(not((theStimme s)==0))
           then (textmfnto fntNormal (show$ taktNummer s) altsOrg) s
           else (textmfntostaff (systemanzahl s) fntNormal (show$ taktNummer s) altsOrg) s
  |otherwise = altsOrg s


doppelstrich:: GenElem
doppelstrich  = printTaktNumber (addTakt initdvielem { 
        minzw    = taktmin,
        maxzw    = taktmax,
        maxbreite   = breite+taktmax,
        minbreite   = breite+taktmin,
       -- genOdvi = quantmit (odvi (D [])) (breite+taktmin)(breite+taktmax),
        parts    = Term pa})
 where
  pa  = initpart
      {poffset    = 0,
      pwoliegt  = \z n -> 0,
      ptiefe    = 4,
      phoehe    = 4,
    --  pvorsatzdvi  = odvi (D []),
      pschlag    = (1,2),
    --   pdvi = \z n -> quantnach
    --       (odvi (D[Set_rule takthoehe strichdicke,Right3 dickstrich
    --           ,Set_rule takthoehe strichdicke]))
    --       (taktmin+breite) (taktmax+breite) z n,
      ppostdefs   = []}
  breite  = dickstrich+2*strichdicke

selcreschr weite = min 3 (abs (div weite mincres))

cres fnr tnr alts = cresaux 0 fnr tnr alts 20
decr fnr tnr alts = cresaux 0 fnr tnr alts 16
crest kor fnr tnr alts = cresaux kor fnr tnr alts 20
decrt kor fnr tnr alts = cresaux kor fnr tnr alts 16



crescendo = crescendoaux crescendoSymbolStart
decrescendo = crescendoaux decrescendoSymbolStart

crescendoaux kleinster alts s
  |not (success sffrompart && success sftopart) = (alt,s1)
  |otherwise  = (alt {parts = partsneu},s1)
   where
     (alt,s1)  = alts s
     partsalt  = parts  alt
     pneu p      = p { ppostdefs=(constr (tiefe2+4) 1 (flength partsalt)  :ppostdefs p)}
     
     constr |decrescendoSymbolStart==kleinster = Decrescat|otherwise = Crescat 
     partsneu    = changeHead pneu partsalt
     sffrompart  = sel 1 partsalt
     sftopart    = lastInCluster partsalt
     tiefe2  = tiefe alt


glissando alts s 
  |not (success sffrompart && success sftopart) = (alt,s1)
  |otherwise  = (alt {parts = partsneu},s1)
   where
     (alt,s1)  = alts s
     partsalt  = parts  alt
     pneu p      = p { ppostdefs=(Glissando 1 (flength partsalt)  :ppostdefs p)}     
     partsneu    = changeHead pneu partsalt
     sffrompart  = sel 1 partsalt
     sftopart    = lastInCluster partsalt
     tiefe2  = tiefe alt

triller alts s 
  |not (success sffrompart && success sftopart) = (alt,s1)
  |otherwise  = (alt {parts = partsneu},s1)
   where
     (alt,s1)  = alts s
     partsalt  = parts  alt
     pneu p      = p { ppostdefs=(Triller 1 (flength partsalt)  :ppostdefs p)}     
     partsneu    = changeHead pneu partsalt
     sffrompart  = sel 1 partsalt
     sftopart    = lastInCluster partsalt
     tiefe2  = tiefe alt


cresaux  korrektur fnr tnr alts kleinster s
  |not (success sffrompart && success sftopart) = (alt,s1)
  |otherwise  = (alt {parts = partsneu},s1)
   where
     (alt,s1)  = alts s
     partsalt  =parts  alt
     pneu p = p {ppostdefs=(Crescat (tiefe2+4) fnr tnr :ppostdefs p)}
     partsneu  = changeHead pneu partsalt
     sffrompart  = (sel fnr partsalt)
     sftopart  = (sel tnr partsalt)
     tiefe2  = tiefe alt

takt :: GenElem
takt = taktstrichmdicke strichdicke



startLower anno elem s
 = ( alt{parts=changeHead (doStart anno) (parts alt)}
   ,s1)
 where
   (alt,s1)  = elem s
   doStart name x= x{startLowerAnno=Just name} 

startUpper anno elem s 
 = (alt{parts=changeHead (doStart anno) (parts alt)}
   ,s1)
 where
   (alt,s1)  = elem s
   doStart name x= x{startUpperAnno=Just name} 

endLowerAnnotation elem s
  = (alt{parts=changeHead (doEnd) (parts alt)},s1) 
 where
   (alt,s1)  = elem s
--   Just (name,start)=openLower s1
   doEnd x= x{endLower=True} 

endUpperAnnotation elem s
  = (alt{parts=changeHead (doend) (parts alt)}
    ,s1)
 where
   (alt,s1)  = elem s
--   (name,start)=maybe ("",\z n -> 0) id ( openUpper s1)
   doend x= x{endUpper=True} 







arpeggio  alts s 
 |isNull (parts alt) = (alt,s1)
 |otherwise
   = ( alt 
       { maxbreite = notendicke+maxbreite alt 
       , minbreite = notendicke+minbreite alt
       , parts     = clus2}
     , s1)
  where
   (alt,s1) = alts s
   ref      = ref_hoehe alt
   clus     = parts alt
   clus1    = changeTail (verschiebe (\z n->notendicke)) clus
   clus2    = changeHead makeVorsatz clus1
   makeVorsatz p 
    =       (p 
      { poffset     = notendicke+poff
      , pvorsatznotation
         = onotation
              ([PushPos]
             ++concat [[PushPos,MoveDown (i*halbtonab),PutArpeggio,PopPos]
                      |i<-[phoehe p,phoehe p+2..ptiefe p]]
             ++[PopPos,MoveRight notendicke])
            +++pvorsatznotation p
      })
      where
        poff     = poffset p









{----------------------------------------
------------------TESTING
----------------------------------------



takt1 =  (is (n4c0))--es (n32e2)--n32 'f' 2--n16 'g' 2--p8--balk4--balk1
takt2 =  (is (n4c0))--p4--n8 'g' 3--n8 'g' 2--p4--balk3
takt3 =  ohnzw (na (n4c0))--p2--n16 'g' 2--p8--n16c1
takt4 =  (is (n4c0)) --es (n32e1)--n64 'f' 1--n64 'f' 1--p16--p8--n8 'g' 1--n8 'f' 1
takt5 =  (akkord1)--akkord2--akkord1--(akkord2)--balk2
takt6 =  f16(hals (na (k16e2)))--f16 (hals (is (k16e2)))
    --f8o (halso (akko [k8c2,k8e2,k8g2]))
    --akkord3--akkord1--balk1
takt7 =  n2 'h' 2--n2 'h'3--balk5
 
akkord1 = halso akkord3
akkord2 = halsu (akku [is(k4c2),(k4 'f' 2),(k4e2),na(k4 'g' 2),k16d2])
akkord3 = akko [is(k16c2),is(k16f2),es(k16e2),na(k16g2),k16d2]
balk1  = balkeno [akkord3,(k32e1),(k16g1),akkord3,(k32e1),(k32 'f' 1),(k32e1),(k32 'f' 1)]
balk2  = balkeno [(k8c1),(k8 'h' 1)]
balk3  = balkeno [(k16f1),(k16h1)]
balk4  = balkeno [(k16h1),(k16f1)]
balk5  = balkeno [es(k8 'h' 1),(k8c2),(k64 'd' 2),(k8e2),is(k8f2),na(k16g2)]

takte =  [takt1,n1 'a' 2,takt2,takt1,takt6,takt3,takt6,takt5,takt3,takt4,p1,takt6,
    takt2,takt2,takt7,takt5,takt4,takt7]









--}

     
c = 'c'    
d = 'd'     
e = 'e'    
f = 'f'     
g = 'g'     
a = 'a'     
h = 'h'     
b = 'h'     





