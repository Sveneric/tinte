module Tinte.Pausen where

import Data.Ratio
import Tinte.Data
import Tinte.Constants
import Tinte.Util
import Tinte.Macros
import Tinte.Hals
import Tinte.Noten
import Tinte.Balken
import Tinte.AltSeq
import Tinte.Fahnen
import Tinte.Notation

--import Dvi.Dvi


{--
p1:: GenElem
p1  = p  
 where
  p s = (center sp1) s
   where
    sp1 = \_->(
      initdvielem 
      { maxbreite  = mp1breite  
      , minbreite  = mp1breite
      , hoehe      = -4      
      , tiefe      = -4        
      , genOdvi        = quantmit (odvi (D [])) mp1breite mp1breite
      , parts      = Term pa
      , ref_hoehe  = -4},s)
    mass n |theGrace s = (n*13)`div` 20|otherwise = n
    mp1breite  =  mass p1breite
    pa  = initpart
          {  poffset    = 0
          , pwoliegt  = \z n -> 0
          , ptiefe    = -4
          , phoehe    = -4
          , pvorsatzdvi  = odvi (D [])
          , pdvi    = quantmit (odvi (D [Put1 60])) mp1breite mp1breite
          , pschlag    = (1,1)
          , pvorsatzart  = []
          , ppostdefs  = []
          , def = P1}
--}


taktepause:: Int -> GenElem
taktepause z = p  
 where
  p s = (center sp1) s
   where
    sp1 = \_->(
      initdvielem {
      maxbreite  = 6*mp1breite,  
      minbreite  = 6*mp1breite,
      hoehe      = -4,      
      tiefe      = -4,        
--      genOdvi        = quantmit (odvi (D [])) (6*mp1breite) (6*mp1breite),
      parts    = Term pa,
      ref_hoehe  = -4},s{taktNummer=taktNummer s+z-1})
    mass n |theGrace s = (n*13)`div` 20|otherwise = n
    mp1breite  =  mass p1breite
    pa  = initpart{ poffset    = 0,
      pwoliegt  = \z n -> 0,
      ptiefe    = -4,
      phoehe    = -4,
      pschlag    = (1,1),
      pvorsatzart  = [],
      ppostdefs  = []
      ,def=Pause (1%1)}
--  setzch = set z

  zoff |z<10  = 3*p1breite-notendicke `div`  2 
       |otherwise = 3*p1breite-notendicke



p1o:: GenElem
p1o = lift (-6) p1
 
p1u:: GenElem
p1u = lift 6 p1


{--
    
p2::GenElem
p2  = p 
 where
  p s = (center sp2) s
   where
    sp2 = result
      initdvielem {
       maxbreite  = mp2breite,  
       minbreite  = mp2breite,
       hoehe      = -4,      
       tiefe      = -4,        
       genOdvi        = \z n ->  odvi (D [X3 mp2breite]) ,
   --     def        = P2,
       ref_hoehe  = -4,
       parts    = Term pa}
      -- rechts = nachrechts ((mp2min-mp2breite)`div` 2) ((mp2max-mp2breite)`div` 2)
    mass n |theGrace s = (n*13)`div` 20|otherwise = n
    mp2breite  =  mass p2breite
    mp2min    = mass p2min
    mp2max    = mass p2max
    ddvi  = quantmit (odvi (D ([Push,Set_char 61,Pop]))) mp2min mp2max
    pa    = mkpart P2 (-4) (1,2)

--}


p2o:: GenElem
p2o = lift (-6) p2
 
p2u:: GenElem
p2u = lift 6 p2
    
p1pur ::GenElem
p1pur  = \s -> p p1breite p1breite p1breite schlag 60 (Pause (1%1)) s
   where 
  schlag  = (1,1)

p2pur ::GenElem
p2pur  = \s -> p p2breite p2breite p2breite schlag 61 (Pause (1%2)) s
   where 
  schlag  = (1,2)


p1::GenElem
p1  = \s -> p p1breite p1min p1max schlag 60 (Pause (1%1)) s
   where 
  schlag  = (1,1)

p2::GenElem
p2  = \s -> p p2breite p2min p2max schlag 60 (Pause (1%2)) s
   where 
  schlag  = (1,2)

p4::GenElem
p4  = \s -> p p4breite p4min p4max schlag 62 (Pause (1%4)) s
   where 
  schlag  = (1,4)


p breite min max schlag char d s
 = (initdvielem 
       {maxbreite   = mmax,
        minbreite   = mmin,
        maxzw       = (mmax-mbreite),
        minzw       = (mmin-mbreite),
        zwischenr   = quant (mmin-mbreite) (mmax-mbreite),
        hoehe       = note,
        tiefe       = note,
        offset      = 0,
--        genOdvi      = \z n ->quantmit (odvi (D [])) mmin mmax z n,
--        vorsatzdvi  = odvi (D []),
        schlaege  = [schlag],
        -- def      = d,
        ref_hoehe  = note,
        parts    = Term pa},s)
   where 
    notaP=schlag2rest schlag
    note = -4
 --   ddvi  = quantmit (odvi (D ([Push,Set_char char,Pop]))) mmin mmax
    dnota  = quantmitnotation (onotation [PushPos,MoveDown (-4*halbtonab) ,notaP,PopPos]) mmin mmax
    pa    = (mkpart d note schlag  mmax mmin){pneedsStem=False}{pnotation=dnota}
    mass n |theGrace s = (n*13)`div` 20|otherwise = n
    mbreite  = mass breite
    mmin    = mass min
    mmax    = mass max


p4o:: GenElem
p4o = lift (-6) p4
 
p4u:: GenElem
p4u = lift 10 p4

p8::GenElem
p8 = \s -> p p8breite p8min p8max schlag 63 (Pause (1%8)) s
   where 
  schlag  = (1,8)

p8o:: GenElem
p8o = lift (-6) p8
 
p8u:: GenElem
p8u = lift 8 p8


p16::GenElem    
p16 = \s -> p p16breite p16min p16max schlag 64 (Pause (1%16)) s
   where 
  schlag  = (1,16)


p16o:: GenElem
p16o = lift (-6) p16
 
p16u:: GenElem
p16u = lift 8 p16

     
p32::GenElem    
p32 = \s -> p p32breite p32min p32max schlag 65 (Pause (1%32)) s
   where 
  schlag  = (1,32)


p32o:: GenElem
p32o = lift (-6) p32

p32u:: GenElem
p32u = lift 8 p32


p64::GenElem    
p64  =  \s -> p p64breite p64min p64max schlag 66 (Pause (1%64)) s
   where 
  schlag  = (1,64)

p64o:: GenElem
p64o = lift (-6) p64
 
p64u:: GenElem
p64u = lift 6 p64
     


