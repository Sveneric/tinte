module Tinte.Tinte where
import System.IO
import System.IO.Error
import GHC.IO.Handle
import Debug.Trace

import Tinte.Constants
import Tinte.Macros
import Tinte.Data
import Tinte.Format
import Tinte.Notation
import Tinte.StartPS

type Stueck = ([(Global,[GenElem])])
type Heft   = [Stueck]

cahier :: Int ->  Heft -> Seiten
cahier _ [] = []
cahier i (([]):sts) = cahier i sts
cahier i ((((g,f):fs)):sts) = ((([]):erg):seiten)++cahier l sts
 where
  l = length seiten + i+1
  (erg:seiten) = if (null  e) then ([]:[])  else  e
    where 
      e :: Seiten
      e = tinteRenderPS langl ((g {seitennr = i},f):fs)

tintenotation:: String -> Global->Seiten-> String ->IO() 
tintenotation dateiname glob seiten kopfdps
 =do   
   let nota =  (map (concat) seiten)
   writeFile  dateiname startPS
   appendFile dateiname kopfdps
   if (snippet glob)
     then appendFile dateiname (generatePSSnippet$head nota)
     else appendFile dateiname (generatePSFirstPage$head nota)
   let pgs =map (generatePSFurther(length nota)) (zip [2,3..] (tail nota))
   sequence_ [do 
                hPutStr stdout ("["++show n++"]")
                appendFile dateiname pg
             |(n,pg)<-zip [2,3..] pgs]


writeswithnotice _ [] w = return w
writeswithnotice n [x] w = do 
  w<-writewithzeilennotice 0 x w 
  writeswithnotice (n+1) [] w
writeswithnotice 0 (x:xs) w = do
  w<-writewithzeilennotice 0 x w 
  writeswithnotice 1 xs w 
writeswithnotice n (x:xs) w 
 = do 
    hPutStr stdout ("\n Seite: " ++(show n) ++ " Zeilen: " ) 
    w<-writewithzeilennotice 0 x w 
    writeswithnotice (n+1) xs w

writewithzeilennotice n [] w  = do
  hPutStr stdout "\n" 
  return w
writewithzeilennotice 0 (x:xs) w = do 
  hPutStr w x
  writewithzeilennotice 1 xs w
writewithzeilennotice n (x:xs) w = do  
  hPutStr stdout ("["++show n++"]") 
  hPutStr w x
  writewithzeilennotice (n+1) xs w





