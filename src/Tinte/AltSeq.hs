module Tinte.AltSeq where
import Data.List

import Debug.Trace
import Tinte.Data
import Tinte.Constants
import Tinte.Util
import Tinte.Macros
import Tinte.Lib
import Tinte.Rat

--import Dvi.Dvi

infixl 1 -/
infixl 0 |/ 

(-/) :: GenElem -> GenElem -> GenElem
(-/) e1 e2 s = (bi e11 e21,s2)
  where
    (e11,s1) = e1 s
    (e21,s2) = e2 s1

(|/) :: GenElem -> GenElem -> GenElem
(|/) e1 e2 s 
  = -- trace ("\n"++(show e11)++"\n"++(show e21)) (trace ("erg"++(show
                 --   erg))
                       (erg,s2)
    where
    (e11,s1) = e1 s
    (e21,s2) = e2 s1
    erg = alt e11 e21

-- //zum Bilden der Seqeuenz zweier Elemente
-- (bi) infixl 1 :: DVIelem DVIelem -> DVIelem
bi e1 e2 
 = e1 
   { maxbreite = maxbe1+maxbe2
   , minbreite = minbe1+minbe2
   , maxzw  = maxzw2
   , minzw  = minzw2
   , zwischenr = zwischenr2
   , hoehe     = min hoehee1 hoehee2
   , tiefe     = max tiefee1 tiefee2
  -- , genOdvi   = quantmit (odvi (D [])) (minbe1+minbe2) (maxbe1+ maxbe2)
   , schlaege  = schlaegee1 ++ schlaegee2
   , parts     = parts1++++parts2verschoben
   } 
  where
    maxbe1 = maxbreite e1- offset e1
    maxbe2 = maxbreite e2
    minbe1 =  minbreite e1-offset e1
    minbe2 = minbreite e2
    maxzw2 = maxzw e2
    minzw2 = minzw e2
    hoehee1 = hoehe e1
    hoehee2 = hoehe e2
    tiefee1 = tiefe e1
    tiefee2 = tiefe e2
 --   dvie1 = genOdvi e1
 --   dvie2 = genOdvi e2
 --   vordvi2 = vorsatzdvi e2
    zwischenr2 = zwischenr e2
 --   dvi2mitvorschlag z n = vordvi2+++dvi2
 --     where
 --      dvi2 = dvie2 z n
    schlaegee1 = schlaege e1
    schlaegee2 = schlaege e2
    parts1  = parts e1
    parts2  = parts e2
    parts2verschoben = fmap (verschiebe (quant minbe1 maxbe1)) parts2

{--
The calculation of the alternatives we will proceed as follows:
First see what kinds of cluster we are trying to compose with
the alternatives\dots
--}


alt e1 e2
   =  e1 
     {
      maxbreite = --trace ("ma "++(show$ maxbreite e1)++" "++(show ma)
                    --   ++"mi "++(show$ minbreite e1)++" "++(show mi)) 
                   ma
    , minbreite = mi
    , 
        hoehe     = min hoehee1 hoehee2
     , tiefe     = max tiefee1 tiefee2
   --       , genOdvi   = quantmit (odvi (D [])) mi ma 
     , 
        parts     =  newParts
     }                      
  where
    hoehee1 = hoehe e1
    hoehee2 = hoehe e2
    tiefee1 = tiefe e1
    tiefee2 = tiefe e2
    parts1  = parts e1
    parts2  = parts e2
    (newParts,mi,ma) = altparts parts1 parts2

{--
\paragraph{Alternatives}
The calculation of the alternatives for the parts we will proceed as follows:
First see what kinds of cluster we are trying to compose as alternatives.
This will be transformed to a list of sequences, which are to be 
interpreted als alternatives. This list ist then passed to the main
processing function for the alternatives:
--} 


altparts ps1 ps2 = altTheseParts ((toSeqList ps1)++ (toSeqList ps2))

toSeqList (Alt xs) = xs
toSeqList ps@(Seq _) = [ps]
toSeqList x = [Seq [x]]

altTheseParts xs = (Alt respectedBeat,mi,ma)
  where
    withBeat  = fst$beatMeAlt (0,1) xs 
    (respectedBeat,mi,ma) = respectBeat withBeat

beatMeAlt:: Rat -> [Cluster] -> ([GenCluster (Part,Rat,Int)], Rat)
beatMeAlt t [] = ([],t)
beatMeAlt t xs = (map fst alts,minrat$map snd alts)
   where
     alts = map (beatMe t) (zip [1,2..] xs)

beatMeSeq t i [] = ([],t)
beatMeSeq t i (x:xs) 
 = ((p1:ps),ts)
  where
     (p1,t1) =beatMe t (i, x)
     (ps,ts) =beatMeSeq t1 i  xs

beatMe :: Rat -> (Int,Cluster) -> (GenCluster (Part, Rat,Int), Rat)
beatMe t (i,(Term p)) 
 = -- trace (" a term of beat"++show (pschlag p)++show (add t $pschlag p)) $
                   (Term (p,t,i),add t $pschlag p)
beatMe t (i,(Seq ys)) = (Seq seq,tend)
 where
  (seq,tend) = beatMeSeq t i ys
beatMe t (i,(Alt ys)) = (Alt seq,tend)
 where
  (seq,tend) = beatMeAlt t ys
beatMe t (i,Null) = (Null,t)

--respectBeat :: [(GenCluster (Part,Rat,Int))] -> ([GenCluster Part], Int, Int)
respectBeat xs = (changeForThis maxis xs,mi,ma)
  where
    concated = concat $ map flattenCluster xs
    clustered = clusterBy (\(_,x,_) (_,y,_)-> eq x y) 
       --   $trace (show $map (\(s,u,v)->(pschlag s ,u)) concated)
               concated    
    (maxis,mi,ma) = 
     maxima
     --  $ trace ((show$length clustered)++show (map (map snd) clustered) ) 
                        clustered

--maxima :: [[(Part,Rat,Int)]] -> ([(Int -> Int -> Int, Rat,Int)], Int, Int)
maxima clustered
 =    -- trace    ("e4"++(show$ (\(xs,off,mo)->(map (\(wo,sl,s)->(wo 1 1,sl)) xs,off,mo)) e4))  
     e4 
 where 
  e0=map (\cl->(cl,getmaxOffset cl)) 
--            $ trace (show (map (map (\(u,v,_)->(pschlag u,v))) clustered))
             clustered  
  e1=map (\(cl,mo)->(maxiBy (\(x,v,i) (y,_,_)->(m1 x y,v,i)) cl,mo)) 
  --            $ trace (show (map (map (\(u,v,_)->(pschlag u,v))) clustered))
            $repairOffset  e0
  e2=sortBy (\((_,x,_),_) ((_,y,_),_) -> ratOrder x y)  
    --          $ trace (show (map (\((x,y,_),v)->( (pschlag x ,y),v))  e1)) 
             e1
  e3=map (\((p,s,_),mo)->(pwoliegt p,pschlag p,s,poffset p,mo))  e2
  e4= konsistentVerschieben  
            --   $   trace     ("e3: "++(show$map (\(wo,sl,s,off,mo)->(wo 1 1,sl,s,off,mo)) e3))  
             e3
  maxiBy max xs   = foldl1 max xs
  m x y 
    |pwoliegt x 1 1> pwoliegt y 1 1 = x
    |otherwise = y
  m1 x y 
    |less (pschlag x)  (pschlag y) = x
    |reduce(pschlag x) == reduce(pschlag y) = m x y
    |otherwise = y
{--  m2 x y 
    |less (pschlag x) (pschlag y) && pwoliegt x 1 1> pwoliegt y 1 1 = x  
    |less (pschlag y) (pschlag x) && pwoliegt y 1 1> pwoliegt x 1 1 = y  
    |less (pschlag y) (pschlag x) && pwoliegt y 1 1< pwoliegt x 1 1 
           =y{pwoliegt=pwoliegt x}
    |otherwise =x{pwoliegt=pwoliegt y}
--}

repairOffset xs 
 = 
     map (\cls ->(map (\(a,b,c,_)->(a,b,c)) cls,(\(_,_,_,x)->x)$head cls) )
    $clusterBy (\(_,x,_,_) (_,y,_,_)-> eq x y) 
    $neuenoffsetinstimme
    $sortBy (\(_,s1,_,_) (_,s2,_,_) ->ratOrder s1 s2)
    $wiederflach xs

wiederflach xs = concat$ map (\(trps,mo)-> map (\(x,v,i)->(x,v,i,mo)) trps) xs

neuenoffsetinstimme [] = []
neuenoffsetinstimme [x] = [x]
neuenoffsetinstimme (x@(p,s,i,mo):xs)
 |poffset p < mo =(x:(neuenoffsetinstimme$ verschieb i s (mo-poffset p) xs))
 |otherwise = (x:neuenoffsetinstimme xs)

verschieb i s off []=[]
verschieb i s off (x@(p,s1,i1,mo):xs)
--  |i==i1 && less s s1 
  --  = (p{pwoliegt= \ z n->off+ pwoliegt p z n},s1,i1,mo):(verschieb i s off xs)
  |otherwise = (x:(verschieb i s off xs))
  
getmaxOffset = foldl (\off (p,_,_)->max off (poffset p) ) (0::Int) 
    
{--konsistentVerschieben xs
 = (map (\(p1,ps1,s1,off,mo)->(p1,s1, mo)) xs 
        ,sum  (map (\(p1,ps1,s1,off,mo)->nextPosition p1 (ps1) 0 0 0 0) xs)
        ,sum  (map (\(p1,ps1,s1,off,mo)->nextPosition p1 (ps1) 0 0 1 1) xs)
      )
--}

konsistentVerschieben [] = ([],0,0)
konsistentVerschieben [(p1,ps1@(z,n),s1,off,mo)] 
   = ([(p1,s1, mo)], npo 0 0,npo 1 1)
  where
    npo = nextPosition p1 ps1 off mo
    (mi,ma) = abstandFuer2 z n
konsistentVerschieben ((p1,ps1,s1,off1,moff1):(p2,ps2,s2,off2,moff2):xs) 
 |neuePos > altePos = (((p1,s1,moff1):rec1),mi1,ma1)
-- |p1 1 1>= p2 1 1 = (((p1,s1,moff1):rec1),mi1,ma1)
 |otherwise         = (((p1,s1,moff1):rec2),mi2,ma2)
    where
      neuePos = npo1 1 1
      altePos = p2 1 1
      (rec1,mi1,ma1)
         = konsistentVerschieben ((npo1,ps2,s2,off2,moff2):xs)
      (rec2,mi2,ma2)
         = konsistentVerschieben ((p2 ,ps2,s2,off2,moff2):xs)
      (rec3,mi3,ma3)
         = konsistentVerschieben ((p2 ,ps2,s2,off2,moff2):xs)
      npo1 = nextPosition  p1 ps1 off1 moff1
     -- npo2 = nextPosition  p2 ps2 off2 moff2

nextPosition p (z,n) off mo x y  = --trace ("result: "++(show result)) 
                                     result
  where 
    result = mo+ p x y + (abstandFuer z n x y) 
    
changeForThis maxlist xs  = map (changeSingleCluster maxlist) xs
    
changeSingleCluster  maxls (Seq  xs)  = Seq (map (changeSingleCluster  maxls) xs)
changeSingleCluster  maxls (Alt  xs) = Alt (map (changeSingleCluster  maxls ) xs)
changeSingleCluster  maxls (Term a ) = Term (changePart maxls a)
changeSingleCluster  maxls (Modi balkeninfs  cluster)
 = Modi balkeninfs  (changeSingleCluster maxls cluster)   
changeSingleCluster  maxls Null = Null   

changePart []  (x,_,_) = x
changePart ((wo,r1,off):xs)  (p2,r2,i)
  |r1 == r2  = --trace ("setze wo liegt"++show (wo 1 1)) $
                setWoliegt wo off p2
  |otherwise = changePart xs (p2,r2,i)

{--
altpartsCluster :: [(GenCluster Part)]->[(GenCluster Part)] -> GenCluster Part
altpartsCluster [] ps2 = Seq ps2
altpartsCluster ps1 [] = Seq ps1
altpartsCluster ps1 ps2 
  = Alt [Seq [Seq ps|(ps,_)<-cluster],Seq[Seq ps|(_,ps)<-cluster]]
  where
    cluster = map verschiebeFuerAlt (makeCluster ps1 ps2)


makeCluster :: [Cluster] -> [Cluster] -> [([Cluster],[Cluster])]
makeCluster ps1 ps2
 |null r1 && null r2 = [clust1]
 |otherwise = (clust1:makeCluster r1 r2)
  where
   (clust1,(r1,r2)) = firstCluster ps1 ps2

firstCluster ::[Cluster] -> [Cluster] -> (([Cluster],[Cluster]),([Cluster],[Cluster]))
firstCluster ps1 ps2 = firstClusteraux nullRat nullRat ps1 ps2

firstClusteraux :: Rat ->Rat ->[Cluster]-> [Cluster]
  -> (([Cluster],[Cluster]),([Cluster],[Cluster]))
firstClusteraux akk1 akk2 (p1:ps1) (p2:ps2)
 |isNullRat akk1 = (((p1:firn1),(p2:firn2)),restn)
 |eq akk1 akk2 = (([],[]),((p1:ps1),(p2:ps2)))
 |less akk1 akk2 = (((p1:firl1),firl2),restl)
 |less akk2 akk1 = ((firg1,(p2:firg2)),restg)
  where
   ((firn1,firn2),restn)
      =firstClusteraux (pschlag(singleHead p1)) (pschlag (singleHead p2)) ps1 ps2
   ((firl1,firl2),restl) 
      =firstClusteraux (add akk1$pschlag(singleHead p1)) akk2 ps1 (p2:ps2)
   ((firg1,firg2),restg) 
      =firstClusteraux akk1 (add akk2 $pschlag(singleHead p2)) (p1:ps1) ps2
firstClusteraux _ _  ps1 ps2 = ((ps1,ps2),([],[]))

nullRat=(0,1)
isNullRat (0,1) = True
isNullRat _ = False

verschiebeFuerAlt ((x:xs),(y:ys)) 
  |(pwoliegt (singleHead x) 1 1) < (pwoliegt (singleHead y) 1 1)
    = (map (fmap (verschiebe (\z n -> (pwoliegt (singleHead y) z n)
                              -(pwoliegt (singleHead x) z n))))(x:xs),(y:ys))
  |(pwoliegt (singleHead x) 1 1) > (pwoliegt (singleHead y) 1 1)
    = ((x:xs),map (fmap (verschiebe (\z n -> (pwoliegt (singleHead x) z n)
                          -(pwoliegt (singleHead y) z n))))(y:ys))
  |otherwise =  ((x:xs),(y:ys))
verschiebeFuerAlt (x,y) = (x,y) --}


-- center ::GenElem -> GenElem
center alt 
 = links-/(ohnzw alt)-/rechts
  where
   rechts= nachrechts ((p1min+taktmin-p1breite)`div` 2) 
                      ((p1max+taktmax-p1breite)`div` 2) 
   links = nachrechts ((p1min-p1breite)`div` 2) ((p1max-p1breite)`div` 2) 


