module Tinte.Staff where

import Tinte.Data
import Tinte.Constants

staff i alts = staffaux i alts

staffaux i alts s
 = (alt  {parts  = partsneu},s1)
   where
    (alt,s1) = alts s
    partsneu = fmap partatstaff (parts alt)
    versatz = ((i-1)*(staffabstand))
    partatstaff p 
     =  p 
        { pnotation = proc2$pnotation p
        , pvorsatznotation =proc2 (\z n ->pvorsatznotation p) 1 1
        , postProcessNotation = proc2 . postProcessNotation p
        }
      where 

        proc2 znonota = \z n -> (onotation [MoveDown (-( versatz)::Int)])
                              +++znonota z n +++onotation [MoveDown versatz] 


