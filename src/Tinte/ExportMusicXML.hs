module Tinte.ExportMusicXML where

import Tinte.Data

toMusicXML _ [] = []
toMusicXML  i (([]):sts) = toMusicXML i sts
toMusicXML  i ((((g,f):fs)):sts) = ((([]):erg):seiten)++toMusicXML l sts
 where
  l = length seiten + i+1
  (erg:seiten) = if (null  e) then ([]:[])  else  e
    where 
      e = tinteToXML  ((g{seitennr = i},f):fs)

tinteToXML:: [(Global,[GenElem])] -> [[String]]
tinteToXML [] = [[[]]]
tinteToXML ((g1,elems):xs) = [elemsToXML g1 elems]

elemsToXML _ [] = []
elemsToXML g1 (e:es)
  = (show$parts e1):elemsToXML g2 es
  where
   (e1,g2) = e g1

bot = bot

writeTinteXML filename glob xml = do
 writeFile filename "<?xml version=\"1.0\"?>\n<!DOCTYPE score-partwise PUBLIC\
  \\n    \"-//Recordare//DTD MusicXML 3.1 Partwise//EN\"\
  \\n    \"http://www.musicxml.org/dtds/partwise.dtd\">\
  \\n<score-partwise version=\"3.1\">\
  \\n  <part-list>\
  \\n    <score-part id=\"P1\">\
  \\n      <part-name>Music</part-name>\
  \\n    </score-part>\
  \\n  </part-list>\
  \\n  <part id=\"P1\"/>"
 appendFile filename (show xml)
 appendFile filename "</part></score-partwise>"