module Tinte.Balken where
import Debug.Trace
import Data.List

import Tinte.Data
import Tinte.Constants
import Tinte.Util
import Tinte.Macros
import Tinte.AltSeq
import Tinte.Hals
import Tinte.Fahnen
import Tinte.Balkeno


--import Dvi.Dvi

{--
balkeno :: [GenElem] -> GenElem
balkeno [] s = initgenelem s
--balkeno [x] s = fa (halso x) s 
balkeno elss s
 = balko list gefaltet s
   where
    (els,sneu) = gefaltet  s{notecontext=(Head:notecontext s)
                            ,beamDirection=(Up:beamDirection s)}
    gefaltet=(foldl1 (-/)  elss) 
    list = [(a,b,c,d)
           |(a,(b,c,d,el))
               <-zip [1,2..] 
                 [( ( (pschlag el)),(phoehe el),(ptiefe el),el)
                 |el<-getSeq$parts els]
            ]

balkenu :: [GenElem] -> GenElem
balkenu [] s = initgenelem s
--balkenu [x] s = fa (halso x) s 
balkenu elss s
 = balku list gefaltet s
   where
    (els,sneu) = gefaltet  s{notecontext=(Head:notecontext s)
                            ,beamDirection=(Up:beamDirection s)}
    gefaltet=(foldl1 (-/)  elss) 
    list = [(a,b,c,d)
           |(a,(b,c,d,el))
               <-zip [1,2..] 
                 [( ( (pschlag el)),(phoehe el),(ptiefe el),el)
                 |el<-getSeq$parts els]
            ]
--}
balkeno =  balkenaux balko
balkenu =  balkenaux balku

balkenaux balkanart [] s = initgenelem s
balkenaux balkanart elss s
 = balkanart list gefaltet s
   where
    (els,sneu) = gefaltet  s{notecontext=(Head:notecontext s)
                            ,beamDirection=(Up:beamDirection s)}
    gefaltet=(foldl1 (-/)  elss) 
    list = [-- trace (show (a,b,c,d)++(show$def  el)++(show$pneedsStem el)) 
            (a,b,c,d)
           |(a,(b,c,d,el))
               <-zip [1,2..] 
                 [( (pschlag el),(phoehe el),(ptiefe el),el)
                 |el<-getSeq$parts els] --,pneedsStem $trace (show$def el ) el]
           ,hasStem$def el 
           ]


--Balken unten
{--
balkenu :: [GenElem] -> GenElem
balkenu [] s = initgenelem s
balkenu [x] s = fa (halso x) s 
balkenu elss s
 = (balku list ((foldl1 (-/) elss))) s
   where
    (els,sneu) = seqs elss s{notecontext=(Head:notecontext s)
                            ,beamDirection=(Down:beamDirection s)}
    list = [(a,b,c,d)
           |(a,(b,c,d,el))
               <-zip [1,2..] 
                 [( (head (schlaege el)),(hoehe el),(tiefe el),el)|el<-els]
            ,needsStem el]
--}

--Balken oben und unten
--balkenou :: [Int] [Int] [GenElem] -> GenElem
balkenou _ _  [] = initgenelem
balkenou [] _ [x] = fa (halsu x) 
balkenou _ [] [x] = fa (halso x) 
balkenou _ _  [x] = fa (halso x) 
balkenou ob un elss = b elss
 where
  b elss s = (balkou olist ulist ((foldr (-/) initgenelem elss))) s
   where
    els      = [fst (el s)|el<-elss]
    list = [(a,b,c,d)|(a,(b,c,d))<-zip [1,2..] 
               [( (head (schlaege el)),(hoehe el),(tiefe el))|el<-els]]
    olist    = [(a,b,c,d)|(a,b,c,d)<-list,elem a ob]
    ulist    = [(a,b,c,d)|(a,b,c,d)<-list,elem a un]

 
{--dvihalsu nhoehe starth (xst,yst) w bhoehe
 = odvi (D [Push,X4 w,Y4 there,Set_rule (abs halsl) halsdicke,Pop])
   where
    halsl = there-nhoehe*halbtonab
    there  = starth+div (yst*w) xst
--}
notahalsu nhoehe starth (xst,yst) w bhoehe
 = onotation [PushPos,MoveRight w,MoveDown (there-halsl),PutLine (halsdicke) (0,abs halsl) ,PopPos]
   where
    halsl = there-nhoehe*halbtonab
    there  = starth+div (yst*w) xst

--dvibalksu :: Int [(Int,Int)] (Int,Int) [(Int,Int)] -> Odvi
{--dvibalksu gibt fromtos steigung weiten
  = odvi (D [Push])
     +++concatO [dvibalksegment gibt fromto steigung weiten|fromto<-fromtos]
     +++odvi (D [Pop,Y4 (-(balkendicke+2*div balkendicke 3))])
--}
notabalksu gibt fromtos steigung weiten
  = onotation [PushPos]
     +++concatON [notabalksegment gibt fromto steigung weiten|fromto<-fromtos]
     +++onotation [PopPos,MoveDown (-(balkendicke+2*div balkendicke 3))]

-- balkweitenu :: Int Int  [(Int,Int,Int,Int)] Int Int -> [(Int,Int)]
balkweitenu dicke s [] z n = []
balkweitenu dicke s ((h,min,max,off):xs) z n 
 = ((s+off,(h+2)*halbtonab+dicke):balkweitenu dicke br xs z n)
    where
      br  = s+(quant min max z n)




--berechnesteigungu ::[(Int,Int)] Grace -> (Int,(Int,Int))
berechnesteigungu znps gr
 |zuhoch    = (korrektur,(xst,yst))
 |otherwise = (starth,(xst,yst))
  where
   (vor,(xh1,yh1),nach)
        = splitvormaxelnach (\x y-> (snd x)>(snd y)) punkte
   punkte    = [(w,h)|(w,h)<-znps]
   versuche  
    |null nach 
      = [(st,su(abstaendeu st punkte tiefster))|st<-((1,0):steigungenpos)] 
    |null vor 
      = [(st,su(abstaendeu st punkte tiefster))|st<-((1,0):steigungenneg)]
    |ausgewogen  = [((1,0),su(abstaendeu (1,0) punkte tiefster))]
    |vormass>nachmass
      = [(st,su(abstaendeu st punkte tiefster))|st<-((1,0):steigungenpos)]  
    |otherwise    
      = [(st,su(abstaendeu st punkte tiefster))|st<-((1,0):steigungenneg)] 
   tiefster  = (xh1,yh1)
   starth    = (yh1-div (yst*xh1) xst)
   posvers    = filter (\(_,y)->(y>=0)) versuche
   posvs    = (sortBy (\x1 x2 -> compare (snd x1) (snd x2)) posvers)
   (xst,yst)  = fst (head posvs)
   vormass    = div (sum [(yh1-h)|(_,h)<-vor]) (length vor)
   nachmass  = div (sum [(yh1-h)|(_,h)<-nach]) (length nach)
   ausgewogen  = abs(vormass-nachmass)<= 6*halbtonab
   laengen    = abstaendeu (xst,yst) punkte tiefster
   maxlaenge  = maximum laengen
   hhhlaenge  |gr = halslaenge-6*halbtonab|otherwise = halslaenge-3*halbtonab
   zuhoch    = maxlaenge < hhhlaenge
   korrektur  = starth+(hhhlaenge-maxlaenge)
   su abs
    |ok    = sum abs
    |otherwise  = -1 
      where
       ok  = null (filter (\x->(x<(-halbtonab))) abs)
   tiefsterp  = (xh1,yh1+2*halbtonab)

abstaendeu (xst,yst) punkte (xh1,yh1) = abst
  where
   gleichung x  = (yh1+div (yst*(x-xh1)) xst)
   eval x h   = ((gleichung x)-h)
   abst    = [eval w h |(w,h)<-punkte]



balko :: [(Int,(Int,Int),Int,Int)]-> GenElem -> GenElem
balko poslist genelem = balkoat 1 poslist genelem

balkoat :: Int ->[(Int,(Int,Int),Int,Int)]-> GenElem -> GenElem
balkoat _ [] alt  s   = alt s
balkoat i balkdef alts s 
 = (alt {parts = partsneu},s1{notecontext=tail$notecontext s1
                             ,beamDirection=tail$beamDirection s1})
   where
    (alt,s1)    = alts s{notecontext=(Head:notecontext s)
                        ,beamDirection=(Up:beamDirection s) }
    partsalt    = parts alt
    partsneu = changeHead addBalken partsalt
    addBalken p  
       = p { ppostdefs = ppostdefs p ++ [Balkoat i balkdef]}


--balkoatpart :: Global ->Int-> [(Int,(Int,Int),Int,Int)]-> Cluster -> (Int -> Int -> Odvi,Int -> Int -> ONotation)
balkoatpart _  _ [] partsaltalt  = (\z n -> onotation [])
balkoatpart s i balkdef partsalt = b balkdef partsalt
 where 
  b balkdef partsalt  = (notaneu)
   where
--    dvineu = \z n -> (odvi (D [Push,Y4 (-staffhoehe)]) 
--                     +++dvifuerbalken z n+++odvi (D [Pop]))
    notaneu= \z n -> onotation [PushPos,MoveDown (-staffhoehe)] 
                     +++notafuerbalken z n+++onotation [PopPos]
{--    dvifuerbalken z n 
     = odvi (D [Push]) +++(odvi$D [X4 (mass notendicke-boff),Push, Y4 starth])
        +++concatO [dvibalks gibt bs (xst,yst) bmitzn|bs<-balkl]
        +++odvi (D [Pop])
        +++concatO [dvihalso h starth (xst,yst) w hb mass
                   |(h,(w,hb))<-(zip tiefenuns bmitzn)]
        +++odvi (D [Pop])
           where
             bmitzn  = balkmitzn z n
             (starth,(xst,yst))  = (berechnesteigung bmitzn sgrace) --}
    notafuerbalken z n 
     = onotation [PushPos] +++(onotation [MoveRight (mass notendicke-boff),PushPos,MoveDown starth]
        +++concatON [notabalks gibt bs (xst,yst) bmitzn|bs<-balkl]
        +++onotation [PopPos]
        +++concatON [notahalso h starth (xst,yst) w hb mass
                   |(h,(w,hb))<-(zip tiefenuns bmitzn)]
        +++onotation [PopPos])
           where
             bmitzn  = balkmitzn z n
             (starth,(xst,yst))  = (berechnesteigung bmitzn sgrace)
    balkmitzn z n= [((pwoliegt p) z n+poffset p,h*halbtonab-maxdicke)
                   |((_,h,t),p)<-korpartsliste]
    maxdicke= (balkend maxbalk)+2*halbtonab
    maxbalk  = maximum balkens
    balkl  = balkenliste (zip balkens [1..])
    balkens  = map balks (map (\((s,_,_),_)->s) korpartsliste)
    korpartsliste
      = [((schlag,hoch,tief),getsuccess(sel nr partsalt))
        |(nr,schlag,hoch,tief)<-balkdef,success(sel nr partsalt)]
    boff  = poffset (singleHead partsalt)
    gibt  = length korpartsliste
    tiefenuns = map (\((_,_,t),_) ->t) korpartsliste
--    pdvi = \z n -> (dvineu z n)
    mass n |sgrace = div (n*13) 20|otherwise = n
    sgrace = theGrace s
    staffhoehe  = putatstaff i 


balku :: [(Int,(Int,Int),Int,Int)] ->GenElem -> GenElem
balku poslist genelem = balkuat 1 poslist genelem

balkuat :: Int ->[(Int,(Int,Int),Int,Int)] ->GenElem -> GenElem
balkuat _ [] alt s    = alt s
balkuat i balkendef alts  s
 = (alt{parts = partsneu},s1{notecontext=tail$notecontext s1
                            ,beamDirection=tail$beamDirection s1})
   where
    (alt,s1) = alts s{notecontext=(Head:notecontext s)
                     ,beamDirection=(Down:beamDirection s) }
    partsalt  = parts alt
    addbalken p  = p{ppostdefs = ppostdefs p ++ [Balkuat i balkendef]}
    partsneu  = changeHead addbalken partsalt


--balkuatpart :: Global Int [(Int,Int,Int,Int)] Cluster -> (Int -> Int -> Odvi)
balkuatpart _  _ [] partsaltalt  = (\z n -> onotation [])
balkuatpart s  i balkdef partsalt = b balkdef partsalt
 where 
  b balkdef partsalt  = (notaneu)
   where
    notaneu= \z n -> onotation [PushPos,MoveDown (-staffhoehe)] 
                     +++notafuerbalken z n+++onotation [PopPos]
    notafuerbalken z n
     = onotation [PushPos] +++(onotation [MoveRight (-(boff)),PushPos,MoveDown starth])
       +++concatON [notabalksu gibt bs (xst,yst) bmitzn|bs<-balkl]
       +++onotation [PopPos]
       +++concatON [  notahalsu h starth (xst,yst) w hb
                  |  (h,(w,hb)) <-(zip hoehenuns bmitzn)]
       +++onotation [PopPos]
        where
          bmitzn  = balkmitzn z n
          (starth,(xst,yst))  = berechnesteigungu bmitzn sgrace
    balkmitzn z n= [  ((pwoliegt p) z n+poffset p,t*halbtonab+maxdicke)
                   | ((_,h,t),p)<-korpartsliste]
    maxdicke= (balkend maxbalk)+2*halbtonab
    maxbalk  = maximum balkens
    balkl  = balkenliste (zip balkens [1..])
    balkens  = map balks (map (\((s,_,_),_)->s) korpartsliste)
    schls  = map (\(_,a,_,_)-> a) balkdef
    korpartsliste = [  ((schlag,hoch,tief),getsuccess(sel nr partsalt))  
                    | (nr,schlag,hoch,tief)<-balkdef
                    ,  success(sel nr partsalt)]
    boff  = poffset (singleHead partsalt) 
    gibt  = length korpartsliste
    tiefenuns = map (\((_,_,t),_) ->t) korpartsliste
    hoehenuns = map (\((_,h,_),_) ->h) korpartsliste

    sgrace = theGrace s
    staffhoehe  = putatstaff i 
--    pdvi = \z n -> (dvineu z n)

--balkou :: [(Int,Int,Int,Int)]-> [(Int,Int,Int,Int)]-> GenElem -> GenElem
balkou = balkouat 1

balkouat i [] [] alt s    = alt s
balkouat i balkdefo [] alt s  = balko balkdefo alt s
balkouat i [] balkdefu alt s  = balku balkdefu alt s
balkouat i balkdefo  balkdefu alts s 
 = (alt {parts = partsneu},s1{notecontext=tail$notecontext s1})
   where
    (alt,s1)    = alts s{notecontext=(Head:notecontext s)}
    partsalt    = parts alt
    partsneu = changeHead addBalken partsalt
    addBalken p  
       = p { ppostdefs = ppostdefs p ++ [Balkouat i  balkdefo  balkdefu]}


-- //balken sowohl oberhalb als auch unterhalb
--balkou :: [(Int,Int,Int,Int)] [(Int,Int,Int,Int)] GenElem -> GenElem
{--balkou [] [] alt     = alt
balkou balkdefo [] alt  = balko balkdefo alt
balkou [] balkdefu alt  = balku balkdefu alt
balkou balkdefous balkdefuus alts = b balkdefous balkdefuus alts
 where
  b balkdefous balkdefuus alts s = (alt { parts = partsneu},s1) 
   where
    (alt,s1)  = alts s
    balkdefu  = sortBy (\(a,_,_,_) (b,_,_,_) -> compare a b) balkdefuus
    balkdefo  = sortBy (\(a,_,_,_) (b,_,_,_) -> compare a b) balkdefous
    partsalt  = parts alt
    dvineu  = \z n -> (odvi (D [Push]) +++dvifuerbalken z n+++odvi (D [Pop]))
    dvifuerbalken z n 
     = odvi (D [Push,X4 (-(boff)),Push, Y4 starth])
       +++concatO [dvibalksou gibt bs (xst,yst) bmitzn|bs<-balkl]
       +++odvi (D [Pop])
       +++concatO [dvihalsou ou h starth (xst,yst) w hb maxdickeh
                  |(h,(ou,w,hb)) <-(zip hoehens bmitzn)]
       +++odvi (D [Pop])
          where
            bmitzn  = balkmitzn z n
            (starthfast,(xst,yst))  = berechnesteigungou bmitzn
            starth  = starthfast+maxdickeh
            balkmitzn z n
              = [ (uo,obenverschieben uo ((pwoliegt p) z n+poffset p)
                     ,berechnewohin uo t h)
                |  (_,uo,(_,h,t),p)<-korpartsliste]
    berechnewohin Ub t h = t*halbtonab+maxdickeh
    berechnewohin Ob t h = h*halbtonab-maxdickeh
    obenverschieben Ob i = i+notendicke
    obenverschieben Ub i = i
    maxdicke= (balkend maxbalk)+2*halbtonab
    maxdickeh = (div (balkend maxbalk)2)
    maxbalk  = maximum balkens
    balkl  = balkenliste (zip balkens [1..])
    balkens  = map balks (map (\(_,_,(s,_,_),_)->s) korpartsliste)
    boff  = poffset (singleHead partsalt)
    gibt  = length korpartsliste
    tiefens = map (\(_,_,(_,_,t),_) ->t) korpartsliste
    hoehens = map (\(_,_,(_,h,_),_) ->h) korpartsliste
    addbalken p  = p{  pdvi = \z n-> (dvineu z n+++pdvi p z n)}
    partsneu  = changeHead addbalken partsalt
    balkenso= map balks (map (\(_,_,(s,_,_),_)->s) korpartslisteo)
    balkensu= map balks (map (\(_,_,(s,_,_),_)->s) korpartslisteu)
    schlso  = map (\(_,a,_,_)-> a) balkdefo
    schlsu  = map (\(_,a,_,_)-> a) balkdefu
    schls  = schlso++schlsu
    korpartslisteo
      = [  (nr,Ob,(schlag,hoch,tief),getsuccess(sel nr partsalt))  
        | (nr,schlag,hoch,tief)<-balkdefo,success(sel nr partsalt)]
    korpartslisteu
      = [  (nr,Ub,(schlag,hoch,tief),getsuccess(sel nr partsalt))  
        | (nr,schlag,hoch,tief)<-balkdefu,success(sel nr partsalt)]
    korpartsliste
      = sortBy (\(a,_,_,_) (b,_,_,_)->compare a b) (korpartslisteo++korpartslisteu)
--}


balkouatpart _  _ [] [] partsaltalt  = (\z n->onotation [])
balkouatpart s i balkdefuus balkdefous partsalt =(notaneu)
 where 
    balkdefu  = sortBy (\(a,_,_,_) (b,_,_,_) -> compare a b) balkdefuus
    balkdefo  = sortBy (\(a,_,_,_) (b,_,_,_) -> compare a b) balkdefous
    notaneu  = \z n -> onotation [PushPos] +++notafuerbalken z n+++onotation [PopPos]

    notafuerbalken z n 
     = onotation [PushPos,MoveRight (-(boff)),PushPos,MoveDown starth]
       +++concatON [notabalksou gibt bs (xst,yst) bmitzn|bs<-balkl]
       +++onotation [PopPos]
       +++concatON [notahalsou ou h starth (xst,yst) w hb maxdickeh
                  |(h,(ou,w,hb)) <-(zip hoehens bmitzn)]
       +++onotation [PopPos]
          where
            bmitzn  = balkmitzn z n
            (starthfast,(xst,yst))  = berechnesteigungou bmitzn
            starth  = starthfast+maxdickeh
            balkmitzn z n
              = [ (uo,obenverschieben uo ((pwoliegt p) z n+poffset p)
                     ,berechnewohin uo t h)
                |  (_,uo,(_,h,t),p)<-korpartsliste]
    berechnewohin Ub t h = t*halbtonab+maxdickeh
    berechnewohin Ob t h = h*halbtonab-maxdickeh
    obenverschieben Ob i = i+notendicke
    obenverschieben Ub i = i
    maxdicke= (balkend maxbalk)+2*halbtonab
    maxdickeh = (div (balkend maxbalk)2)
    maxbalk  = maximum balkens
    balkl  = balkenliste (zip balkens [1..])
    balkens  = map balks (map (\(_,_,(s,_,_),_)->s) korpartsliste)
    boff  = poffset (singleHead partsalt)
    gibt  = length korpartsliste
    tiefens = map (\(_,_,(_,_,t),_) ->t) korpartsliste
    hoehens = map (\(_,_,(_,h,_),_) ->h) korpartsliste
--    addbalken p  = p{  pdvi = \z n-> (dvineu z n+++pdvi p z n)}
--    partsneu  = changeHead addbalken partsalt
    balkenso= map balks (map (\(_,_,(s,_,_),_)->s) korpartslisteo)
    balkensu= map balks (map (\(_,_,(s,_,_),_)->s) korpartslisteu)
    schlso  = map (\(_,a,_,_)-> a) balkdefo
    schlsu  = map (\(_,a,_,_)-> a) balkdefu
    schls  = schlso++schlsu
    korpartslisteo
      = [  (nr,Ub,(schlag,hoch,tief),getsuccess(sel nr partsalt))  
        | (nr,schlag,hoch,tief)<-balkdefo,success(sel nr partsalt)]
    korpartslisteu
      = [  (nr,Ob,(schlag,hoch,tief),getsuccess(sel nr partsalt))  
        | (nr,schlag,hoch,tief)<-balkdefu,success(sel nr partsalt)]
    korpartsliste
      = sortBy (\(a,_,_,_) (b,_,_,_)->compare a b) (korpartslisteo++korpartslisteu)





notabalksou gibt fromtos steigung weiten
 = onotation [PushPos]
     +++concatON [notabalksegment gibt fromto steigung (map (\(_,w,h) -> (w,h)) weiten)|fromto<-fromtos]
     +++onotation [PopPos,MoveDown (-(balkendicke+div(2*balkendicke)3))]




notahalsou Ub nhoehe starth (xst,yst) w bhoehe maxdickeh
 = onotation [PushPos,MoveRight w,MoveDown (there-halsl),PutLine halsdicke (0,(abs halsl)) ,PopPos]
   where
     halsl = there-nhoehe*halbtonab
     there  = starth+div (yst*w) xst
notahalsou Ob nhoehe starth (xst,yst) w bhoehe maxdickeh
 = onotation [PushPos,MoveRight w,MoveDown (nhoehe*halbtonab-halsl),PutLine halsdicke (0,abs halsl) ,PopPos]
   where
    halsl = abs (starth+div (yst*w) xst-nhoehe*halbtonab) + 2*maxdickeh


data UO  = Ub | Ob

berechnesteigungou :: [(UO,Int,Int)]-> (Int,(Int,Int))
berechnesteigungou punkte  = (starth,(xst,yst))
 where
  wechselpunkt = mittevonwechsel punkte
  (xh1,yh1)  = wechselpunkt 
  versuche  = [(st,su(abstaendeou st punkte wechselpunkt))|st<-((1,0):steigungen)] 
  starth    = (yh1-div (yst*xh1) xst)
  posvers    = filter (\(_,y)->(y>=0)) versuche
  posvs    = (sortBy (\x1 x2 -> compare (snd x1) (snd x2)) posvers)
  (xst,yst)  = if (null posvs)  then (1,0) else  (fst (head posvs))
  su abs
    |ok    = sum abs
    |otherwise  = -1 
     where
      ok  = null (filter (\x->(x<(-(halbtonab)))) abs)

mittevonwechsel ((Ub,w1,h1):(Ob,w2,h2):rest) = (div (w1+w2) 2,div (h1+h2) 2)
mittevonwechsel ((Ob,w1,h1):(Ub,w2,h2):rest) = (div (w1+w2) 2,div (h1+h2) 2)
mittevonwechsel (_:xs) = mittevonwechsel xs

abstaendeou (xst,yst) punkte (xh1,yh1) = abst
  where
   gleichung x  = (yh1+div (yst*(x-xh1))xst)
   evaluo Ub x h = ((gleichung x)-h)
   evaluo Ob x h = (h-(gleichung x))
   abst    = [(evaluo uo w h)-4*halbtonab |(uo,w,h)<-punkte]

