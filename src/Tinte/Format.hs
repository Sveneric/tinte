module Tinte.Format where
import Data.List
import Data.Maybe

import Tinte.Constants
import Tinte.Data
import Tinte.Util
import Tinte.Macros
import Tinte.AltSeq
import Tinte.Noten
import Tinte.Balken
import Tinte.Bogen
import Tinte.Grace

import Tinte.PP hiding (klammer)

import Debug.Trace

import Data.Ratio

type Zeile = ([Notation])
type Zeilen= [Zeile]
type Seiten= [Zeilen]

mktaktGlob :: Global -> GenElem -> (TaktInfo,Global)
mktaktGlob s altsOrg = (((mnotation),(minb,maxb)),s1)
   where
    alts = printTaktNumber altsOrg
    (alt,s0) = alts s
    wantsSegno = segno s0
    s1 = s0{taktNummer=taktNummer s0+1,segno=False}
    minb = minbreite alt
    maxb = maxbreite alt
    par   = parts alt
    mnotation z n = pdnotation z n
    pdnotation z n
       = onotation [PushPos]
           +++concatON 
                 [
                  onotation ( [PushPos,MoveRight (pwoliegtwirklich p z n-poffset p )])
                  +++(pvorsatznotation p)
                  +++onotation ([PopPos,PushPos,MoveRight (pwoliegtwirklich p z n)])
                  +++(pnotation p z n)
                  +++onotation ([PopPos,PushPos,MoveRight$poffset p])
                  +++(makepostdefs s p ps z n)
                  +++onotation ([PopPos])
                 |(p,ps)<-zipwithtail (flattenCluster par)]
              +++onotation [PopPos]+++segnoNota z n
    segnoNota z n 
     |wantsSegno = onotation[PushPos,MoveRight(quant minb maxb z n),MoveDown (-11*halbtonab),PutSegno,PopPos]
     |otherwise = onotation[]


-- //erste Argument wird nie gelesen //obsolet
-- //zweite Argument ist die reguläre defintion eines Stückes
tinteRenderPS ::  Int -> [(Global,[GenElem])] -> Seiten
tinteRenderPS _ parts  = (verteileAufSeiten (map machePart parts))

verteileAufSeiten :: [[(Global,Zeile)]] -> Seiten
verteileAufSeiten ys@(((g,_):_):xs)
 =  (take w zeils: verteileAufSeitenAux (drop w gs) (drop w  zeils))
  --  ++verteileAufSeiten xs 
  where 
   concated = concat ys
   zeils = map snd concated
   gs = map fst concated
   w = anzahlzeilenersteseite g
verteileAufSeiten _ = []

--verteileAufSeitenAux :: Int [Zeile] -> Seiten
verteileAufSeitenAux _ [] = []
verteileAufSeitenAux gs ys = (take w ys : verteileAufSeitenAux (drop w gs) (drop w ys))
  where
   w = anzahlzeilenweitereseiten$head gs

machePart :: (Global,[GenElem]) -> [(Global,Zeile)]
machePart (startGlob,elems) = splitInLines (calcGlobals startGlob elems)

linesplitting:: [(Global,TaktInfo)] -> [[(Global,TaktInfo)]]
linesplitting = processGlobalFeatures . (map (map snd))  . improveLastLine . linesplitting2

processGlobalFeatures [] = []
processGlobalFeatures [x] = [x]
processGlobalFeatures (z1:z2:xs) = z1Neu:processGlobalFeatures (z2Neu:xs)
 where
   (g1start,z1first)=head z1
   (g1end,z1last)   =last z1
   (g2start,z2first)=head z2
   z1Neu = addAnnotationLine z1
   z2Neu = z2


addAnnotationLine [] = [] 
addAnnotationLine (x@(g,(odviOld,ab)):xs) 
   = (x:addAnnotationLine xs)
 -- ((g,(foldr1 (appGenOdvi) (odviOld:(openline++linien)),ab)):addAnnotationLine xs)
   where
      name =fromJust $openUpper g
      --openline
      -- |isJust$openUpper g = [punktlinie name start ab]
      -- |otherwise = []
      --ans          =closedAnnotation g
      --linien       = trace (show$length ans)$map mkLinie ans

improveLastLine (x1:x2:x3:x4:x5:xs) = x1:improveLastLine (x2:x3:x4:x5:xs)
improveLastLine xs
 |null n = xs
 |otherwise =head n 
   where 
     islegal xs  = and $map notToLong  xs
     notToLong xs = 0<= freiRaum xs
     n = sortBy (\x y -> compare (maximum$map freiRaum x) (maximum$map freiRaum y))
             [brks |brks <- breaking (length xs) (concat xs),islegal brks]

freiRaum xs@((_,(gx,x)):_) = lx-xslength
       where
         xslength = sum $map fst xs 
         lx = zeilenlaenge gx

dropLast []=[]
dropLast [x]=[]
dropLast (x:xs)=x:dropLast xs 

breaking:: Int -> [a] ->[ [[a]] ]
breaking 1 xs = [[xs]]
breaking n arg@(x:xs) 
 |length xs == n-1 = [map (\y->[y]) arg]
 |otherwise = [[x]:tls|tls<-tlss] ++ [((x:t):ls)|(t:ls)<-tlss2]
  where
   tlss = breaking (n-1) xs
   tlss2= breaking n xs

linesplitting2 els@(e1:_) = (erst):weiter
  where
    (g,_) = e1
    (_,(erst,_)) = getpasst g initl schlab [] els 
    weiter = linesplitting2 (drop genommen els)
    schlab = (laenge-(schluesselab+artabb+3*schluesselab `div` 4))
    initl = artab g
    laenge = zeilenlaenge g
    artabb = artab g
    genommen = length   erst
linesplitting2 [] = []
--linesplitting2 rest = [rest]

splitInLines :: [(Global,TaktInfo)] -> [(Global,Zeile)]
splitInLines els = map splitInLines2 $ linesplitting els

splitInLines2 erst@(e1:_)
  = (g
    ,((((notationzeile g) . zeile2notation endzeilenglobal laenge) ( (fit staffs schlab erst)))))
  where
    (g,_) = e1
    schlab = (laenge-(schluesselab+artabb))
    staffs = systemanzahl g
    laenge = zeilenlaenge g
    endzeilenglobal = fst$head erst
    artabb = artab g

partitionen schlab teiler taktstrbr els 
  = concat [npartition schlab teiler taktstrbr i els |i<-[1..length els]]


npartition _ _ _ _ [] = []
npartition schlab teiler taktstrbr 1 el
 = [[bewertezeile schlab teiler taktstrbr el]]
npartition schlab teiler taktstrbr n1 els 
  = concat (map (\(a,as)->(map (\x->(a:x)) as))
           (filter akzeptabel (map ak [1..maxnehmen])))
 where
  menge = length els
  maxnehmen = menge-n
  n = n1-1
  akzeptabel ((i,_),_) 
     = (i> (-lockeraufseite)) && (i < ((div schlab 30)*3)) -- ((schlab*2)/3))
  ak i = (bew,npartition schlab teiler taktstrbr n (drop i els))
    where 
     bew = bewertezeile schlab teiler taktstrbr (take i els)

lockeraufseite = 360 * notendicke

bewertezeile schlab teiler taktstrbr zeile 
  = (schlab - (mi+(div ((ma-mi)*10) teiler)+l*taktstrbr),zeile)
     where
       (mi,ma) = foldr (\(a1,a2) (b1,b2) -> (a1+b1,a2+b2)) (0,0) 
                  [ab | (_,(_,ab))<-zeile]
       l = length zeile - 1


calcGlobals :: Global -> [GenElem] -> [(Global,TaktInfo)]
calcGlobals startGlob [] = []
calcGlobals startGlob (e:lems) 
  = (nnGlob,((apON klammerKastenNota$apON addedNotaZN onotattionzn),firstTaktAb))
    :calcGlobals nnGlob{taktstrichart=Normal,klammer=Nothing} lems
  where 
    (((onotattionzn),firstTaktAb),newGlob) = mktaktGlob startGlob e
    annos
      =  (maybe [] (\x->[StartUpper "" (\z n->0)]) (openUpper startGlob))
       ++(maybe [] (\x->[StartLower "" (\z n->0)]) (openLower startGlob))
       ++(collectAnnos$parts (fst$e startGlob))
    (annotation,openLow,openUp) = createAnnos firstTaktAb annos
    nnGlob = newGlob{openUpper=maybe Nothing (\x->Just$fst x) openUp
                    ,openLower=maybe Nothing (\x->Just$fst x) openLow}
    linienNotaZN = map mkLinieNota annotation 

    addedNotaZN = foldr apON  (\z n->onotation [])  linienNotaZN

    klammerKastenNota
     = maybe (\z n ->onotation []) 
             (\x->(mkKlammerNota (show x) firstTaktAb))
             (klammer newGlob)  


mkKlammerNota name (mi,ma) z n 
 =onotation 
  ([PushPos,MoveDown ((-20)*halbtonab),PushPos,PutText (fntNumberToPSFont fntfinger) name]
  ++[PopPos,MoveDown (div halbtonab 1),PushPos
   ,PutLine strichdicke ((quant mi ma z n), 0),PopPos
   ,PutLine strichdicke (0,(2*halbtonab))  ,PopPos])


mkLinieNota (UpperAnnotation name start end) z n
 = onotation ([PushPos,MoveDown ((-20)*halbtonab)]++addAnnoNota (start z n) name
          ++[PopPos,PushPos,MoveRight (start z n),MoveDown ((-17)*halbtonab)]
          ++(punktlinie2Nota (start z n) (end z n))++[PopPos]) 
mkLinieNota (LowerAnnotation name start end) z n
 = onotation ([PushPos,MoveDown ((11)*halbtonab)]++addAnnoNota (start z n)name
          ++[PopPos,PushPos,MoveRight$ start z n,MoveDown ((8)*halbtonab)]
          ++(punktlinie2Nota (start z n) (end z n))++[PopPos]) 


punktlinie2Nota start end
 |start > end = []
 |otherwise   = pNota++punktlinie2Nota (start+notendicke) end



addAnnoNota start name =[MoveRight start,PutText (fntNumberToPSFont fntfinger) name ]



pNota=[PushPos,PutDot,PopPos,MoveRight notendicke]

createAnnos mima@(mi,ma) annos
 = (anlistLow++endlessL++anlistUp++endlessU,stl,stu)
  where
    (lows1,ups1) = partition aLower annos
    lows2 = sortBy (\x y -> compare (whereIsAnno  x 1 1) (whereIsAnno y 1 1))lows1
    ups2 = sortBy (\x y -> compare (whereIsAnno  x 1 1) (whereIsAnno y 1 1))ups1
    (anlistUp,stu) =  makeAnnoList  ups2
    (anlistLow,stl) =  makeAnnoList  lows2
    endlessU = maybe [] (\(n,s)->[UpperAnnotation n s (quant mi ma)]) stu
    endlessL = maybe [] (\(n,s)->[LowerAnnotation n s (quant mi ma)]) stl

makeAnnoList [] = ([],Nothing)
makeAnnoList [StartLower name start] = ([],Just (name, start))
makeAnnoList [StartUpper name start] = ([],Just (name, start))
makeAnnoList ((StartUpper name st):(EndUpper nd):xs) 
 = ((UpperAnnotation name st nd):rest,mb)
  where
    (rest,mb) = makeAnnoList xs
makeAnnoList ((StartLower name st):(EndLower nd):xs) 
 = ((LowerAnnotation name st nd):rest,mb)
  where
    (rest,mb) = makeAnnoList xs
makeAnnoList (x:xs) = trace ("something strange with: "++show x)$makeAnnoList xs 


getpasst :: Global -> Int -> Int -> [(Int,(Global,TaktInfo))] ->[(Global,TaktInfo)]
  -> (Bool,([(Int,(Global,TaktInfo))],[(Global,TaktInfo)]))
getpasst g schon    _    anf [] = (_Ready,(anf,[]))
getpasst glob schon max  anf t@((gl,(takt,(mi,ma))):ts)
  |max >= (schon+nael) = getpasst glob (schon+nael) max begt ts
  |(abs (max-schon))>(abs (max-(schon+mi+(div ((ma-mi)*10) teilerr)))) 
     = (_NotReady,(begt,ts))
  |otherwise = (_NotReady,(anf,t))
    where
     nael = mi+(div((ma-mi)*10)teilerr)+taktmin+(div ((taktmax-taktmin)*10)teilerr)
     begt = (anf++[(nael,(gl,(takt,(mi,ma))))])
     teilerr = teiler glob

_Ready = True      
_NotReady = False

fit :: Int  -> Int ->[(Global,TaktInfo)] -> (ONotation)
fit staffs  i zeile 
 = --trace ("fittingline ("++show zaehler++","++show nenner++")"++show (zaehler % nenner)++"]")
    (    concatON (map (\((nota),(mi,ma))
                         -> --trace( "mi: "++show mi++" ma: "++show ma)
                          (quantnachnotation (nota zaehler (nenner)) mi ma zaehler nenner))
         withBars))
   where 
     withBars=inserttaktstrich staffs  zeile
     numbtakt = length zeile
     (minn,maxx) = pairsum (map (snd.snd) zeile)
     tktstrbr = sum$map (taktstrichbreite.taktstrichart.fst) zeile
     min = (fromIntegral minn+fromIntegral (numbtakt-1)*fromIntegral taktmin+fromIntegral tktstrbr)::Integer
     max = (fromIntegral maxx+fromIntegral (numbtakt-1)*fromIntegral taktmax+fromIntegral tktstrbr)::Integer
     zaehler =  (fromInteger $(abs (div (fromIntegral i-min) 1000)))::Int
     nenner  = (fromInteger $(abs (div(max-min)$ fromIntegral 1000) ))::Int
     
inserttaktstrich staffs  [] = []
inserttaktstrich staffs  ((g,x):xs) 
 = 
  (x
   :( (
     quantmitnotation  (onotation (partstaktstrichnota staffs (taktstrichart g))) (br+taktmin)(br+ taktmax)   )
    ,(taktmin+br,taktmax+ br))
   :inserttaktstrich staffs  xs)
  where
   br = taktstrichbreite (taktstrichart g)


notationzeile g x 
 = notation (onotation  [PushPos]+++x
   +++onotation [PopPos,MoveDown (linab+staffs*(takthoehe+staffabstand)-staffabstand)])
    where
     staffs = systemanzahl g
     linab  = linienabstand g


zeile2notation glob laengeo xs 
 =  concatON
      (if (theStimme glob == 0) --Komplettpartitur
       then
        [onotation  [PushPos]+++ (onotation (notenliniennotation (getKeyChar key) laenge))
          +++onotation [MoveRight (-notendicke)]
          +++vorzeichenNota key  (tonartsymbol glob)
          +++onotation  [PopPos,MoveDown (staffabstand)]
        |key <- keys ] 
       else if (systemanzahl glob > 1) -- einzelne Stimme aus mehreren Systemzeilen
       then
         [onotation  [PushPos]+++ (onotation (notenliniennotation (getKeyChar key) laenge))
          +++onotation [MoveRight (-notendicke)]
          +++vorzeichenNota key  (tonartsymbol glob)
          +++onotation  [PopPos,MoveDown (staffabstand)]
         |key <- drop ((theStimme glob)-1) keys ] 
       else  
        [onotation  [PushPos]+++ (onotation (notenliniennotation (getKeyChar key) laenge))
          +++onotation [MoveRight (-notendicke)]
          +++art 
          +++onotation  [PopPos,MoveDown (staffabstand )]
        |key <-  [(keys !! ((theStimme glob) -1))] ] 
      )
          +++onotation [MoveDown (takthoehe-staffabstand)]
          +++onotation (systemtaktstrichnota systemanzahll)
          +++onotation (braces)
          +++onotation  [MoveRight (schluesselab+artabb)]
          +++ xs 
       where
         art   = vorzeichenNota key  (tonartsymbol glob)
         artabb = artab glob
         laenge = zeilenlaenge glob
         systemanzahll = systemanzahl glob
         keys = schluessel glob
         key
           |theStimme glob >0 = keys !! ((theStimme glob) -1)
           |otherwise = head keys
         taktstrichArt = taktstrichart glob

         braces = concat [[PushPos,MoveRight (-notendicke),MoveDown (-(systemanzahl glob-n)*staffabstand),PutBrace takthoehe,PopPos]|n <- doubleStaff glob]
