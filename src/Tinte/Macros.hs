module Tinte.Macros where

import Tinte.Constants
import Tinte.Util
import Tinte.Data

isOdd x = mod x 2 == 1

notenliniennotation key l
 =  [PutLine 9830 (l,0),
    MoveDown 327680,
    PutLine 9830 (l,0),
    MoveDown 327680,
    PutLine 9830 (l,0),
    MoveDown 327680,
    PutLine 9830 (l,0),
    MoveDown 327680,
    PutLine 9830 (l,0)
   ,MoveDown (-2*halbtonab)
   ,MoveRight (2*halbtonab)
   ,key
   ]

--setzehilfslinie mass = Put_rule strichdicke (mass hilslaenge)
setzehilfslinienota mass = PutLine strichdicke (mass hilslaenge,0)

hilfslinien :: Int -> Int
hilfslinien note
      |note>1  = note 
      |note<(-9)   = (note+8)
      |otherwise = 0

setzehilfsliniennotation:: Int  -> Grace -> [Notation]
setzehilfsliniennotation i grace
 |i<0 && (isOdd i) 
   = (PushPos:MoveRight (-hoffset):MoveDown (halbtonab):setzenegliniennota (div i 2+1) mass)++[PopPos]
 |i<0 = (PushPos:MoveRight (-hoffset):setzenegliniennota (div i 2) mass)++[PopPos]
 |i>0 && (isOdd i)
   = (PushPos:MoveRight (-hoffset):MoveDown (-halbtonab):(setzeposliniennota (div i 2) mass))++[PopPos]
 |i>0 = (PushPos:MoveRight (-hoffset):setzeposliniennota (div i 2) mass)++[PopPos]
 |otherwise = []
   where
    mass n |grace = div (n*13)  20|otherwise = n
    hoffset = mass hilfsoffset


setzenegliniennota n mass
        |not (n==0) =  (PushPos: setzehilfslinienota mass
                :MoveDown (2*halbtonab-strichdicke):setzenegliniennota (n+1) mass)++[PopPos]
 |otherwise = []


setzeposliniennota n mass
  |not(n==0) = (PushPos: setzehilfslinienota mass:
                    MoveDown (-2*halbtonab):setzeposliniennota (n-1) mass)++[PopPos]
  |otherwise = []


taktstrichnota          =  [MoveDown (-takthoehe),PutLine (2*strichdicke)(0,takthoehe) ]
doppeltaktstrichnota    =  [MoveDown (-takthoehe),PutLine (2*strichdicke)(0,takthoehe)
                           ,MoveRight (2*dickstrich),PutLine (2*strichdicke)(0,takthoehe) ]
endwiedertaktstrichnota =  dotdot++[MoveDown (-takthoehe),
                            MoveRight (2*dickstrich),PutLine (2*strichdicke) (0,takthoehe) 
                           ,MoveRight (2*dickstrich),PutLine dickstrich (0,takthoehe)
                           ,MoveRight (2*dickstrich) ]
endtaktstrichnota       =  [MoveDown (-takthoehe),PutLine (2*strichdicke) (0,takthoehe) 
                          ,MoveRight (2*dickstrich),PutLine dickstrich (0,takthoehe) ]
endwtaktstrichnota      =  [MoveDown (-takthoehe),PutLine (2*strichdicke) (0,takthoehe) 
                          ,MoveRight (2*dickstrich),PutLine dickstrich (0,takthoehe) ]

dotdot =[PushPos,MoveDown (-3*halbtonab),PutDotDot,PopPos]


endanfangwiedertaktstrichnota 
                      =  dotdot++[MoveDown (-takthoehe),
                            MoveRight (2*dickstrich),PutLine (2*strichdicke) (0,takthoehe) 
                           ,MoveRight (2*dickstrich),PutLine dickstrich (0,takthoehe)
                           ,MoveRight (2*dickstrich),MoveDown takthoehe ]++dotdot

anfangwiedertaktstrichnota
                    =  [PushPos,MoveDown (-takthoehe),PutLine strichdicke (0,takthoehe) 
                       ,MoveRight (dickstrich),PutLine dickstrich (0,takthoehe)
                       ,PopPos,MoveRight (3*dickstrich)]++dotdot


systemtaktstrichnota i = [PushPos,MoveDown (-(i-1)*(staffabstand)-takthoehe)
                       --  ,PutLine strichdicke
                       --     (0,((i-1)*(staffabstand)+takthoehe))
                         ]++theBar++
                         [PopPos ]
 where
  theBar |i==1 =[PutLine strichdicke (0,((i-1)*(staffabstand)+takthoehe))]
         |otherwise = [MoveRight (-2*dickstrich),PutStaffBar ((i-1)*(staffabstand)+takthoehe)]



partstaktstrichnota i taktstrichArt
 = concat [[PushPos, MoveDown (-(putatstaff s))]++strichnota taktstrichArt++[PopPos]|s<-[1..i]]


strichnota Normal                 = taktstrichnota
strichnota Doppel                 = doppeltaktstrichnota
strichnota EndeWiederholung       = endwiedertaktstrichnota
strichnota Ende                   = endtaktstrichnota
strichnota EndeAnfangWiederholung = endanfangwiedertaktstrichnota 
strichnota AnfangWiederholung     = anfangwiedertaktstrichnota

taktstrichbreite Normal           = 0 --strichdicke
taktstrichbreite Doppel           = 2*dickstrich
taktstrichbreite EndeWiederholung = doppelpunktbreite + 2*dickstrich
taktstrichbreite Ende=2*dickstrich
taktstrichbreite EndeAnfangWiederholung = 2*doppelpunktbreite + 2*dickstrich
taktstrichbreite AnfangWiederholung = doppelpunktbreite + 2*dickstrich
