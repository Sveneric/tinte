module Tinte.Akkord where
import Data.List
import Tinte.Rat

import Tinte.Data
import Tinte.Constants
import Tinte.Util
import Tinte.Macros
import Tinte.Hals
import Tinte.Noten
import Tinte.Balken
import Tinte.AltSeq
import Tinte.Fahnen

import Debug.Trace
data MaxMin = Max | Min

akk :: [GenElem] -> GenElem
akk gen s 
 |(head$beamDirection s)==Up = akko gen s
 |(head$beamDirection s)==Down = akku gen s
 |otherwise= akkaux Max gen s

akkmax  :: [GenElem] -> GenElem
akkmax gen s
 |(head$beamDirection s)==Up = akko gen s
 |(head$beamDirection s)==Down = akku gen s
 |otherwise=akkaux Max gen s

akkaux :: MaxMin ->  [GenElem] -> GenElem
akkaux _ [] s = initgenelem s
akkaux maxmin elss s 
  = (  initdvielem 
        {maxbreite  = maxn
        ,minbreite  = minn
        ,minzw    = notendicke+maximum minzws
        ,maxzw    = notendicke+maximum maxzws
        ,zwischenr  = quant (notendicke+maximum minzws)(notendicke+maximum maxzws)
        ,hoehe    = hoechste
        ,tiefe    = tiefste
        ,ref_hoehe  = hoechste
        ,schlaege  = [minrat [foldr add (0,1) sgs|sgs<- schlge]]
        ,parts    = newparts},sneu{notecontext=tail$notecontext sneu})
    where
      (els,sneu) = seqs elss s{notecontext=(Head:notecontext s)}
      maxn  = maxbpl+off
      minn  = minbpl+off
      (hoehenuns,maxbreits,minbreits,minzws,maxzws,schlge)
        = unzip6 [  (ref_hoehe el,maxbreite el,minbreite el,
              minzw el,maxzw el,schlaege el)|el<-els]
      clusters    = map (\x->parts x) els
      partss    = map flattenCluster clusters
      pvorart    = [ pvorsatzart (head parts) |parts<-partss]
      vorsaetzes  = concat pvorart
      pnotass    = [map (\x->pnotation x) parts|parts<-partss]
      hmitnota   = zip hoehenuns  pnotass
      hoehen    = sort hoehenuns
      hoechste  = head hoehen
      tiefste    = last hoehen

      loeschzwnota (dvizn, pnotazn)
        = \z n ->   
              onotation [PushPos]+++concatON [d z n|d<-pnotazn]+++onotation  [PopPos]
      notanorm   = foldl apON     (\z n -> onotation []) (map loeschzwnota normkoeppenota)
      notag z n  = quantmitnotation (notanorm z n)  minn maxn z n
      maxb    = mb maxmin maxbreits
      mb Min x   = minimum x
      mb Max x   = maximum x
      maxbpl    = maxb+versetzung
      minb    = mb maxmin minbreits
      minbpl    = minb+versetzung
      versetzung  = 0

      normkoeppenota  = [(d,pds)|(d,pds)<-hmitnota] 
      vorhoehen  = reverse (sort (map snd vorsaetzes))
      (vor1,vor2,vor3)
        = vhoehenverteilen vorhoehen

      vors1nota    = concatON [mkvorsatznota v|v<-vorsaetzes,elem (snd v) vor1]
      vors2nota    = concatON [mkvorsatznota v|v<-vorsaetzes,elem (snd v) vor2]
      vors3nota    = concatON [mkvorsatznota v|v<-vorsaetzes,elem (snd v) vor3]
      (howfar,vnota) 
        = if  (null vor1) 
          then (0,onotation [])
           else if (null vor2) 
             then  (1,vors1nota+++onotation [MoveRight isdicke])
              else  if  (null vor3) 
               then  (2,vors1nota+++onotation [MoveRight isdicke]
                          +++vors2nota+++onotation [MoveRight isdicke])
               else 
                        (3,vors1nota+++onotation [MoveRight isdicke]+++vors2nota
                              +++onotation [MoveRight isdicke]+++vors3nota
                              +++onotation [MoveRight isdicke])
      off      = howfar*isdicke
      newparts 
       = Term (head (head partss)) {
                pnotation =notag,
                poffset    = off,
               pvorsatznotation  = vnota
               ,pschlag = (1,16)--minrat (map (\x ->pschlag (head x)) partss)
   ,pneedsStem=True}
              


akko elss s
 |(head$notecontext s)==Note = fo(halso (akkoaux elss)) s
 |otherwise = akkoaux elss s

--Akkord mit nach o b e n zeigenden Hals
--akko :: [GenElem] -> GenElem
akkoaux [] s = initgenelem s
akkoaux elss s
   = (  initdvielem 
        {maxbreite  = maxn
        ,minbreite  = minn
        ,minzw    = notendicke+maximum minzws
        ,maxzw    = notendicke+maximum maxzws
        ,zwischenr  = quant (notendicke+maximum minzws) (notendicke+maximum maxzws)
        ,hoehe    = hoechste
        ,tiefe    = tiefste
        ,ref_hoehe  = hoechste
        ,schlaege  = newSchlaege
        ,parts    = newparts},sneu{notecontext=tail$notecontext sneu})
    where
      newSchlaege = [minrat [foldr add (0,1) sgs|sgs<- schlge]]-- head schlge
      (els,sneu) = seqs elss s{notecontext=(Head:notecontext s)}

      maxn  = maxbpl+off
      minn  = minbpl+off
      (hoehenuns,maxbreits,minbreits,minzws,maxzws,schlge)
        = unzip6 [  (ref_hoehe el,maxbreite el,minbreite el,
              minzw el,maxzw el,schlaege el)|el<-els]
      partss    = map (\x->flattenCluster $parts x) els
      pnotass    = [map (\x->pnotation x) parts|parts<-partss]
      pvorart    = [ pvorsatzart(head parts) |parts<-partss]
      vorsaetzes = concat pvorart--concat vorsaetzess
      hmitnota   = zip hoehenuns  pnotass
      hoehen     = sort hoehenuns
      hoechste   = head hoehen
      tiefste    = last hoehen
      loeschzwnota pdviszn
        = \z n -> onotation [PushPos]+++concatON [d z n|d<-pdviszn]+++onotation [PopPos]

      notanorm   = foldl apON     (\z n -> onotation []) (map loeschzwnota normkoeppenota)

      notavers   = foldl apON     (\z n -> onotation []) (map loeschzwnota verskoeppenota)

      notag z n = onotation [PushPos]+++notanorm z n+++
              onotation [PopPos]+++onotation [PushPos,MoveRight versetzung]
              +++notavers z n+++onotation [PopPos]
      maxb    = minimum maxbreits
      maxbpl    = maxb+versetzung-- +off
      minb    = minimum minbreits-- +off
      minbpl    = minb+versetzung
      versetzung  |null versetzt   = 0
            |otherwise      = notendicke
      hrev    = reverse hoehen
      (normal,versetzt) = verteilekoeppeo hrev

      normkoeppenota  = [pds | (n,pds)<-hmitnota,elem n normal] 

      verskoeppenota  = [pds |(n,pds)<-hmitnota,elem n versetzt] 
      vorhoehen  = reverse (sort (map snd vorsaetzes))
      (vor1,vor2,vor3)
        = vhoehenverteilen vorhoehen

      vors1nota    = concatON [mkvorsatznota v|v<-vorsaetzes,elem (snd v) vor1]
      vors2nota    = concatON [mkvorsatznota v|v<-vorsaetzes,elem (snd v) vor2]
      vors3nota    = concatON [mkvorsatznota v|v<-vorsaetzes,elem (snd v) vor3]
      (howfar,vnota) 
        = if  (null vor1)  then (0,onotation [])
           else if (null vor2) 
             then  (1,vors1nota+++onotation [MoveRight isdicke])
              else  if  (null vor3) 
               then  (2,vors1nota+++onotation [MoveRight isdicke]
                          +++vors2nota+++onotation [MoveRight isdicke])
               else 
                        (3,vors1nota+++onotation [MoveRight isdicke]+++vors2nota
                              +++onotation [MoveRight isdicke]+++vors3nota
                              +++onotation [MoveRight isdicke])
      off      = howfar*isdicke
      newparts
        = Term(head (head partss)) 
             {pnotation= notag
             ,poffset    = off-- +versetzung,
             ,pvorsatznotation  = vnota
             ,pvorsatzart  = vorsaetzes
             ,def=(Akkord $map def $concat partss) Up
             ,phoehe    = hoechste
             ,ptiefe    = tiefste
             ,pneedsStem=True
             ,pschlag = head newSchlaege
             }


akku elss s
 |(head$notecontext s)==Note = fu(halsu (akkuaux elss)) s
 |otherwise = akkuaux elss s

--Akkord mit nach u n t e n zeigenden Hals
akkuaux :: [GenElem] -> GenElem
akkuaux [] = initgenelem
akkuaux elss = ak elss
  where
   ak elss s  
    = (  initdvielem 
      {maxbreite  = maxbpl
      ,minbreite  = minbpl
      ,minzw    = notendicke+maximum minzws
      ,maxzw    = notendicke+maximum maxzws
      ,zwischenr  = quant (notendicke+maximum minzws) (notendicke+maximum maxzws)
      ,hoehe    = hoechste
      ,tiefe    = tiefste
      ,ref_hoehe  = tiefste
      ,schlaege  =newSchlaege --[minrat [foldr add (0,1) sgs|sgs<- schlge]] --head schlge
      ,parts    = newparts},sneu{notecontext=tail$notecontext sneu})
    where
      newSchlaege = [minrat [foldr add (0,1) sgs|sgs<- schlge]]-- head schlge
      (els,sneu) = seqs elss s{notecontext=(Head:notecontext s)}
      (hoehenuns,maxbreits,minbreits,minzws,maxzws,schlge)
        = unzip6 [  (ref_hoehe el,maxbreite el,minbreite el,
           minzw el,maxzw el,schlaege el)|el<-els]
      vorsaetzes  = concat pvorart--concat vorsaetzess
      hmitnota   = zip hoehenuns  pnotass
      hoehen    = sort hoehenuns
      hoechste  = head hoehen
      tiefste    = last hoehen
      loeschzwnota pdviszn
        = \z n -> onotation [PushPos]+++concatON [d z n|d<-pdviszn]+++onotation [PopPos]

      notanorm   = foldl apON (\z n -> onotation []) (map loeschzwnota normkoeppenota)

      notavers   = foldl apON (\z n -> onotation []) (map loeschzwnota  verskoeppenota)


      notag z n = onotation [PushPos,MoveRight zurueckVersetzt]+++notavers z n+++
              onotation [PushPos,MoveRight versetzung]+++notanorm z n
              +++onotation [PopPos,PopPos]
      pvorart    = [pvorsatzart  (head parts) |parts<-partss]
      maxb    = minimum maxbreits
      maxbpl    = maxb+versetzung-- +off
      minb    = minimum minbreits-- +off
      minbpl    = minb+versetzung
      zurueckVersetzt    = (-versetzung)
      versetzung  |null versetzt   = 0
            |otherwise      = notendicke
      hrev    = reverse hoehen
      (normal,versetzt)
        = verteilekoeppeo hrev
      normkoeppenota  = [pds |(n,pds)<-hmitnota,elem n normal] 
      verskoeppenota  = [pds |(n,pds)<-hmitnota,elem n versetzt] 

      vorhoehen  = reverse (sort (map snd vorsaetzes))
      (vor1,vor2,vor3)
        = vhoehenverteilen vorhoehen
  --    vors3    = concatO [mkvorsatz v|v<-vorsaetzes,elem (snd v) vor3]
      vors1nota= concatON [mkvorsatznota v | v<-vorsaetzes,elem (snd v) vor1]
      vors2nota= concatON [mkvorsatznota v | v<-vorsaetzes,elem (snd v) vor2]
      vors3nota= concatON [mkvorsatznota v | v<-vorsaetzes,elem (snd v) vor3]
{--      (howfar,vd)
        = if  (null vor1)  
          then (0,odvi (D []))
          else if  (null vor2)
          then   (1,vors1+++odvi (D [X3 isdicke]))
          else if  (null vor3) 
          then (2,vors1+++odvi (D [X3 isdicke])
                          +++vors2+++odvi (D [X3 isdicke]))
          else      (3,vors1+++odvi (D [X3 isdicke])+++vors2
                                +++odvi (D [X3 isdicke])+++vors3
                                +++odvi (D [X3 isdicke]))
--}
      (howfar,vnota)
        = if  (null vor1)  
          then (0,onotation [])
          else if  (null vor2)
          then   (1,vors1nota+++onotation [MoveRight isdicke])
          else if  (null vor3) 
          then (2,vors1nota+++onotation[MoveRight isdicke]
                          +++vors2nota+++onotation[MoveRight isdicke])
          else      (3,vors1nota+++onotation [MoveRight isdicke]+++vors2nota
                                +++onotation [MoveRight isdicke]+++vors3nota
                                +++onotation [MoveRight isdicke])
      off      = howfar*isdicke
      partss   = map (\x->flattenCluster $parts x) els
--      pdviss    = [map (\x->pdvi x) parts|parts<-partss]
      pnotass    = [map (\x->pnotation x) parts|parts<-partss]
      newparts 
       = Term (head (head partss)) 
           {pnotation= notag
           ,poffset    = off+versetzung
           ,pvorsatznotation  = vnota
           ,pvorsatzart  = vorsaetzes
           ,def=(Akkord $map def $concat partss) Down
           ,phoehe    = hoechste
           ,ptiefe    = tiefste
           ,pschlag = head newSchlaege
           ,pneedsStem=True}



mkvorsatznota (Is _,h)  = onotation[PushPos,MoveDown (h*halbtonab),PutSharp,PopPos]
mkvorsatznota (Es _,h)  = onotation[PushPos,MoveDown (h*halbtonab),PutFlat,PopPos]
mkvorsatznota (Na _,h)  = onotation[PushPos,MoveDown (h*halbtonab),PutNatural,PopPos]
mkvorsatznota (Isis _,h)= onotation[PushPos,MoveDown (h*halbtonab),PutDoubleSharp,PopPos]
mkvorsatznota (Eses _,h)= onotation[PushPos,MoveDown (h*halbtonab),PutDoubleFlat,PopPos]
mkvorsatznota _      = onotation []

verteilekoeppeo []  = ([],[])
verteilekoeppeo [x]  = ([x],[])
verteilekoeppeo (x1:x2:xs)
  |x1<=x2+1  = ((x1:n1),(x2:v1))
  |otherwise  = ((x1:n2),v2)
    where
      (n1,v1) = verteilekoeppeo xs
      (n2,v2)  = verteilekoeppeo (x2:xs)


vhoehenverteilen :: [Int] -> ([Int],[Int],[Int])
vhoehenverteilen []    = ([],[],[])
vhoehenverteilen [x]  = ([x],[],[])
vhoehenverteilen x@[x1,x2]
  |x1<=x2+3  = ([x1],[x2],[])
  |otherwise  = (x,[],[])
vhoehenverteilen (x1:x2:x3:xs)
  |x1<=x2+3 && x1<=x3+3  = ((x1:u1),(x2:u2),(x3:u3))
  |x1<=x2+3 && x2<=x3+3  = ((x1:v1),(x2:v2),v3)
  |x1<=x2+3         = ((x1:v2),(x2:v3),v1)
  |otherwise        = ((x1:w1),w2,w3)
    where
      (u1,u2,u3)  = vhoehenverteilen xs
      (v1,v2,v3)  = vhoehenverteilen (x3:xs)
      (w1,w2,w3)  = vhoehenverteilen (x2:x3:xs)




