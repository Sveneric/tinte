module Tinte.Noten where

import Tinte.Data
import Tinte.Constants
import Tinte.Util
import Tinte.Macros
import Tinte.Hals
import Tinte.Fahnen
import Tinte.AltSeq
--import Tinte.Grace
import Tinte.Notation
import Data.Ratio

import Debug.Trace


kEnv c beamDir glob  
 |(head$notecontext glob)==Head
   =  (k c (currentMod glob) (schlagToMin sl) (schlagToMax sl) (schlagToNoteHead sl) (schlagToNoteHead sl) 
       sl  (schlagNeedsStem sl) ((schlagToNote sl) c (fst sl))) glob
 |beamDir==Automatic = (schlagToN sl) c (currentMod glob) glob 
 |beamDir==Up = (schlagToNo sl) c (currentMod glob) glob 
 |beamDir==Down = (schlagToNu sl) c (currentMod glob) glob 
  where
    sl = currentWert glob

kEnv2 c i beamDir glob 
 |(head$notecontext glob)==Head
   =  (k c i (schlagToMin sl) (schlagToMax sl) (schlagToNoteHead sl) (schlagToNoteHead sl) 
       sl  (schlagNeedsStem sl) ((schlagToNote sl) c (fst sl))) glob
 |beamDir==Automatic = (schlagToN sl) c i glob 
 |beamDir==Up = (schlagToNo sl) c i glob 
 |beamDir==Down = (schlagToNu sl) c i glob 
  where
    sl = currentWert glob

n1 c i= k1 c i
n2 c i = hals (k2 c i)
n4 c i = hals (k4 c i)
n8 c i = fa (hals (k8 c i))    
n16 c i = fa (hals (k16 c i))    
n32 c i = fa (hals (k32 c i))    
n64 c i = fa (hals (k64  c i))    

n1o c i= k1 c i
n2o c i = halso (k2 c i)
n4o c i = halso (k4 c i)
n8o c i = fo (halso (k8 c i))    
n16o c i = fo (halso (k16 c i))    
n32o c i = fo (halso (k32 c i))    
n64o c i = fo (halso (k64 c i))    

n1u c i= k1 c i
n2u c i = halsu (k2 c i)
n4u c i = halsu (k4 c i)
n8u c i = fu (halsu (k8 c i))    
n16u c i = fu (halsu (k16 c i))    
n32u c i = fu (halsu (k32 c i))    
n64u c i = fu (halsu (k64 c i))    

schlagToNoteHead (1,1) = ganzerKopf
schlagToNoteHead (1,2) = halberKopf
schlagToNoteHead _ = viertelKopf

schlagToDicke (1,1) = ganznotendicke
schlagToDicke (1,2) = ganznotendicke
schlagToDicke _ = notendicke

schlagNeedsStem (1,1) = False
schlagNeedsStem _ = True

schlagToMin (1,1) = n1min
schlagToMin (1,2) = n2min
schlagToMin (1,4) = n4min
schlagToMin (1,8) = n8min
schlagToMin (1,16) = n16min
schlagToMin (1,32) = n32min
schlagToMin (1,64) = n64min

schlagToN (1,1) = n1
schlagToN (1,2) = n2
schlagToN (1,4) = n4
schlagToN (1,8) = n8
schlagToN (1,16) = n16
schlagToN (1,32) = n32
schlagToN (1,64) = n64

schlagToNo (1,1) = n1o
schlagToNo (1,2) = n2o
schlagToNo (1,4) = n4o
schlagToNo (1,8) = n8o
schlagToNo (1,16) = n16o
schlagToNo (1,32) = n32o
schlagToNo (1,64) = n64o
schlagToNu (1,1) = n1u
schlagToNu (1,2) = n2u
schlagToNu (1,4) = n4u
schlagToNu (1,8) = n8u
schlagToNu (1,16) = n16u
schlagToNu (1,32) = n32u
schlagToNu (1,64) = n64u

schlagToMax (1,1) = n1max
schlagToMax (1,2) = n2max
schlagToMax (1,4) = n4max
schlagToMax (1,8) = n8max
schlagToMax (1,16) = n16max
schlagToMax (1,32) = n32max
schlagToMax (1,64) = n64max

k1 :: Char -> Int -> GenElem;
k1 c i = k c i n1min n1max ganznotendicke ganzerKopf schlag False (N (1%1) c i)
   where
  schlag  = (1,1)

s g = (initdvielem{parts=Term initpart},g)

k c i min max breite char schlag needsStem d s
 =(DVIelem
    {maxbreite=mmax
    ,minbreite=mmin
    ,minzw=mmax-mbreite
    ,maxzw=mmin-mbreite 
    ,zwischenr=quant (mmin-mbreite) (mmax-mbreite)
    ,hoehe=note
    ,tiefe=note
    ,offset=0 
--    ,genOdvi=quantmit  (odvi (D []))  mmin mmax
--    ,vorsatzdvi=(odvi(D[])) 
    ,schlaege=[schlag] 
  --,needsStem=needsStem
    ,ref_hoehe=note
    ,parts=Term (pa{pnotation=notat})
    }
   ,s{currentWert=schlag,currentMod = i})
   where
--  ddvi  = setzekopf char hoehe note mmin mmax (theGrace s)
  notat = setzekopfnotation schlag char hoehe note mmin mmax (theGrace s)
  note  = setzenoteum ((schluessel s)!!(currentVoice s-1)) c i  
  hoehe  = note*halbtonab
  pa    = mkpart ((schlagToNote schlag) c i) note schlag mmax mmin
  mass n |theGrace s = div (n*1) 2|otherwise = n
  mbreite  = mass breite
  mmin  = mass min
  mmax  = mass max


schlagToNote (x,y) = N (x%y)


-- mkpart :: Element Int (Int,Int) (Int Int -> Odvi) -> Part  
mkpart element note schlag  maxbr minbr 
 = initpart {poffset    = 0,
    pwoliegt  = \z n -> 0,
    ptiefe    = note,
    phoehe    = note,
 --   pvorsatzdvi  = odvi (D []),
 --   pdvi    = ddvi,
    pschlag    = schlag,
    pvorsatzart  = [],
    ppostdefs  = [],
    def = element
    ,pmaxbr=maxbr
    ,pminbr=minbr
    }
  

setzekopfnotation schlag kopfchr hoehe note min max grace
 = quantmitnotation
   (onotation
    ((PushPos:MoveDown hoehe:setzehilfsliniennotation (hilfslinien note) grace)
       ++ selfnt++[(if grace then schlag2gracekopf else schlag2kopf) schlag,PopPos])) min max
      where
        selfnt |grace = []|otherwise= []  


-- //kopf fuer halbe Noten      
-- k2 :: Char Int -> GenElem;
k2  c i = k  c i n2min n2max ganznotendicke halberKopf schlag True (N(1%2)  c i)
   where
  schlag  = (1,2) 


-- k4 :: Char Int -> GenElem;
k4  c i = k  c i n4min n4max notendicke viertelKopf schlag True (N (1%4) c i)
   where
  schlag  = (1,4) 


-- k8 :: Char Int -> GenElem;     
k8  c i = k  c i n8min n8max notendicke viertelKopf schlag True (N (1%8)  c i)
   where
  schlag  = (1,8) 


-- k16 :: Char Int -> GenElem;     
k16  c i = k  c i n16min n16max notendicke viertelKopf schlag True (N (1%16) c i)
   where
  schlag  = (1,16) 


-- k32 :: Char Int -> GenElem;
k32  c i = k  c i n32min n32max notendicke viertelKopf schlag True (N (1%32)c i)
   where
  schlag  = (1,32) 


-- k64 :: Char Int -> GenElem;
k64 c i = k  c i n64min n64max notendicke viertelKopf schlag True (N (1%64) c i)
   where
  schlag  = (1,64) 



-- gr8o :: Char Int -> GenElem;




