module Tinte.Balkeno where
import Data.List

import Tinte.Data
import Tinte.Constants
import Tinte.Util
import Tinte.Macros
import Tinte.AltSeq

import Tinte.Rat

concatparts [] _                 = []
concatparts (([],_,_):ps) vor    = concatparts ps vor
concatparts ((p1s,mi,ma):ps) vor = [vorschub p1 vor|p1<-p1s]
                                    ++concatparts ps vorneu
 where
    wo =pwoliegt (head p1s)
    vorneu = \z n ->(quant mi ma z n)+(vor z n)
    vorschub p vor = p { pwoliegt = (\z n -> (v z n)+(vor z n))}
      where
        v = pwoliegt p

--dvihalso :: Int Int !(Int,Int) Int a  (Int -> Int) -> Odvi;
--dvihalso nhoehe starth (xst,yst) w bhoehe mass
-- = odvi (D [Push,X4 w,Y3 (nhoehe*halbtonab),Set_rule (abs halsl) strichdicke,Pop])
--   where
--  halsl = starth+div (yst*w)xst-nhoehe*halbtonab

notahalso nhoehe starth (xst,yst) w bhoehe mass
 = onotation [PushPos,MoveRight w, MoveDown (nhoehe*halbtonab+halsl)
             ,PutLine (halsdicke) (0,abs halsl) ,PopPos]
   where
  halsl = starth+div (yst*w)xst-nhoehe*halbtonab

--splitvormaxelnach :: (a -> a -> Bool) ![a] -> ([a],a,[a]);
splitvormaxelnach _ [x] = ([],x,[])
splitvormaxelnach cmp (x:xs)
  |cmp x m  = ([],x,vor++(m:nach))
  |otherwise  = ((x:vor),m,nach)
  where
   (vor,m,nach) = splitvormaxelnach cmp xs

--balkweiten :: Int Int  [(Int,Int,Int,Int)] Int Int -> [(Int,Int)]
balkweiten dicke s [] z n = []
balkweiten dicke s ((h,min,max,off):xs) z n 
 = ((s+off,h*halbtonab-dicke):balkweiten dicke br xs z n)
    where
     br  = s+(quant min max z n)

-- dvibalks :: Int [(Int,Int)] (Int,Int) [(Int,Int)] -> Odvi
--dvibalks gibt fromtos steigung weiten
--  = odvi (D [Push])
--     +++concatO [dvibalksegment gibt fromto steigung weiten|fromto<-fromtos]
--     +++odvi (D [Pop,Y3 (balkendicke+div (2*balkendicke) 3)])

notabalks gibt fromtos steigung weiten
  = onotation [PushPos]
     +++concatON [notabalksegment gibt fromto steigung weiten|fromto<-fromtos]
     +++onotation [PopPos,MoveDown (balkendicke+div (2*balkendicke) 3)]

{--
--dvibalksegment :: Int !(!Int,!Int) !(Int,Int) [(Int,a)] -> Odvi;
dvibalksegment gibt (frm,to) st@(stx,sty) weiten
 |length weiten <= 1 =  (odvi (D [])) 
 |fglt&& not (frm==1) 
    = odvi (D [Push,X4 anfw2,Y3 (anfh2)])
      +++ zeichnelinie lweite2 st
      +++odvi (D [Pop])
 |fglt = odvi (D [Push,X4 anfw,Y3 (anfh)])+++ zeichnelinie lweite1 st
         +++odvi (D [Pop])
 |to==gibt = odvi (D [Push,X4 anfw,Y3 (anfh)])
             +++ zeichnelinie (lweite+halsdicke) st
             +++odvi (D [Pop])
 |otherwise = odvi (D [Push,X4 anfw,Y3 (anfh)])+++ zeichnelinie lweite st 
               +++odvi (D [Pop])
  where
    (anfw,_)  =      weiten!!(frm-1)
    (endw,_)  =      weiten!!(to-1)
    (endw1,_)  =     weiten!!(frm)
    (anfwe,_)  =    weiten!!(frm-2)
    anfw2  = anfw-lweite2
    endh  = div (sty*endw) stx
    endh1  = div (sty*(endw1-lweite1)) stx
    anfh  = div (sty*anfw) stx
    anfh2  = div (sty*anfw2) stx
    lweite  = endw-anfw
    lhoehe  = endh-anfh
    lweite1  = min notendicke (div (endw1-anfw) 2)
    lweite2  = min notendicke (div (anfw-anfwe) 2)
    lhoehe1  = endh1-anfh
    lhoehe2     = anfh-anfh2
    fglt  = frm==to

--}

notabalksegment gibt (frm,to) st@(stx,sty) weiten
 |length weiten <= 1 =  onotation []
 |fglt&& not (frm==1) 
    = onotation [PushPos,MoveRight (anfw2-halsdicke`div`2),MoveDown anfh2]
      +++ zeichnebeamnota lweite2 st
      +++onotation [PopPos]
 |fglt = onotation [PushPos,MoveRight (anfw-halsdicke`div`2),MoveDown anfh] +++ zeichnebeamnota lweite1 st
         +++onotation [PopPos] 
 |to==gibt = onotation [PushPos,MoveRight (anfw-halsdicke`div`2),MoveDown anfh]
             +++ zeichnebeamnota (lweite+halsdicke) st
             +++onotation [PopPos]
 |otherwise = onotation [PushPos,MoveRight (anfw-halsdicke`div`2),MoveDown anfh]+++ zeichnebeamnota lweite st 
               +++onotation [PopPos]
  where
    (anfw,_)  =      weiten!!(frm-1)
    (endw,_)  =      weiten!!(to-1)
    (endw1,_)  =     weiten!!(frm)
    (anfwe,_)  =    weiten!!(frm-2)
    anfw2  = anfw-lweite2
    endh  = div (sty*endw) stx
    endh1  = div (sty*(endw1-lweite1)) stx
    anfh  = div (sty*anfw) stx
    anfh2  = div (sty*anfw2) stx
    lweite  = endw-anfw
    lhoehe  = endh-anfh
    lweite1  = min notendicke (div (endw1-anfw) 2)
    lweite2  = min notendicke (div (anfw-anfwe) 2)
    lhoehe1  = endh1-anfh
    lhoehe2     = anfh-anfh2
    fglt  = frm==to

-- ///////////////////////////////
-- //Berechnung von Balkenanzahlen
-- balks :: !Int -> Int
{--balks 8   = 1
balks 16  = 2
balks 32  = 3
balks 64  = 4
balks _    = 0
--}

balks rat 
 |lessEq rat (1,64)  = 4
 |lessEq rat (1,32) || eq rat (3,64) = 3
 |lessEq rat (1,16) || eq rat (3,32) = 2
 |lessEq rat (1,8) || eq rat (3,16) || eq rat (7,32) = 1
 |otherwise = 0


--balkenliste :: [(Int,Int)] -> [[(Int,Int)]]
balkenliste []  = []
balkenliste xs  
 |maxb <= 0  = []
 |otherwise  = (balkenstufe maxb xs:balkenliste (map red xs))
  where
  (maxb,_) = maximum xs
  red (x,p) |x>0 = (x-1,p) |otherwise=(0,p)
  
balkenstufe mx [] = []
balkenstufe mx [(x1,p)] |x1>0 = [(p,p)]|otherwise = []
balkenstufe mx t@((x1,p):xs)
 |x1>0    = ((p,snd (last ms)):balkenstufe mx nx)
 |otherwise  = balkenstufe mx xs
  where
  ms  = takeWhile (\(x,p)->x>0) t
  nx  = dropWhile (\(x,p)->x>0) xs
  

--balkend :: !Int -> Int
balkend 1 = balkendicke
balkend n 
  |n<=0      = 0
  |otherwise  = n*balkendicke+div ((n-1)*balkendicke) 2


-- ////////////////////////////////
-- //Balkensteigungen mit vorgegebenen Steigungen
-- ///////////////////////////////

abstaende (xst,yst) punkte (xh1,yh1) = abst
  where
   gleichung x  = yh1+div (yst*(x-xh1)) xst
   eval x h   = h-(gleichung x)
   abst    = [eval w h |(w,h)<-punkte]

berechnesteigung znps gr
 |zutief    = (korrektur,(xst,yst))
 |otherwise = (starth,(xst,yst))
  where
   (vor,(xh1,yh1),nach)
     = splitvormaxelnach (\x y-> (snd x)<(snd y)) punkte
   punkte    = znps 
   versuche  
    |null nach = [(st,su(abstaende st punkte hoechster))|st<-((1,0):steigungenneg)] 
    |null vor = [(st,su(abstaende st punkte hoechster))|st<-((1,0):steigungenpos)]
    |ausgewogen = [((1,0),su(abstaende (1,0) punkte hoechster))]
    |vormass>nachmass
     = [(st,su(abstaende st punkte hoechster))|st<-((1,0):steigungenneg)]  
    |otherwise = [(st,su(abstaende st punkte hoechster))|st<-((1,0):steigungenpos)] 
   hoechster  = (xh1,yh1)
   starth    = yh1-div (yst*xh1)xst
   posvers    = filter (\(_,y)->(y>=0)) versuche
   posvs    = sortBy (\x1 x2 -> compare (snd x1) (snd x2)) posvers
   (xst,yst)  = fst (head posvs)
   vormass  = div (sum [(h-yh1)|(_,h)<-vor]) (length vor)
   nachmass  = div (sum [(h-yh1)|(_,h)<-nach]) (length nach)
   ausgewogen  = abs(vormass-nachmass)<= 6*halbtonab
   laengen    = abstaende (xst,yst) punkte hoechster
   maxlaenge  = maximum laengen
   hhhlaenge  |gr = halslaenge-4*halbtonab|otherwise = halslaenge-2*halbtonab
   zutief    = maxlaenge < hhhlaenge
   korrektur  = starth-(hhhlaenge-maxlaenge)
   su abs
    |ok    = sum abs
    |otherwise  = -1 
    where
     ok = null (filter (\x->(x<0)) abs)




