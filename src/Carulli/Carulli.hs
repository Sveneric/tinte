module Carulli.Carulli where


import Codec.Picture
import Codec.Picture.Types

import System.Environment
import System.Exit
import Debug.Trace

import Data.List
import Data.Maybe

import Carulli.TrainNotes hiding (main)

import AI.HNN.FF.Network
import Numeric.LinearAlgebra hiding (find)

import Data.Ratio

import Tinte.Data

moin args = do
  fname <- case args of
    [fn] -> return fn
    _    ->  putStrLn "usage: viewer <imagefile.ext>" >> exitFailure
  img <- readImage fname
  either
    (\n -> putStrLn n >> exitFailure)
    (toRGB fname) img

toRGB fname img = do
  print w
  print h

  nnN4 <- (loadNetwork "neuralNetworks/n4.nn")::(IO (Network Double))
  nnF8 <- (loadNetwork "neuralNetworks/f8.nn")::(IO (Network Double))


  let ls = skipDoubleLines$fmap fst $potentialLines i
  print ls
  print$minMaxDiff ls
  let staffs = mkstaffs i ls 
  print$ staffs
  
  let startEndStaff = map (startEnd i) staffs
  let staffDims = map staffDim startEndStaff  
  let noHeads1 =
          sort$
          map getMiddle$ 
          clusterBy (\(x1,y1)(x2,y2) -> abs (x1-x2) <=4 && abs (y1-y2) <=3)
          [(x,y)  |x<-[0..w-9],y<-[0..h-9],(output nnN4 tanh$fromList$img2list2 x y 6 6 i) > 0.9 ] 
  let hereMightBeBeams = looksLikeBeams i
  let hereMightBePosBeams = looksLikePosBeams i

  let theMightyBeams =  connectBeams(hereMightBeBeams++hereMightBePosBeams)

  let noHeads = filter (notInBeam (hereMightBeBeams++hereMightBePosBeams)) noHeads1

  let potf8os =  [(x,y)  |x<-[0..w-9],y<-[0..h-12],(output nnF8 tanh$fromList$img2list2 x y 6 10 i) > 0.9 ] 
  
  
  
  let staffSortedHeads =
        map (sortBy (\(x1,_)(x2,_)->compare x1 x2))$
--        map (clusterBy (\(x1,_)(x2,_)->abs (x1-x2)<7))$
        map (\((xB,xE),(yB,yE)) -> filter (\(x,y)-> x>=xB+(yE-yB)`div` 3 && x <= xE && y>= yB && y <= yE) noHeads) staffDims

  let f8os =
        map (sortBy (\(x1,_)(x2,_)->compare x1 x2))$
        map (\((xB,xE),(yB,yE)) -> filter (\(x,y)-> x>=xB+(yE-yB)`div` 3 && x <= xE && y>= yB && y <= yE) potf8os) staffDims

  
  let staffSortedBeams =
        map (sortBy (\(x1,_,_,_)(x2,_,_,_)->compare x1 x2))$
--        map (clusterBy (\(x1,_,_,_)(x2,_,_,_)->abs (x1-x2)<7))$
        map (\((xB,xE),(yB,yE)) -> filter (\(x,y,_,_)-> x>=xB+(yE-yB)`div` 3 && x <= xE && y>= yB && y <= yE)
                                     theMightyBeams) staffDims

        
  print "Taktstriche!"
  let bars2 = map (\staff->  findBars i staff) startEndStaff


  let bars1 = map realBars (zip bars2 staffSortedHeads)
--  print $zip bars1 $map concat staffSortedHeads
  print (head bars2)
  print (head bars1)
  
  let heads = map (filter (not.null).splitAtBars)   $trace (show$head$zip bars1 staffSortedHeads) $zip bars1 staffSortedHeads
  print $head heads
  let beams = map (filter (not.null).splitBeamsAtBars)$zip bars1 staffSortedBeams 

  print "Köpfe"

  let tinteElements 
        = map
        ( (\(f8o,staff,els)->map (mkElementsInBar f8o staff) els)
         .(\(f8o,staff,a,b)->(f8o,staff,zip a (  map (  -- (nubBy (\((n1,_),_) ((n2,_),_) -> n1==n2)).
                                            map (\x->(toElement staff x,stemDirection i x))) b))))
        $zip4 f8os staffs beams heads
  print $length$head tinteElements
  
  {--  
  let tinteElements
        = concat$
          map (\(lines,ess,bs)->
                 map (map ((mkBeamElements bs).concat. nub . map (\x-> (toElement lines x,stemDirection i x)).removeRedundant)) ess)
          (zip3 staffs staffSortedHeads staffSortedBeams)
--}
  let name = takeWhile (/= '.') fname

  let elementsStaffbars = map (map showElements) tinteElements
  
  let tinteSourceCode = "out =\n\
    \  { title =\""++name++"\"\n\
    \  , anzahlzeilenersteseite     = 8\n\
    \  ,anzahlzeilenweitereseiten     = 9\n\
    \  , linienabstand              = 900000*4+200000\n\
    \  , schluessel                 =[Violin]\n\
    \  , instrument                 =[\"guitar\"]\n\
    \  , tonartsymbol               = CDur\n\
    \  , systemanzahl               = 1\n\
    \  , teiler                     = 210\n\
    \  , stimme                     = 1\n\
    \  , taktNummer                 = 1\n\
    \  }\n\n\
    \test = [["++(concat$intersperse "\n   ,"$concat elementsStaffbars )++"]]"
  

  writeFile (name++".tinte") tinteSourceCode


  {--  
  let heads1 = map(\stff
          ->  --(stff, -- snd$partition (propablyBeam i 5)$
                     removeBeams$map head$findNoteHeads nnN4 i stff)  --)
        startEndStaff
  print heads1
--}

--  let headBars = map removeWrongBars$zip heads1 bars1

  print "Bilder erzeugen"
  
  img <- createMutableImage w h (PixelRGB8 255 255 255)
  img2 <- thawImage i  

--  let testOut = img
--  let testOut2 = img2
--  testOut <-  testOutImg (zip startEndStaff $map fst headBars) i img
--  testOut2 <- testOutImg (zip startEndStaff $map fst headBars) i img2

  testOut <-  testOutImg (zip startEndStaff [1..]) i img
  testOut2 <- testOutImg (zip startEndStaff [1..]) i img2

  print "Notenlinien gezeichnet"

  sequence_ $map (paintBars testOut) ( bars1)
  sequence_ $map (paintBars testOut2) ( bars1)

--  sequence_ $map (paintRest testOut) (rests)
--  sequence_ $map (paintRest testOut2) (rests)

  sequence_ $map (paintBeam2 testOut) theMightyBeams
  sequence_ $map (paintBeam2 testOut2) theMightyBeams


--  sequence_ $map (paintBeam testOut) hereMightBeBeams
--  sequence_ $map (paintBeam testOut2) hereMightBeBeams

--  sequence_ $map (paintPosBeam testOut) hereMightBePosBeams
--  sequence_ $map (paintPosBeam testOut2) hereMightBePosBeams

  sequence_ $map (paintN4 testOut) (concat$concat heads)
  sequence_ $map (paintN4 testOut2) (concat$concat heads)

  sequence_ $map (paintF8 testOut) (concat$f8os)
  sequence_ $map (paintF8 testOut2) (concat$f8os)


  sequence_ $map (paintStem testOut) (map (\x->(x,stemDirection i x))$concat$concat heads)
  sequence_ $map (paintStem testOut2) (map (\x->(x,stemDirection i x))$concat$concat heads)

  frozen <- freezeImage testOut
  frozen2 <- freezeImage testOut2

  writePng "testOut.png" frozen
  writePng "testOut2.png" frozen2 
  print "ready"
   where
    i@(Image w h pixels) = convertRGB8 img


notInBeam beams (x,y)
  = null [1|(bx,by,bw,bh)<-beams,x>=bx-3&&y>=by-3&&x<bx+bw&&y<by+bh]
    

{--
removeWrongBars (heads,bars)
  = (heads, map head $ clusterBy (\(x1,l1,l5)(x2,_,_)->(abs (x1-x2)<l5-l1)) notCloseToHead)
   where
    notCloseToHead = filter (\(xb,_,_) -> isNothing(find (\(Head _ xh) -> abs (xh-xb) < 5  ) heads )) bars
   --}  
potentialLines i@(Image w h pixels)
  = filter (\(l,s)->s> w* 5 `div` 8)
             $zip [1..][blackInRow i y |y<-[1..h-2]] 

isBlack (PixelRGB8 r g b) = r+g+b < 190

looksLikePosBeams i@(Image w h pxs)
  = [(x,y,15,5)|x<-[5..w-15],y<-[5..h-12]
          ,    somePosLine i (x,y) 14 7
            || someNegLine i (x,y) 14 7
            || somePosLine i (x,y) 15 5
            || someNegLine i (x,y) 15 5
          ]

getMiddle [x] = x
getMiddle xs  = divide $foldl (\(x1,y1)(x2,y2) ->(x1+x2,y1+y2)) (0,0) xs
  where
    divide (x,y)  = (x `div` lxs,y `div` lxs)
    lxs = length xs
                     
looksLikeBeams i@(Image w h pxs)
  = [(x,y,bfx,bfy)|x<-[0..w-6],y<-[0..h-6],darkArea x y]
  where
    darkArea x y
      =  (not$hasWhiteArea x y)
         &&sum [1|xp<-[x..x+bfx-1],yp<-[y..y+bfy-1],isBlack$pixelAt i xp yp] > bfx*bfy* 80 `div` 100 

    hasWhiteArea x y
      = not$ null
          [xp | xp<-[x..x+bfx-1-2]
              ,sum [1 | xpp <- [xp..xp+3], yp <-[y..y+bfy-1],not$isBlack$pixelAt i xpp yp ] >3*bfy-5 
              ]

bfx = 14
bfy = 3

skipDoubleLines (n:n1:n2:ls)
 |n1==n+1 &&n2 == n+2 = (n+1):skipDoubleLines ls
skipDoubleLines (n:n1:ls)
 |n1==n+1 = (n+1):skipDoubleLines ls
skipDoubleLines (n:ls) = n:skipDoubleLines ls
skipDoubleLines [] = []

minMaxDiff (l1:l2:ls) = mmAux (l2-l1)(l2-l1) (l2:ls)
  where
    mmAux min max (l1:l2:ls)
      |l2-l1 > max = mmAux min (l2-l1) (l2:ls)
      |l2-l1 < min = mmAux (l2-l1) max (l2:ls)
      |otherwise = mmAux min max (l2:ls)
    mmAux min max  _ = (min,max)
minMaxDiff _ = (0,0)

find5 min xs@[x1,x2,x3,x4,x5] = xs
find5 min ys@(x1:x2:xs)
  |x2-x1 > min +2 = x1:find5 min (x1+min:x2:xs) 
  |otherwise = x1:find5 min (x2:xs)
find5 min xs = xs

add1 i min xs
  |l == 5    = xs
  |l > 5     = take 5 xs
  |otherwise = trace ("add1: "++show (v++h))$add1 i min (v++xs++h)
  where
    l = length xs
    m = 5-l
    x1 = head xs
    xn = last xs
    h1 = x1-min
    t1 = xn+min
    
    (v,h) = better 

    better  
      |bh1>bt1 = ([h1],[])
      |otherwise = ([],[t1])
     where
       bh1 = blackInRow i h1
       bt1 = blackInRow i t1

blackInRow i@(Image w h pixels) y
 |y >= h-1 = 0
 |otherwise
      = sum [1 |x<-[0..w-1]
            ,  ( ( isBlack$pixelAt i x y))
             ||( (isBlack$pixelAt i x (y-1)))
             ||( (isBlack$pixelAt i x (y+1)))
            ] 

blackInColumn i@(Image w h pixels) x yStart yEnd
  = sum [1 |y<-[yStart..yEnd] ,isBlack$pixelAt i x y]

mkstaffs i [] = []
mkstaffs i ls = fmap ((add1 i min).(find5 min))
                (fs:mkstaffs i rs)
  where
    (min,max)= minMaxDiff ls

    (fs,rs) = getFirst ls

    getFirst [] = ([],[])
    getFirst (l1:l2:ls)
      |l2-l1 > min*4 = ([l1],ls)
      |otherwise = (l1:fs,rs)
         where 
          (fs,rs) = getFirst  (l2:ls)
    getFirst [x] = ([x],[])

startEnd i@(Image w h pixels) staff = ((startX,endX),staff)
  where
    startY = head staff
    endY   = last staff

    startX
      |null emptiesStart = 5
      |otherwise = fst$head emptiesStart

    endX
      |null emptiesEnd = w-5
      |otherwise = fst$head emptiesEnd

    emptiesStart  =  dropWhile (\(_,p)->p  < 5)
        [(x,blackInColumn i x startY endY) |x<-[0..w]]
                
    emptiesEnd = dropWhile (\(_,p)->p  < 5)
         [(x,blackInColumn i x startY endY )|x<-[w,w-1..0]]


staffDim (p,[l1,l2,l3,l4,l5]) = (p,(l1-(l2-l1)*5,l5+5*(l5-l4)))

findBars i ((startX,endX),[l1,l2,l3,l4,l5])
  = [(x,l1,l5)
    |x<-[startX+(l5-l1)..endX]
    ,length [y | y<-[l1..l5],isBlack$pixelAt i x y]>l5-l1-5
    ]


realBars ([],_) = []
realBars (_,[]) = []
realBars (bs@(b@(bx,_,_):bs'), ns@(n@(nx,_):ns'))
  |(abs (nx-bx) <= 8 && nx < bx)
     ||(abs (bx-nx)<=4  &&(bx < nx)) = realBars (bs', ns)
  |bx>nx =  (realBars (bs, ns'))
  |otherwise  = b:(realBars (bs', ns))


splitAtBars ([],ns) = [ns]
splitAtBars (_ , [n]) = [[n]]
splitAtBars (_ , []) = []
splitAtBars (bs@((bx,_,_):bs'), ns@(n@(nx,_):ns'))
  |bx>nx = intoHead n $ (splitAtBars (bs, ns'))
  |otherwise  = []:(splitAtBars (bs', ns))

intoHead n [] = [[n]]
intoHead n (x:xs) = ((n:x):xs)

splitBeamsAtBars ([],ns) = [ns]
splitBeamsAtBars (_ , [n]) = [[n]] 
splitBeamsAtBars (_ , []) = [] 
splitBeamsAtBars (bs@((bx,_,_):bs'), ns@(n@(nx,_,_,_):ns'))
  |bx>nx = intoHead n $ (splitBeamsAtBars (bs, ns'))
  |otherwise  = []:(splitBeamsAtBars (bs', ns))


removeRedundant xs = removeSame $sortBy sorter xs
  where
    sorter (x1,y1) (x2,y2)
      |abs (x1-x2) < 7 = compare y1 y2
      |x1 < x2 = LT
      |otherwise = GT

    
    removeSame [] = []
    removeSame [x] = [x]
    removeSame ((x1,y1):(x2,y2):(x3,y3):xs)
      |abs (x1-x2) < 7 &&abs (x1-x3) < 7 && abs (y1-y2)<4 && abs (y2-y3)<4  = removeSame ((x2,y2):xs)
    removeSame ((x1,y1):(x2,y2):xs)
      |abs (x1-x2) < 8 && abs (y1-y2)<4 = removeSame ((x2,y2):xs)
      |otherwise = (x1,y1):removeSame ((x2,y2):xs)


clusterBy eq (x:xs)
  = (((x:[y |y<-xs,eq y x]) : clusterBy eq [y |y<-xs,not $ eq y x]))
clusterBy _ [] = []



testOutImg staffs i@(Image w h pixels) img = do
  sequence_ (concat$map (staffRender img) staffs)
  return img
   where
    staffRender img (((xStart,xEnd),ls@[l1,l2,l3,l4,l5]),hds)
      =    (concat$ map (renderLine img xStart xEnd) ls) 

    renderLine img xStart xEnd l
     |l<h = [writePixel img x l (PixelRGB8 255 0 0)|x<-[xStart..xEnd],x>=0,x<w]
     |otherwise = []


paintBars i bars = sequence_ $concat$map paintBar bars
  where
    paintBar (x,y1,y2)
      = [writePixel i x y  (PixelRGB8 0 0 255)|y<-[y1..y2]]


paintRest i (x,y) = sequence_  [writePixel i xp yp  (PixelRGB8 0 255 0)|xp<-[x..x+5],yp<-[y..y+15]]

paintN4 i (x,y) = sequence_  [writePixel i xp yp  (PixelRGB8 0 255 0)|xp<-[x..x+5],yp<-[y..y+5]]

paintF8 i (x,y) = sequence_  [writePixel i xp yp  (PixelRGB8 255 100 0)|xp<-[x..x+5],yp<-[y..y+9]]

paintStem i ((x,y),Up) = sequence_  [writePixel i xp yp  (PixelRGB8 0 255 255)|xp<-[x+7],yp<-[y-25..y]]
paintStem i ((x,y),Down) = sequence_  [writePixel i xp yp  (PixelRGB8 255 255 0)|xp<-[x],yp<-[y..y+25]]


paintBeam2 i (x,y,bw,bh) = sequence_  [writePixel i xp yp  (PixelRGB8 200 200 200)|xp<-[x..x+bw-1],yp<-[y..y+bh-1]]

paintBeam i@(MutableImage w h pixels) (x,y,bw,bh)
  = sequence_  [writePixel i xp yp  (PixelRGB8 255 255 175)|xp<-[x..x+bw-1],yp<-[y..y+bh-1], xp<w, yp<h]

paintPosBeam i (x,y,bw,bh) = sequence_  [writePixel i xp yp  (PixelRGB8 255 255 0)|xp<-[x..x+bw-1],yp<-[y..y+bh-1]]

toElement ls (x,y) =  (toElement2 ls (x,y),(x,y))
  where
   toElement2 [l1,l2,l3,l4,l5] (_,y)
    |btw l5 (l5+l5-l4) y  = N (1%8) 'd' 1
    |btw l4 l5 y = N (1%8) 'f' 1
    |btw  l3 l4 y = N (1%8) 'a' 1
    |btw l2 l3 y = N (1%8) 'c' 2
    |btw l1 l2 y = N (1%8) 'e' 2
    |btw (l1-(l2-l1)) l1 y  = N (1%8) 'g' 2
    |btw (l1-2*(l2-l1)) (l1-1*(l2-l1)) y = N (1%8) 'h' 2
    |btw (l1-3*(l2-l1)) (l1-2*(l2-l1)) y = N (1%8) 'd' 3
    |btw (l1-2*(l2-l1)) (l1-3*(l2-l1)) y = N (1%8) 'f' 3
    |btw (l5+1*(l5-l4))  (l5+2*(l5-l4)) y  = N (1%8) 'h' 0
    |btw (l5+2*(l5-l4))  (l5+3*(l5-l4)) y  = N (1%8) 'g' 0
    |btw (l5+3*(l5-l4))  (l5+4*(l5-l4)) y  = N (1%8) 'e' 0
    |on l5 y  = N (1%8) 'e' 1
    |on l4 y  = N (1%8) 'g' 1
    |on l3 y  = N (1%8) 'h' 1
    |on l2 y = N (1%8) 'd' 2
    |on l1 y  = N (1%8) 'f' 2
    |on (l1-(l2-l1)) y = N (1%8) 'a' 2
    |on (l1-2*(l2-l1)) y = N (1%8) 'c' 3
    |on (l1-3*(l2-l1)) y = N (1%8) 'e' 3
    |on (l1-4*(l2-l1)) y = N (1%8) 'g' 3
    |on (l5+1*(l5-l4)) y = N (1%8) 'c' 1
    |on (l5+2*(l5-l4)) y = N (1%8) 'a' 0
    |on (l5+3*(l5-l4)) y = N (1%8) 'f' 0
    |otherwise = NoElement

   on l y = (abs (l-y)) <5 && abs (y+6-l) <5
   btw l2 l3 y = abs (l2 -y) <2 && abs (l3 -(y+6)) <3

showElements es = " "++concat (intersperse " "$map showElement es)++""

showDir Up = "^"
showDir Down = "_"
showDir _ = ""

showElement (((Alts es),(x,y)),dir) = " ("++concat (intersperse " | "$map  (\el -> showElement ((el,(x,y)),dir)) es)++")"   
showElement (((Akkord es dir),(x,y)),_)
  = " (<"++concat (intersperse ", "$map  (\el -> showElement ((el,(x,y)),NoStem)) es)++">"++showDir dir++")"
showElement ((N d c i,(x,y)),Up) = " n"++showDur d++"^"++c:show i
showElement ((N d c i,(x,y)),Down) = " n"++showDur d++"_"++c:show i
showElement ((N d c i,(x,y)),NoStem) = " k"++showDur d++c:show i
showElement ((Beam ns Down,(x,y)),_) = " ["++(concat$intersperse ", "$map  (\el -> showElement ((el,(x,y)),NoStem)) ns)++"]_"
showElement ((Beam ns Up,(x,y)),_) = " ["++(concat$intersperse ", "$map  (\el -> showElement ((el,(x,y)),NoStem)) ns)++"]^"
showElement  _ = " p4 "

showDur x
  |x==(1%4) = "4"
  |x==(1%8) = "8"
  |x==(1%16) = "16"
  |x==(1%32) = "32"
  |otherwise = "4"

stemDirection i@(Image w h pixels) (x,y)
 | sum [1 |y<-[y+4..y+16]
           , x < w
           , y < h
           ,  ( ( isBlack$pixelAt i x y))
              ||( (isBlack$pixelAt i (x+1) y))
              ||( (isBlack$pixelAt i (x+2) y))
              ||( (isBlack$pixelAt i (x-1) y))
           ] > 8= Down
 |otherwise = Up

somePosLine i@(Image w h pixels) (x,y) dx dy
  = sum [1|xp<-[0..dx],(not.null)[1|yp<-[0..dy],isBlack$pixelAt i (x+xp) (y+dy-xp*dy`div`dx ),isBlack$pixelAt i (x+xp) (y+dy-1-xp*dy`div`dx )]]
     > dx-2


someNegLine i@(Image w h pixels) (x,y) dx dy
  = sum [1|xp<-[0..dx],(not.null)[1|yp<-[0..dy],isBlack$pixelAt i (x+xp) (y+xp*dy`div`dx ),isBlack$pixelAt i (x+xp) (y-1+xp*dy`div`dx )]]
     > dx-3

connectBeams [] = []
connectBeams (be@(x,y,w,h):xs)
  |not$null connected = connectBeams (connect be connector:(xs\\[connector]))
  |otherwise = be:connectBeams xs
  where
    connected = [(x1,y1,w1,h1)| (x1,y1,w1,h1)<-xs,(x1>=x&&x1<=x+w)||(x>=x1&&x<=x1+w1),abs(y1-y)-1<=max h h1 ]
    connector = head connected

    connect (x1,y1,w1,h1) (x2,y2,w2,h2) = (xstart,ystart,xend-xstart,yend-ystart)
      where
        xstart = min x1 x2
        ystart = min y1 y2
        xend = max (x1+w1) (x2+w2)
        yend = max (y1+h1) (y2+h2)

mkElementsInBar f8os ls bar = sortBy (\((_,(x1,_)),_)((_,(x2,_)),_) -> compare x1 x2)
          $map mkAlt alts
  where
    es = mkElements ls bar
    alts = clusterBy (\x y-> abs (((fst.snd.fst) x)-((fst.snd.fst) y)) <= 8) es

    mkAlt [x] =  x 
    mkAlt xs = ((Alts (map (fst.fst) (map (setDuration f8os) ys)),snd$fst$head ys),snd$head ys)
      where
        isNote ((N _ _ _,_),_) = True
        isNote _ = False

        is upDown (_,updo) = upDown== updo        
        
        upper = [x | x <- xs, isNote x, is Up x]
        up = if (null upper)
          then [] else [((Akkord (map (fst.fst)upper) Up,snd$fst$head upper),Up)]
        
        lower = [x | x <- xs, isNote x, is Down x]
        lo = if (null lower)
          then [] else [((Akkord (map (fst.fst)lower) Down,snd$fst$head lower),Down)]
        noChord = xs\\(upper++lower)

        ys =  noChord++up++lo


setDuration ((fx,fy):f8os) x@((N d c i, (x1, y1)), dir)
  |abs (fx-x1)<8  =  x
setDuration ((fx,fy):f8os) x@((Akkord xs stem, (x1, y1)), dir)
  |abs (fx-x1)<8  =  trace ("found Flag for chord")x
setDuration f8os x@((N d c i, (x1, y1)), dir) = ((N (1%4) c i, (x1, y1)), dir)
setDuration f8os x@(((Akkord xs stem), pos), dir) = trace "Akkord" ((Akkord (map (setDur (1%4)) xs) stem, pos), dir)
setDuration f8os x = x

setDur dur (N _ c i) = N dur c i
setDur _ x = x
                       
mkElements ls  ( [],notes) = notes --map (\(xy,dir) -> (toElement  ls xy,dir)) notes 
mkElements ls ( theBeam@(x,y,w,h):bs, notes)
  |null beamedNotes = mkElements ls (bs, notes)
  |otherwise  = ((beam,(x,y)),stem):(mkElements ls (bs, (notes\\beamedNotes)))
  where
    (notesUnderneath1,notesAbove1)
      = partition (\((_,(_,ny)),_)-> ny > y) [note | note@((_,(nx,_)),_) <- notes, nx>x-10, nx<x+w+10]
    notesUnderneath = filter (\((_,(xp,_)),upDown)->upDown==Up&&xp<=x+w) notesUnderneath1
    notesAbove = filter (\((_,(xp,_)),upDown)->upDown==Down&&xp>=x) notesAbove1

    (beamedNotes,stem)
      |length notesUnderneath> length notesAbove = (notesUnderneath,Up) 
      |otherwise  = (notesAbove,Down)

    beam = 
              Beam 
                   (map (\chord -> if (length chord>1) then Akkord (map (fst.fst) chord) NoStem else (fst$fst$head chord))
                      $clusterBy
                        (\x y-> abs (((fst.snd.fst) x)-((fst.snd.fst) y)) <= 5)
                        (mkElements ls ([],map (\(n,_) -> (n,NoStem)) beamedNotes)))
                    stem



