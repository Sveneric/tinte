module Carulli.TrainNotes where
import Data.Either
import AI.HNN.FF.Network
import Numeric.LinearAlgebra
import Codec.Picture
import Codec.Picture.Types

sampleNotes = map (\n-> "trainingData/k4_"++(show n)++".png") [1..7]
sampleNoNotes = map (\n-> "trainingData/no_n4_"++(show n)++".png") [1..31]

sampleFlags8O = map (\n-> "trainingData/f8o_"++(show n)++".png") [1..7]
sampleNoFlags8O = map (\n-> "trainingData/nof8o_"++(show n)++".png") [1..38]


img2list i@(Image w h pxs) = img2list2  0 0 w h i

img2list2  x y w h i = [1.0 * (toDouble$pixelAt i xp yp) |xp<-[x..x+w-1],yp <-[y..y+h-1]]

toDouble :: PixelRGB8 -> Double
toDouble (PixelRGB8  r g b) = (fromIntegral r/255 +fromIntegral g/255 +fromIntegral b/255) / 3

readSampleFiles fs w h= do
  eithers <- sequence (map readImage fs)
  return$  map  ((img2list2 0 0 w h).convertRGB8)$rights eithers 
  
readN4s  = readSampleFiles sampleNotes 6 6
readNoN4s  = readSampleFiles sampleNoNotes 6 6

getTestable x y = img2list2 x y 5 15

readF8s  = readSampleFiles sampleFlags8O 6 10
readNoF8s  = readSampleFiles sampleNoFlags8O 6 10


moin = do
  samples <- readN4s
  let ss = map (\x-> (fromList x --> fromList [1.0])) samples
  nosamples <- readNoN4s
  let ns = map (\x-> (fromList x --> fromList [-1.0])) nosamples
  net <- createNetwork 36 [16] 1
  let smartNet = trainNTimes 100000 0.8 tanh tanh' net (ss++ns)
  mapM_ (print . output smartNet tanh . fst) (ss++ns)
  saveNetwork "neuralNetworks/n4.nn" smartNet


  samples <- readF8s
  let ss = map (\x-> (fromList x --> fromList [1.0])) samples
  nosamples <- readNoF8s
  let ns = map (\x-> (fromList x --> fromList [-1.0])) nosamples
  net <- createNetwork 60 [32] 1
  let smartNet = trainNTimes 100000 0.8 tanh tanh' net (ss++ns)
  mapM_ (print . output smartNet tanh . fst) (ss++ns)
  saveNetwork "neuralNetworks/f8.nn" smartNet
