for i in $( ls *.tinte); do
    echo item: $i;
    ../src/tintec ${i%%.*};
    ps2pdf ${i%%.*}.ps ${i%%.*}.pdf;
    echo convert: ${i%%.*}.pdf ${i%%.*}.jpg;
    convert ${i%%.*}.pdf ${i%%.*}.jpg;
    convert   -size 250000x300 ${i%%.*}-0.jpg -resize 2500000x200 +profile "*" middle-${i%%.*}.png;
    mv *.pdf ../website/files/music;
    mv middle-${i%%.*}.png ../website/files/previews;
done
rm *.jpg;

